// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Пример, когда унарный оператор sizeof вычисляет и
// возвращает размер типа данных в байтах
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
void setup() {
  Serial.begin(9600);
  Serial.print("Type\t\t"); Serial.println("Size");
  
  Serial.print("bool:\t\t"); Serial.print(sizeof(bool)); Serial.println(" bytes");
  Serial.print("char:\t\t"); Serial.print(sizeof(char)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(char) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("char16_t:\t"); Serial.print(sizeof(char16_t)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(char16_t) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("char32_t:\t"); Serial.print(sizeof(char32_t)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(char32_t) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("short:\t\t"); Serial.print(sizeof(short)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(short) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("int:\t\t"); Serial.print(sizeof(int)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(int) * 8) - 1) - 1); Serial.println(" values");
  // Архетиктуронезависимые типы (16, 32, 64)
  Serial.print("int_16_t:\t"); Serial.print(sizeof(int16_t)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(int16_t) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("uint_16_t:\t"); Serial.print(sizeof(uint16_t)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(uint16_t) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("int_32_t:\t"); Serial.print(sizeof(int32_t)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(int32_t) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("uint_64_t:\t"); Serial.print(sizeof(uint64_t)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(uint32_t) * 8) - 1) - 1); Serial.println(" values");
  
  Serial.print("long:\t\t"); Serial.print(sizeof(long)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(long) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("long long:\t"); Serial.print(sizeof(long long)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(long long) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("float:\t\t"); Serial.print(sizeof(float)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(float) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("double:\t\t"); Serial.print(sizeof(double)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(double) * 8) - 1) - 1); Serial.println(" values");
  Serial.print("long double:\t"); Serial.print(sizeof(long double)); Serial.print(" bytes, "); Serial.print(pow(2, (sizeof(long double) * 8) - 1) - 1); Serial.println(" values");
}

void loop() {

}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
