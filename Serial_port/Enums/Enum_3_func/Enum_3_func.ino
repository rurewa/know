// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Пример использования перечислений и функции в коде 
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
enum Colors {
  COLOR_RED, // index 0
  COLOR_GREEN, // index 1
  COLOR_BLUE, // index 2
  COLOR_WHITE,
  COLOR_BROWN,
  COLOR_ORANGE,
  COLOR_YELLOW
};

Colors chooseColors(char userColor) {
   Colors color = static_cast<Colors>(userColor);
   //Serial.print("Your enter: "); Serial.println(userColor); // Диагностика вывода в последовательный порт
    switch (userColor)
    {
      case '0': // 48
        Serial.println("Red\n");
        break;
      case '1':
        Serial.println("Green\n");
        break;
      case '2':
        Serial.println("Blue\n");
        break;
      case '3':
        Serial.println("White\n");
        break;
      case '4':
        Serial.println("Brown\n"); 
        break;
      case '5':
        Serial.println("Orange\n");
        break;
      case '6':
        Serial.println("Yellow\n");    
        break;
      default:
        Serial.println("No color!");
        break;
    }
}

void setup() {
  Serial.begin(9600);
  Serial.print("Choose a colors: \n red\t - 0,\n green\t -1\n blue\t - 2\n white\t - 3\n brown\t - 4\n orange\t - 5\n yellow\t - 6\n");
  Serial.println("Choose your color, enter a number: ");
}

void loop() {
  int userNumColor(0);
  while(Serial.available() > 0) {
    userNumColor = Serial.read();
    chooseColors(userNumColor);
  }
}
/* Output:
Choose a colors: 
 red   - 0,
 green   -1
 blue  - 2
 white   - 3
 brown   - 4
 orange  - 5
 yellow  - 6
Choose your color, enter a number: 
Red
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
