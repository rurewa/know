// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Пример использования перечислений в коде
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
enum Colors {
  COLOR_RED, // index 0
  COLOR_GREEN, // index 1
  COLOR_BLUE, // index 2
  COLOR_WHITE,
  COLOR_BROWN,
  COLOR_ORANGE,
  COLOR_YELLOW
};

void setup() {
  Serial.begin(9600);
  Serial.print("Choose a colors: \n red\t - 0,\n green\t -1\n blue\t - 2\n white\t - 3\n brown\t - 4\n orange\t - 5\n yellow\t - 6\n");
  Serial.println("Choose your color, enter a number: ");
}

void loop() {
  int userNumColor(0);
  while(Serial.available() > 0) {
    userNumColor = Serial.read();
    Colors color = static_cast<Colors>(userNumColor);
    switch (userNumColor)
    {
      case 48:
        Serial.println("Red\n");
        break;
      case 49:
        Serial.println("Green\n");
        break;
      case 50:
        Serial.println("Blue\n");
        break;
      case 51:
        Serial.println("White\n");
        break;
      case 52:
        Serial.println("Brown\n"); 
        break;
      case 53:
        Serial.println("Orange\n");
        break;
      case 54:
        Serial.println("Yellow\n");    
        break;
      default:
        Serial.println("No color!");
        break;
    }
  }
}
/* Output:
Choose a colors: 
 red   - 0,
 green   -1
 blue  - 2
 white   - 3
 brown   - 4
 orange  - 5
 yellow  - 6
Choose your color, enter a number: 
Red
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
