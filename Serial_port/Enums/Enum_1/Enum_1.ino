// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Пример использования перечислений в коде
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
enum Color {
  COLOR_RED, // 0
  COLOR_GREEN, // 1
  COLOR_BLUE // 2
};

void setup() {
  Serial.begin(9600);
  Serial.print("Color index: ");
  Serial.println(COLOR_GREEN);
  Color colorName = COLOR_BLUE;
  Serial.print("BLUE index: ");
  Serial.println(colorName);
}

void loop() {
  // put your main code here, to run repeatedly:

}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
