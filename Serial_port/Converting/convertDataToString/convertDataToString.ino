// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Конвертирование данных в строку String
// проверка преобразования числа в String
// http://mypractic.ru/urok-30-tekstovye-stroki-v-arduino-konvertirovanie-dannyx-v-stroki-i-naoborot-klass-string.html
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
int x = 0;  // переменная, которая выводится
float f = 0;  // переменная, которая выводится
String MyStr;
String MyStrF;
char myStrF[10];

void setup() {
  Serial.begin(9600); // скорость 9600
}

void loop() {
  //MyStr = String(x, DEC); // int -> DEC
  //MyStr= String(x, HEX);  // int -> HEX
  //MyStr= String(x, BIN);  // int -> BIN

  //Serial.println(MyStr);
  //++x;
  //----------------
  dtostrf(f, 2, 3, myStrF);  // выводим в строку myStr 2 разряда до, 3 разряда после запятой
  MyStrF = myStrF;
  Serial.println(MyStrF); 
  f += 0.01; 
  delay(500); 
  //----------------
  //delay(300);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
