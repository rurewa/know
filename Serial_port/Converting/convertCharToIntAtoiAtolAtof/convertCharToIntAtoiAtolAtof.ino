// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Преобразование строки в данные с помощью функций atoi, atol, atof
// http://mypractic.ru/urok-30-tekstovye-stroki-v-arduino-konvertirovanie-dannyx-v-stroki-i-naoborot-klass-string.html
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
const char MY_STR_INT[] = "123"; // текстовый массив
const char MY_STR_F[] = "123.456";  // текстовый массив

void setup() {
  Serial.begin(9600); // скорость 9600
  //Serial.println(atoi(MY_STR_INT));  // преобразование строки в int
  //Serial.println(atol(MY_STR_INT));  // преобразование строки в long
  Serial.println(atof(MY_STR_F), 3);  // преобразование строки в float
}

void loop() {

}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
