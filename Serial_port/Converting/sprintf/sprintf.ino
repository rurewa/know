// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Конвертирование данных в строку с помощью функции sprintf
// проверка преобразования числа в текстовую строку с помощью sprintf
// http://mypractic.ru/urok-30-tekstovye-stroki-v-arduino-konvertirovanie-dannyx-v-stroki-i-naoborot-klass-string.html
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
int x = 0;  // переменная, которая выводится
char myStr[20];  // текстовый массив

void setup() {
  Serial.begin(9600);
}

void loop() {
  // подготовка буфера строки
  for (int i = 0; i < 20; i++) { myStr[i] = ' '; }  // заполнение пробелами
  myStr[18] = '\r'; // возврат каретки
  myStr[19] = '\n'; // перевод строки

  // преобразование переменной int x в строку, где "%d" - ширина строки
  //sprintf(myStr,"%d", x); // int -> DEC
  //sprintf(myStr,"%5d", x); // int -> DEC
  sprintf(myStr,"%05d", x); // int -> DEC
  //sprintf(myStr,"%+05d", x); // int -> DEC
  //sprintf(myStr,"Cycle %d is over", x);  // int -> DEC с текстом
  //sprintf(myStr,"%o", x); // int -> OCT
  //sprintf(myStr,"%x", x); // int -> HEX
  Serial.write(myStr, 20);
  ++x;
  delay(250);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
