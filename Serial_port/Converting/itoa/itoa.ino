//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
// Конвертирование целочисленных данных в строку через функции itoa, ltoa, ultoa.
// проверка преобразования числа в текстовую строку
// http://mypractic.ru/urok-30-tekstovye-stroki-v-arduino-konvertirovanie-dannyx-v-stroki-i-naoborot-klass-string.html
// V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
int x = 0;  // переменная, которая выводится
char myStr[20];  // текстовый массив

void setup() {
  Serial.begin(9600); // скорость 9600
}

void loop() {
  // подготовка буфера строки
  for (int i = 0; i < 20; i++) { myStr[i] = ' '; }  // заполнение пробелами
  myStr[18] = '\r'; // возврат каретки
  myStr[19] = '\n'; // перевод строки
  // Раскомментировать нужное!
  // преобразование переменной int x
  //itoa(x, myStr, 10); // int -> DEC
  itoa(x, myStr, 8); // int -> OCT
  //itoa(x, myStr, 16); // int -> HEX
  //itoa(x, myStr, 2); // int -> BIN

  // преобразование переменной long x
  //ltoa(x, myStr, 10); // long -> DEC
  //ltoa(x, myStr, 8); // long -> OCT
  //ltoa(x, myStr, 16); // long -> HEX
  //ltoa(x, myStr, 2); // long -> BIN

  // преобразование переменной unsigned long x
  //ultoa(x, myStr, 10); // long -> DEC
  //ultoa(x, myStr, 8); // long -> OCT
  //ultoa(x, myStr, 16); // long -> HEX
  //ultoa(x, myStr, 2); // long -> BIN

  Serial.write(myStr, 20);
  ++x;
  delay(250);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
