// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Получим данные из объекта String в массив и выполним преобразование функцией atof()
// проверка преобразования String в число с плавающей точкой
// http://mypractic.ru/urok-30-tekstovye-stroki-v-arduino-konvertirovanie-dannyx-v-stroki-i-naoborot-klass-string.html
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
String MyStr = "34.123";
char myStr[10];

void setup() {
  Serial.begin(9600); // скорость 9600
  MyStr.toCharArray(myStr, MyStr.length()); // копирование String в массив myStr
  Serial.println(atof(myStr));  //  преобразование в float
}

void loop() { 
   
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
