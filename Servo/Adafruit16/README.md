## PCA9685 module with Arduino - 16 канальный модуль сервоприводов

![PCA9685 module with Arduino](/Servo/Adafruit16/Images/servo-driver-pinout.jpg)

Установить библиотеку можно по ключевым словам **Adafruit PWM Servo Driver Library** в Менеджере библиотек Arduino IDE

[Загрузить библиотеку можно тут](https://www.arduinolibraries.info/libraries/adafruit-pwm-servo-driver-library)

Для корректной работы кода возможно понадобится доустановить ещё 2 библиотеки в Arduino IDE

```
Adafruit BusIO
```
и

```
Adafruit PWM
```

[Примеры кода](/Servo/Adafruit16/Code/)

### Реурсы

[Официальная страница производителя контроллера]()

[Adafruit-PWM-Servo-Driver-Library](https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library?tab=readme-ov-file)

[PCA9685 - Multiple Servo Control Using Arduino](https://circuitdigest.com/microcontroller-projects/pca9685-multiple-servo-control-using-arduino)

[16 сервоприводов. Библиотека Adafruit_PWMServoDriver.h](https://arduino.ru/forum/programmirovanie/16-servoprivodov-biblioteka-adafruitpwmservodriverh)

[Заброшенный репозиторий проекта](https://gitflic.ru/project/tank2010/spider_robot)