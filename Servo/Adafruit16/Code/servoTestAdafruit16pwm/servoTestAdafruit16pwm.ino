// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Тест сервоконтроллера
// V 1.3
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Wire.h>
#include "Adafruit_PWMServoDriver.h"

// default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

const int FREQUENCY = 60; // Частота. Определяется эксперементально

void setup() {
  Serial.begin(9600); // Для диагностики
  pwm.begin(); // Инициализируем сервоконтроллер
  pwm.setPWMFreq(1600);  // This is the maximum PWM frequency
  Wire.setClock(400000);
  pwm.setOscillatorFrequency(27000000);
  //pwm.setPWMFreq(FREQUENCY);
  // Ставим все серво в 0 град. начале программы (не обязательно)
  /*
  pwm.setPWM(4, 0, pulseWidth(0)); // Ставим серво в  град
  pwm.setPWM(5, 0, pulseWidth(0));
  pwm.setPWM(6, 0, pulseWidth(0));
  pwm.setPWM(7, 0, pulseWidth(0));
  pwm.setPWM(8, 0, pulseWidth(0));
  pwm.setPWM(9, 0, pulseWidth(0));
  pwm.setPWM(10, 0, pulseWidth(0));
  pwm.setPWM(11, 0, pulseWidth(0));
  */
}

void loop() {
  // Тест все 16 каналов серво
  for (uint16_t i=0; i<4096; i += 8) {
    for (uint8_t pwmnum=0; pwmnum < 16; pwmnum++) {
      pwm.setPWM(pwmnum, 0, (i + (4096/16)*pwmnum) % 4096 );
   }
  }
  //pwm.setPWM(4, 0, pulseWidth(90)); // Ставим серво в 180 град
  //pwm.setPWM(5, 0, pulseWidth(180));
  //pwm.setPWM(6, 0, pulseWidth(180));
  //pwm.setPWM(7, 0, pulseWidth(180));
  //pwm.setPWM(8, 0, pulseWidth(180));
  //pwm.setPWM(9, 4096, pulseWidth(180)); // Работает
  //pwm.setPWM(10, 0, pulseWidth(180));
  //pwm.setPWM(11, 0, pulseWidth(180));
}

int pulseWidth(int angle) { // Необходимая функция!
  const int MIN_PULSE_WIDTH = 650;
  const int MAX_PULSE_WIDTH = 2350;
  int pulse_wide, analog_value;
  pulse_wide   = map(angle, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
  analog_value = int(float(pulse_wide) / 1000000 * FREQUENCY * 4096);
  return analog_value;
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
