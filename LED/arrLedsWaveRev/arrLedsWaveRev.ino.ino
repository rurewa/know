const int ARR_LEDS[] = {3, 5, 6, 9, 10, 11, 12, 13};

void setup() {
  for (byte pins = 0; pins <= 7; ++pins) {
    pinMode(ARR_LEDS[pins], OUTPUT);
  }
  Serial.begin(9600);
}

void loop() {
  int count = 0;
  int revers = 7;
  while (count <= 7) {
    digitalWrite(ARR_LEDS[count], !HIGH);
    digitalWrite(ARR_LEDS[revers], !HIGH);
    delay(500);
    digitalWrite(ARR_LEDS[count], !LOW);
    digitalWrite(ARR_LEDS[revers], !LOW);
    delay(500);
    Serial.print(count);
    Serial.print(" ");
    ++count;
    Serial.print(revers);
    Serial.println(" ");
    --revers;
  }
}
