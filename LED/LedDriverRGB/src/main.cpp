// Demo for LED Strip Driver controling single RGB LED strip by Catalex
// Hardware: A LED Strip Driver and 5 meters of RGB LED strip
// Board: Carduino or Arduino UNO R3
// IDE:   Arduino-1.0
// Function: Show 7 colors with a single RGB LED.
// Store: catalex.taobao.com
// Пример работы RGB-светодиодной ленты с драйвером. V 1.0
/***************************************************************/
#include "RGBdriver.h"

const int CLK = 3; // Pins definitions for the driver
const int DIO = 2;

RGBdriver Driver(CLK, DIO);

const int times = 2000;
const int brightness = 45;
const int R_BRIGHTNESS = 45;
const int G_BRIGHTNESS = 45;
const int B_BRIGHTNESS = 45;

void setup() { }

void loop() {
  Driver.begin();
  Driver.SetColor(R_BRIGHTNESS, 0, 0); //Red
  Driver.end();
  delay(times);
  Driver.begin();
  Driver.SetColor(0, G_BRIGHTNESS, 0); //Green
  Driver.end();
  delay(times);
  Driver.begin();
  Driver.SetColor(0, 0, B_BRIGHTNESS);//Blue
  Driver.end();
  delay(times);
  Driver.begin();
  Driver.SetColor(R_BRIGHTNESS, G_BRIGHTNESS, 0); // Yellow
  Driver.end();
  delay(times);
  Driver.begin();
  Driver.SetColor(0, G_BRIGHTNESS, B_BRIGHTNESS); // Cyan
  Driver.end();
  delay(times);
  Driver.begin();
  Driver.SetColor(R_BRIGHTNESS, 0, B_BRIGHTNESS); // Magenta
  Driver.end();
  delay(times);
  Driver.begin();
  Driver.SetColor(R_BRIGHTNESS, G_BRIGHTNESS, B_BRIGHTNESS); // White
  Driver.end();
  delay(times);
  Driver.begin();
  Driver.SetColor(0, 0, 0);//all LED is off
  Driver.end();
  delay(5000);
  // Текущий код
  /*
  for (int i = 0; i < 256; ++i) {
    Driver.begin(); // begin
    Driver.SetColor(0, 0, i); // Blue. First node data. SetColor(R,G,B)
    Driver.end();
    delay(2);
  }
  for (int i = 0; i < 256; ++i) {
    Driver.begin(); // begin
    Driver.SetColor(0, i, 0); // Blue. First node data. SetColor(R,G,B)
    Driver.end();
    delay(2);
  }
  for (int i = 0; i < 256; ++i) {
    Driver.begin(); // begin
    Driver.SetColor(i, 0, 0); // Blue. First node data. SetColor(R,G,B)
    Driver.end();
    delay(2);
  }
  */
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
