// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Пример применения в программах для Arduino простых перечислений
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
enum Colors {
  NO_COLOR, // 0 в enum, 48 - в ASCII
  WHITE,
  ORANGE,
  YELLOW
};

enum LedPins { // Пины светодиодов
  RED_PIN = 6,
  BLUE_PIN = 9,
  GREEN_PIN // 10
};
// Объекты пинов
LedPins red = RED_PIN;
LedPins blue = BLUE_PIN;
LedPins green = GREEN_PIN;

const int buttonPin = 3;

auto testLeds() -> void;
auto whiteColor(int redStat, int blueStat, int greenStat) -> void;
auto orangeColor(int redStat, int blueStat, int greenStat) -> void;
auto yellowColor(int redStat, int blueStat, int greenStat) -> void;
auto noColor() -> void;

void setup() {
  Serial.begin(9600);
  pinMode(red, OUTPUT);
  pinMode(blue, OUTPUT);
  pinMode(green, OUTPUT);
}

void loop() {
  static int count = 0;
  static bool flag = false;
  if (digitalRead(buttonPin) == true && flag == false) {
    flag = true; // Простой антидребезг
    ++count;
  }
  if (digitalRead(buttonPin) == false && flag == true) {
    flag = false;
  }
  while (count >= 4) {
    count = 0;  // Счётчик
  }
  Serial.println(count);
  switch (count) { // Выбор цвета
    case NO_COLOR:
      noColor();
      break;
    case WHITE: // Значение берём из перечисления Colors
      whiteColor(255, 255, 255);
      break;
    case ORANGE:
      orangeColor(255, 0, 65);
      break;
    case YELLOW:
      yellowColor(255, 0, 125);
      break;
    default:
      noColor();
      break;
  }
}

void whiteColor(int redStat, int blueStat, int greenStat) {
  analogWrite(red, redStat);
  analogWrite(blue, blueStat);
  analogWrite(green, greenStat);
}

void orangeColor(int redStat, int blueStat, int greenStat) {
  analogWrite(red, redStat);
  analogWrite(blue, blueStat);
  analogWrite(green, greenStat);
}

void yellowColor(int redStat, int blueStat, int greenStat) {
  analogWrite(red, redStat);
  analogWrite(blue, blueStat);
  analogWrite(green, greenStat);
}

void noColor() {
  whiteColor(0, 0, 0); orangeColor(0, 0, 0); yellowColor(0, 0, 0);
}

void testLeds() {
  digitalWrite(red, HIGH);
  delay(1000);
  digitalWrite(red, LOW);
  delay(1000);
  digitalWrite(blue, HIGH);
  delay(1000);
  digitalWrite(blue, LOW);
  delay(1000);
  digitalWrite(green, HIGH);
  delay(1000);
  digitalWrite(green, LOW);
  delay(1000);
}
//testLeds();
//whiteColor(255, 255, 255);
//orangeColor(255, 0, 65);
//yellowColor(255, 0, 125);
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
