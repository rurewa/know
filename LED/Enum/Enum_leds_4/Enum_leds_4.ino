// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Пример применения в программах для Arduino простых перечислений
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
enum Colors {
  WHITE = 48, // 48 - код в ASCII
  ORANGE, // 49
  YELLOW // 50
};

enum LedPins { // Пины светодиодов
  RED_PIN = 6,
  BLUE_PIN = 9,
  GREEN_PIN // 10
};
// Объекты пинов
LedPins red = RED_PIN;
LedPins blue = BLUE_PIN;
LedPins green = GREEN_PIN;
// Объекты цветов
Colors white = WHITE;
Colors orange = ORANGE;
Colors yellow = YELLOW;

const int buttonPin = 3;

auto testLeds() -> void;
auto whiteColor(int redStat, int blueStat, int greenStat) -> void;
auto orangeColor(int redStat, int blueStat, int greenStat) -> void;
auto yellowColor(int redStat, int blueStat, int greenStat) -> void;
auto testInput_EnamColors(char chooseColors) -> void;

int count = 0;

void setup() {
  Serial.begin(9600);
  pinMode(red, OUTPUT);
  pinMode(blue, OUTPUT);
  pinMode(green, OUTPUT);
}

void loop() { 
  char chooseColors = 0;
  //Serial.println(chooseColors); // Диагностика
  while (Serial.available() > 0) {
    chooseColors = Serial.read();
    if (chooseColors == white) { whiteColor(255, 255, 255); }
    else if (chooseColors == orange) { orangeColor(255, 0, 65); }
    else if (chooseColors == yellow) { yellowColor(255, 0, 125); }
    else { whiteColor(0, 0, 0); orangeColor(0, 0, 0); yellowColor(0, 0, 0); }
  }
}

void whiteColor(int redStat, int blueStat, int greenStat) {
  analogWrite(red, redStat);
  analogWrite(blue, blueStat);
  analogWrite(green, greenStat);
}

void orangeColor(int redStat, int blueStat, int greenStat) {
  analogWrite(red, redStat);
  analogWrite(blue, blueStat);
  analogWrite(green, greenStat);
}

void yellowColor(int redStat, int blueStat, int greenStat) {
  analogWrite(red, redStat);
  analogWrite(blue, blueStat);
  analogWrite(green, greenStat);
}

void testLeds() {
  digitalWrite(red, HIGH);
  delay(1000);
  digitalWrite(red, LOW);
  delay(1000);
  digitalWrite(blue, HIGH);
  delay(1000);
  digitalWrite(blue, LOW);
  delay(1000);
  digitalWrite(green, HIGH);
  delay(1000);
  digitalWrite(green, LOW);
  delay(1000);
}

void testInput_EnamColors(char chooseColors) {
  Serial.print("Input: ");
  Serial.print(chooseColors);
  Serial.print(", Enum white: ");
  Serial.println(white); 
}
//testLeds();
//whiteColor(255, 255, 255);
//orangeColor(255, 0, 65);
//yellowColor(255, 0, 125);
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
