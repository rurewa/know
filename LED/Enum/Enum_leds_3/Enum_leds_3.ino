// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Пример применения в программах для Arduino class перечислений
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
enum class Colors {
  WHITE,
  ORANGE,
  YELLOW
};

enum class LedPins {
  RED_PIN = 6,
  BLUE_PIN = 9,
  GREEN_PIN = 10
};

LedPins red = LedPins::RED_PIN;
LedPins blue = LedPins::BLUE_PIN;
LedPins green = LedPins::GREEN_PIN;

void setup() {
  Serial.begin(9600);
  pinMode(static_cast<int>(red), OUTPUT);
  pinMode(static_cast<int>(blue), OUTPUT);
  pinMode(static_cast<int>(green), OUTPUT);
}

void loop() {
  digitalWrite(static_cast<int>(red), HIGH);
  delay(1000);
  digitalWrite(static_cast<int>(blue), HIGH);
  delay(1000);
  digitalWrite(static_cast<int>(green), HIGH);
  delay(1000);
  digitalWrite(static_cast<int>(red), LOW);
  digitalWrite(static_cast<int>(blue), LOW);
  digitalWrite(static_cast<int>(green), LOW);
  delay(1000);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
