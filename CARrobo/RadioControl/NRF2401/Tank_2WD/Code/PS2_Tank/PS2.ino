#include "PS2X_lib.h"
#include "PCF8574.h"

PCF8574 pcf(0x20);
PS2X ps2x; // создание устройства типа "контроллер"


const int del = 100;

const int LeftMotorPin = 3;
const int RightMotorPin = 5;
const int RightMotorIn1 = P5;
const int RightMotorIn2 = P4;
const int LeftMotorIn1 = P3;
const int LeftMotorIn2 = P2;

int motor_speed_right = 245;
int motor_speed_left = 255;

void move_turn();
void forward();
void backward();
void left();
void right();
void stop();

void setup()
{
	Serial.begin(9600);
	ps2x.config_gamepad(10, 12, 11, 13, true, true); //настройка джойстика (clock, command, attention, data, Pressures?, Rumble?)
	pcf.pinMode(P2, OUTPUT);
	pcf.pinMode(P3, OUTPUT);
	pcf.pinMode(P4, OUTPUT);
	pcf.pinMode(P5, OUTPUT);
	analogWrite(3, motor_speed_right);
	analogWrite(5, motor_speed_left);
}

void loop()
{
	ps2x.read_gamepad();
	if (ps2x.Button(PSB_START))
	{
		
	}
	else if (ps2x.Analog(PSS_RY) < 128 || ps2x.Analog(PSS_LY) < 128)
	{
		move_turn();
		forward();
	}
	else if (ps2x.Analog(PSS_RY) > 128 || ps2x.Analog(PSS_LY) > 128)
	{
		move_turn();
		backward();
	}
	else if (ps2x.Button(PSB_L3) && motor_speed_left >= 140)
	{
		motor_speed_right -= 15;
		motor_speed_left -= 15;
		analogWrite(3, motor_speed_right);
		analogWrite(5, motor_speed_left);
	}
	else if (ps2x.Button(PSB_R3) && motor_speed_left != 255)
	{
		motor_speed_right += 15;
		motor_speed_left += 15;
		analogWrite(3, motor_speed_right);
		analogWrite(5, motor_speed_left);
	}
	else if (ps2x.Button(PSB_GREEN))
	{
		Serial.println("Forward");
		forward();
	}
	else if (ps2x.Button(PSB_BLUE))
	{
		Serial.println("Backward");
		backward();
	}
	else if (ps2x.Button(PSB_PINK))
	{
		Serial.println("Left");
		left();
	}
	else if (ps2x.Button(PSB_RED))
	{
		Serial.println("Right");
		right();
	}
	else if (ps2x.Button(PSB_R1))
	{
		
	}
	else if (ps2x.Button(PSB_R2))
	{
		
	}
	else if (ps2x.Button(PSB_L1))
	{
		
	}
	else if (ps2x.Button(PSB_L2))
	{
		
	}
	else if (ps2x.Button(PSB_PAD_LEFT))
	{
		
	}
	else if (ps2x.Button(PSB_PAD_RIGHT))
	{
		
	}
	else if (ps2x.Button(PSB_PAD_UP))
	{
		
	}
	else if (ps2x.Button(PSB_PAD_DOWN))
	{
		
	}
	else
	{
		if (ps2x.Analog(PSS_RY) == 128 || ps2x.Analog(PSS_LY) == 0)
		{
			analogWrite(3, motor_speed_right);
			analogWrite(5, motor_speed_left);
		}
		stop();
	}
	delay(20);
}

void move_turn()
{
	if (ps2x.Analog(PSS_RX) < 128)
	{
		int deg = map(ps2x.Analog(PSS_RX), 0, 128, 5, 1);
		analogWrite(3, motor_speed_right / deg);
		analogWrite(5, motor_speed_left);
	}
	else if (ps2x.Analog(PSS_RX) > 128)
	{
		int deg = map(ps2x.Analog(PSS_RX), 128, 255, 1, 5);
		analogWrite(3, motor_speed_right);
		analogWrite(5, motor_speed_left / deg);
	}
	else if (ps2x.Analog(PSS_LX) < 128)
	{
		int deg = map(ps2x.Analog(PSS_LX), 0, 128, 5, 1);
		analogWrite(3, motor_speed_right / deg);
		analogWrite(5, motor_speed_left);
	}
	else if (ps2x.Analog(PSS_LX) > 128)
	{
		int deg = map(ps2x.Analog(PSS_LX), 128, 255, 1, 5);
		analogWrite(3, motor_speed_right);
		analogWrite(5, motor_speed_left / deg);
	}
	else
	{
		analogWrite(3, motor_speed_right);
		analogWrite(5, motor_speed_left);
	}
}

void forward()
{
	pcf.digitalWrite(P3, HIGH);
	pcf.digitalWrite(P2, LOW);
	pcf.digitalWrite(P5, HIGH);
	pcf.digitalWrite(P4, LOW);
}

void backward()
{
	pcf.digitalWrite(P3, LOW);
	pcf.digitalWrite(P2, HIGH);
	pcf.digitalWrite(P5, LOW);
	pcf.digitalWrite(P4, HIGH);
}

void right()
{
	pcf.digitalWrite(P3, LOW);
	pcf.digitalWrite(P2, HIGH);
	pcf.digitalWrite(P5, HIGH);
	pcf.digitalWrite(P4, LOW);
}

void left()
{
	pcf.digitalWrite(P3, HIGH);
	pcf.digitalWrite(P2, LOW);
	pcf.digitalWrite(P5, LOW);
	pcf.digitalWrite(P4, HIGH);
}

void stop()
{
	pcf.digitalWrite(P3, LOW);
	pcf.digitalWrite(P2, LOW);
	pcf.digitalWrite(P5, LOW);
	pcf.digitalWrite(P4, LOW);
}

