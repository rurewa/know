#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

const int CSN_GPIO = 10;
const int CE_GPIO = 9;

// Hardware configuration
RF24 radio(CE_GPIO, CSN_GPIO);                           // Set up nRF24L01 radio on SPI bus plus pins 7 & 8

const byte ADDRESS[6] = "00001";
int Pot_Val_Y = 0,Pot_Val_X = 0, Up_key = 0, Dn_key = 0, Left_key = 0, Right_key = 0;
unsigned char Tx_joystick = 0;
unsigned char Tx_buttons = 0;

void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.openWritingPipe(ADDRESS);
  radio.setPALevel(RF24_PA_MAX);
  radio.stopListening();
  radio.write(&Tx_command, sizeof(Tx_command));
}
void loop() {
  Dn_key = digitalRead(5);
  Up_key = digitalRead(3);
  Right_key = digitalRead(4);
  Left_key = digitalRead(6);
  1_key = digitalRead(7);
  2_key = digitalRead(8);
  Pot_Val_Y = analogRead(A0);
  Pot_Val_X = analogRead(A1);


  

  if(Dn_key==0)
  {
      Tx_command = 2;
  }
  else if(Up_key==0)
  {
      Tx_command = 1;
  }
  else if(Right_key==0)
  {
      Tx_command = 4;
  }
  else if(Left_key==0)
  {
      Tx_command = 3 ;
  }
  else if(1_key==0)
  {
      Tx_command = 1;
  }
  else if(2_key==0)
  {
      Tx_command = 1;
  }
  else
  {
      Tx_command = 0;
  }
  

  radio.write(&Tx_command, 1);
  delay(150);
}
