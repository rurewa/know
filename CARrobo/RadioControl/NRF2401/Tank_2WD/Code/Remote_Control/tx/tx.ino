// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// TX. Remote control for NRF24L01. Joystik and buttons
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include "nRF24L01.h"
#include "RF24.h"

// transmitter
RF24 radio(9, 10); // CE, CSN
const byte ADDRESS[6] = "00001";
char xyData[32] = "";
String xAxis, yAxis, rightButton, upButton, downButton;
bool leftButton;

void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.openWritingPipe(ADDRESS);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}

void loop() {
  xAxis = analogRead(A0); // Считываем показания по X
  yAxis = analogRead(A1); // Считываем показания по Y
  
  leftButton = digitalRead(6); //
  rightButton = digitalRead(4); //
  upButton = digitalRead(3); //
  downButton = digitalRead(5); //

  // Значение по Х
  /*
  xAxis.toCharArray(xyData, 5); // Переводим значения X в массив символов
  radio.write(&xyData, sizeof(xyData)); // Отправляем данные массива X в другой модуль NRF24L01
  //Serial.print("x: ");
  //Serial.print(xyData);
  // Значение по Y
  yAxis.toCharArray(xyData, 5);
  radio.write(&xyData, sizeof(xyData));
  
  if (leftButton == true) {
    radio.write(&leftButton, sizeof(leftButton));
    Serial.println(leftButton);
    
  }
  */
  //diagnButtons();
  //diagnJoystic();
  delay(20);
}

void diagnButtons() { // For diagnostics
  Serial.print("Left: ");
  Serial.print(leftButton);
  Serial.print(" Up: ");
  Serial.print(upButton);
  Serial.print(" Right: ");
  Serial.print(rightButton);
  Serial.print(" Down: ");
  Serial.println(downButton);
}

void diagnJoystic() {
  Serial.println(xAxis);
  Serial.println(yAxis);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
