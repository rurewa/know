#include "IRremote.h"

int walk = 0;

IRrecv irrecv(A0);
decode_results results;

const int ENBSpeed = 255;
const int ENASpeed = 255;

const int ENA = 3;
const int In1 = 8;
const int In2 = 7;
const int ENB = 9;
const int In3 = 6;
const int In4 = 5;

unsigned long timer = 0;

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn();
  pinMode (ENBIn1, OUTPUT);
  pinMode (ENBIn2, OUTPUT);
  pinMode (In1, OUTPUT);
  pinMode (In2, OUTPUT);
}
void loop() { 
  if (irrecv.decode(&results))
  {
    Serial.println(results.value, HEX);
    switch (results.value) {
      case 0xFF629D: // - вперёд,
        forward ();
        walk = 1;
        break;
      case 0xFFA857: // - назад,
        backward ();
        walk = 2;
        break;
      case 0xFF22DD: // - влево,
        left ();
        walk = 3;
        break;
      case 0xFFC23D: // - вправо
        right ();
        walk = 4;
        break;
      case 0xFFFFFFFF: // - повтор последней команды
        if (walk == 1) {
          forward ();
        }
        if (walk == 2) {
          backward ();
        }
        if (walk == 3) {
          left ();
        }
        if (walk == 4) {
          right ();
        }
        break;
    }
    irrecv.resume();
  }
  delay (100);
  digitalWrite (ENBIn1, LOW);
  digitalWrite (ENBIn2, LOW);
  digitalWrite (In1, LOW);
  digitalWrite (In2, LOW);
  analogWrite (ENBPin, 0);
  analogWrite (ENAPin, 0);
}


void forward () {
  analogWrite (ENBPin, ENBSpeed);
  analogWrite (ENAPin, ENASpeed);
  digitalWrite (ENBIn1, LOW);
  digitalWrite (ENBIn2, HIGH);
  digitalWrite (In1, LOW);
  digitalWrite (In2, HIGH);
}

void backward () {
  analogWrite (ENBPin, ENBSpeed);
  analogWrite (ENAPin, ENASpeed);
  digitalWrite (ENBIn1, HIGH);
  digitalWrite (ENBIn2, LOW);
  digitalWrite (In1, HIGH);
  digitalWrite (In2, LOW);
}

void left () {
  analogWrite (ENBPin, ENBSpeed);
  analogWrite (ENA, ENASpeed);
  digitalWrite(ENBIn1, HIGH);
  digitalWrite (ENBIn2, LOW);
  digitalWrite (In1, LOW);
  digitalWrite (In2, HIGH);
}

void right () {
  analogWrite (ENBPin, ENBSpeed);
  analogWrite (ENA, ENASpeed);
  digitalWrite (ENBIn1, LOW);
  digitalWrite (ENBIn2, HIGH);
  digitalWrite (In1, HIGH);
  digitalWrite (In2, LOW);
}
