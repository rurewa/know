// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Робот 2WD с использованием L298P. Автор - Тимофей Курганов.
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <NewPing.h> // Бибилиотека сонара
#include <Servo.h> // Библиотека сервопривода

Servo myservo; // Объект библиотеки сервопривода
// Настройки сонара
const unsigned int ECHO_PIN = 10;
const unsigned int TRIG_PIN = 11;
NewPing sonar(TRIG_PIN, ECHO_PIN, 200); // Максимальное расстояние видимости (<= 400)
// Моторы
// Right side
const int ENA = 3;
const int IN1 = 4;
const int IN2 = 5;
// Left side
const int ENB = 9;
const int IN3 = 6;
const int IN4 = 7;

void setup() {
  Serial.begin(9600); // Для диагностики сонара
  //pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  //pinMode(ENB, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT); // Настройка trig сонара
  myservo.attach(14); // Установка номера пина для сервопривода
}

void loop() {
  // Тест сонара
  int distance = sonar.ping_cm(); // Получение и запись расстояния с сонара
  Serial.println(distance); // Для диагностики сонара
  /*
  // Тест сервопривода
  myservo.write(0); // Устанавливаем сервопривод на 0 град.
  delay(1000);  // Задержка 1 сек.
  myservo.write(90);  // Устанавливаем сервопривод на 90 град.
  delay(1000);  // Задержка на 1 сек.
  myservo.write(180); // Устанавливаем сервопривод на 180 град.
  delay(1000);  // Задержка на 1 сек.
  */
  
  /*
  // Тест моторов
  analogWrite(ENA, 255);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENB, 255);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  delay(2000);
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
  delay(2000);
  analogWrite(ENA, 255);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENB, 255);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  delay(2000);
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
  delay(2000);
  */
}
