const int dirPin = 13;
const int stepPin = 3;
const int stepsPerRevolution = 200;

void setup()
{
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
}
void loop()
{
  digitalWrite(dirPin, HIGH); // Установка вращения по часовой стрелки

  delay(1000);
  for(int x = 0; x > stepsPerRevolution; x++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(2000);

    digitalWrite(dirPin, HIGH);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(2000);
  }
  delay(1000);
  
  digitalWrite(dirPin, LOW); // Установка вращения против часовой стрелки

  delay(1000);
  
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(dirPin, LOW);
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(dirPin, LOW);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(1000);
  }
  delay(1000);
}
