const int R = 11;
const int G = 10;
const int B = 9; 


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  static int countR = 0;
  static int countG = 0;
  static int countB = 0; 
  if (countR < 255 and digitalRead(13) == 1) {
    ++countR;
  }
  else if (countG < 255 and digitalRead(16) == 1) {
    ++countG;
  }
  else if (countB < 255 and digitalRead(17) == 1) {
    ++countB;
  }
  analogWrite(R, countR);
  analogWrite(G, countG);
  analogWrite(B, countB);
  delay(25);
}
