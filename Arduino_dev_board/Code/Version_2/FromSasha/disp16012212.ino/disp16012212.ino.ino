#include <Wire.h>                             
#include <LiquidCrystal_I2C.h>        
LiquidCrystal_I2C lcd(0x27, 16, 2); 
      
byte rightP[8] = {
 B01100, 
 B01100, 
 B00000, 
 B01110, 
 B11100, 
 B01100, 
 B11010, 
 B10011
};
byte strP[8] = {
  B00110,
  B00110,
  B01111,
  B01111,
  B01111,
  B01111,
  B00110,
  B00110
};
byte leftP[8] = {
 B00110, 
 B00110, 
 B00000, 
 B01110, 
 B00111, 
 B00110, 
 B01011, 
 B11001
};
byte non[8]{
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};
void setup(){
  Serial.begin(9600);
  lcd.init();                        
  lcd.backlight(); 
  lcd.clear();            
  
  lcd.createChar(3, strP);
  lcd.createChar(1, rightP);
  lcd.createChar(2, leftP);
  lcd.createChar(4, non);
}
    
void loop(){
    static int pos = 0;
    int line = 0;
    int left = digitalRead(16), right = digitalRead(13);
    Serial.println(pos);
    lcd.setCursor(pos, 0);
    if (left == true and right == true) {
      lcd.noDisplay();
      lcd.display();
      lcd.write(3);
      lcd.setCursor(pos, 0);
      delay(100);
    }
    else if (left == false and right == true and pos < 15) {
      lcd.setCursor(pos, 0);
      lcd.noDisplay();
      lcd.display();
      lcd.write(2);
      ++pos;
      lcd.scrollDisplayRight();
      delay(100);
      lcd.clear();
    }
    else if (left == true and right == false and pos > 0){
      lcd.setCursor(pos, 0);
      lcd.noDisplay();
      lcd.display();
      lcd.write(1);
      --pos;
      lcd.scrollDisplayLeft();
      delay(100);
      lcd.clear();
    }
    else {
      lcd.noDisplay();
      lcd.display();
      lcd.write(3);
      lcd.setCursor(pos, 0);
      delay(100);
    }
}
