#include <GyverTM1637.h>
#include <Temperature_LM75_Derived.h>
const int BUZZER = 9;
const int CLK= 10;
const int DIO= 11;

GyverTM1637 disp(CLK, DIO);
Generic_LM75 temperature;

void setup() {
  pinMode(BUZZER, OUTPUT);
  Serial.begin(9600);
  Wire.begin();
  disp.clear();
  disp.brightness(7);
}

void loop() {
  /*for(int d = 10; d>=1; --d){
    Serial.println(d);
    disp.displayInt(d);
    if (d == 1) {
      tone(BUZZER, 900);
      delay(600);
      noTone(BUZZER);
    }
    else{
      delay(500);
    }
  }*/
  int t = temperature.readTemperatureC();
  disp.displayInt(t);
  for(int c = 0; t >= 31; ++c) {
    t = temperature.readTemperatureC();
    disp.displayInt(-c);
    tone(BUZZER, c);
    delay(250);
  }
  noTone(BUZZER);
  delay(250);
}
