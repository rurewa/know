#include <Wire.h>           
#include <DallasTemperature.h>                  
#include <LiquidCrystal_I2C.h>  
#include <NewPing.h>  
#include "LedControl.h"  

const int trig = 14;
const int echo = 15;
NewPing sonar(trig, echo, 40);

LiquidCrystal_I2C lcd(0x27, 16, 2); 
LedControl lc = LedControl(5, 6, 7, 1);

const int ONE_WIRE_BUS = 2;

// Шина I2C для работы датчика t
OneWire oneWire(ONE_WIRE_BUS);

// Передача c шины на датчик t
DallasTemperature sensors(&oneWire);
byte rightP[8] = {
 B01100, 
 B01100, 
 B00000, 
 B01110, 
 B11100, 
 B01100, 
 B11010, 
 B10011
};
byte strP[8] = {
  B00110,
  B00110,
  B01111,
  B01111,
  B01111,
  B01111,
  B00110,
  B00110
};
byte leftP[8] = {
 B00110, 
 B00110, 
 B00000, 
 B01110, 
 B00111, 
 B00110, 
 B01011, 
 B11001
};
void ledMatrix() {
  // Проверка 8х8 светодиодной матрицы
  //lc.setLed(0, 3, 4, true);
  lc.setRow(0, 0, B11111111);
  lc.setRow(0, 1, B11111111);
  lc.setRow(0, 2, B11100111);
  lc.setRow(0, 3, B11011011);
  lc.setRow(0, 4, B11100111);
  lc.setRow(0, 5, B11111111);
  lc.setRow(0, 6, B11111111);
  lc.setRow(0, 7, B11111111);
}

byte non[8]{
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};

const byte IMAGES[][8] = {
  {0x30, 0x70, 0x30, 0x30, 0x30, 0x30, 0xfc, 0x00},
  {0x78, 0xcc, 0x0c, 0x38, 0x60, 0xcc, 0xfc, 0x00},
  {0x78, 0xcc, 0x0c, 0x38, 0x0c, 0xcc, 0x78, 0x00},
  {0x1c, 0x3c, 0x6c, 0xcc, 0xfe, 0x0c, 0x1e, 0x00},
  {0xfc, 0xc0, 0xf8, 0x0c, 0x0c, 0xcc, 0x78, 0x00},
  {0x38, 0x60, 0xc0, 0xf8, 0xcc, 0xcc, 0x78, 0x00},
  {0xfc, 0xcc, 0x0c, 0x18, 0x30, 0x30, 0x30, 0x00},
  {0x78, 0xcc, 0xcc, 0x78, 0xcc, 0xcc, 0x78, 0x00},
  {0x78, 0xcc, 0xcc, 0x7c, 0x0c, 0x18, 0x70, 0x00},
  {0x7c, 0xc6, 0xce, 0xde, 0xf6, 0xe6, 0x7c, 0x00}
};
const int IMAGES_LEN = sizeof(IMAGES)/8;


void setup(){
  Serial.begin(9600);
  pinMode(trig, OUTPUT);
  for (int thisPin = 8; thisPin <= 12; ++thisPin)
    {
      pinMode(thisPin, OUTPUT);
    }
  lcd.init();                        
  lcd.backlight(); 
  lcd.clear();            
  lcd.createChar(3, strP);
  lcd.createChar(1, rightP);
  lcd.createChar(2, leftP);
  lcd.createChar(4, non);
  lc.shutdown(0, false);// Выключить энергосбережение, включить матрицу
  lc.setIntensity(0, 2);// Устанавлить яркость (0 ~ 15 возможных значений)
  lc.clearDisplay(0);// Очистить матрицу
  lc.setRow(0, 0, B11111111);
  lc.setRow(0, 1, B11111111);
  lc.setRow(0, 2, B11100111);
  lc.setRow(0, 3, B11011011);
  lc.setRow(0, 4, B11100111);
  lc.setRow(0, 5, B11111111);
  lc.setRow(0, 6, B11111111);
  lc.setRow(0, 7, B11111111);
  pinMode(4, OUTPUT);
}
    
void loop(){
    static int count = 1;
    static int pos = 0;
    if (digitalRead(17) == 1) {
      ++count;
      lcd.clear();
    }
    
    if (count == 1){
      lcd.clear();
      int line = 0;
      int left = digitalRead(16), right = digitalRead(13);
      Serial.println(pos);
      lcd.setCursor(pos, 0);
      if (left == true and right == true) {
        lcd.noDisplay();
        lcd.display();
        lcd.write(3);
        lcd.setCursor(pos, 0);
        delay(100);
      }
      else if (left == false and right == true and pos < 15) {
        lcd.setCursor(pos, 0);
        lcd.noDisplay();
        lcd.display();
        lcd.write(2);
        ++pos;
        lcd.scrollDisplayRight();
        delay(100);
      }
      else if (left == true and right == false and pos > 0){
        lcd.setCursor(pos, 0);
        lcd.noDisplay();
        lcd.display();
        lcd.write(1);
        --pos;
        lcd.scrollDisplayLeft();
        delay(100);
      }
      else {
        lcd.noDisplay();
        lcd.display();
        lcd.write(3);
        lcd.setCursor(pos, 0);
        delay(100);
      }
    }
    else if (count == 2) {
      lcd.setCursor(0, 0);      // курсор/строка
      lcd.print("Curient t C     "); // Тест на 1-й строке экрана
      lcd.setCursor(5, 1);
      lcd.print("           ");
      lcd.setCursor(0, 1);
      lcd.print(sensors.getTempCByIndex(0)); // Значение t на 2-й строке экрана
      delay(100);
    }
    else if( count == 3) {
      int val = analogRead(7);
      lcd.setCursor(0, 0);      // курсор/строка
      lcd.print("Analog: "); // Тест на 1-й строке экрана
      lcd.setCursor(5, 1);
      lcd.print("           ");
      lcd.setCursor(0, 1);
      val = map(val, 0, 1023, 0, 254);
      if(val < 100) {
        lcd.print("0");
        lcd.setCursor(1, 1);
        if(val < 10) {
          lcd.print("0");
          lcd.setCursor(2, 1);
        }
      }
      analogWrite(3, val);
      lcd.print(val); // Значение t на 2-й строке экрана
      delay(100);
    }
    else if (count == 4) {
      analogWrite(3, 0);
      lcd.setCursor(0, 0);      // курсор/строка
      lcd.print("8x8 DISPLAY: "); // Тест на 1-й строке экрана
      lcd.setCursor(5, 1);
      lcd.print("           ");
      lcd.setCursor(0, 1);
      for(int i = 0; i < 10;++i) {
        delay(200);
         if (digitalRead(17) == 1) {
           ++count;
           lc.setRow(0, 0, B11111111);
           lc.setRow(0, 1, B11111111);
           lc.setRow(0, 2, B11100111);
           lc.setRow(0, 3, B11011011);
           lc.setRow(0, 4, B11100111);
           lc.setRow(0, 5, B11111111);
           lc.setRow(0, 6, B11111111);
           lc.setRow(0, 7, B11111111);
           delay(100);
           lcd.clear();
           break;
         }
         for(int k = 0; k <= 8; ++k) {
            lc.setRow(0, k, IMAGES[i][k]);
         }
         delay(300);
      }
      delay(100);
    }   
    else if (count == 5) {
      Serial.println(sonar.ping_cm());
      unsigned sm = sonar.ping_cm();
      if (sm <= 0)
      {
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        digitalWrite(12, LOW);
      }
      else if (sm > 0 && sm <= 5)
      {
        digitalWrite(8, HIGH);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        digitalWrite(12, LOW);
      }
      else if(sm > 5 && sm <= 10)
      {
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        digitalWrite(12, LOW);
      }
      else if(sm > 10 && sm <= 20)
      {
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, HIGH);
        digitalWrite(11, LOW);
        digitalWrite(12, LOW);
      }
      else if(sm > 20 && sm <= 30)
      {
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, HIGH);
        digitalWrite(11, HIGH);
        digitalWrite(12, LOW);
      }
      else if(sm > 30 && sm <= 40)
      {
        digitalWrite(8, HIGH);
        digitalWrite(9, HIGH);
        digitalWrite(10, HIGH);
        digitalWrite(11, HIGH);
        digitalWrite(12, HIGH);
      }
      lcd.setCursor(0, 0);      // курсор/строка
      lcd.print("Sonar:          "); // Тест на 1-й строке экрана
      lcd.setCursor(0, 1); // курсор/строка
      lcd.print("distance "); // Тест на экране дисплея
      lcd.setCursor(12, 1);
      if (sm < 10) {
        lcd.print("0");
        lcd.setCursor(13, 1);
      }
      lcd.print(sm);
      delay(100);
    }
    else {
      digitalWrite(4, HIGH);
      count = 1;
      delay(100);
      digitalWrite(4, LOW);
    }
}
