#include <GyverTM1637.h>

const int CLK = 10;
const int DIO = 11;
const int KNOB = A0;

GyverTM1637 disp(CLK, DIO); 

void setup() {
  Serial.begin(9600);
  Serial.print("Управление дисплеем с помощью потенциометра");
  disp.clear();
  disp.brightness(7);
}
void loop() {
  int c = (analogRead(A0)/4); 
  disp.displayInt(c);
  delay(5);
}
