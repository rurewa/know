## Arduino Dev Board

Arduino Dev Board - плата для обучения программированию на Arduino.

Представляет собой печатную плату, в которой интегрированны 3 кнопки, потенциометр, 5 светодиодов и RGB светодиод.

Кроме этого в плате предусмотренны разъёмы для подключения датчика температуры, LCD дисплея 1602, мотора постоянного тока, светодиодной матрицы 8х8, ультазвукового сонара.

**Внешний вид платы**

![Logo](/Arduino_dev_board/img/psbNoAssemb.jpg)

![logo](/Arduino_dev_board/img/boardFront.jpg)

![Logo](/Arduino_dev_board/img/boardButtom.jpg)

![Logo](/Arduino_dev_board/img/fillSpring.jpg)

**Схемотехника платы**

![Logo](/Arduino_dev_board/img/schema.jpg)

**Трассировка платы**

![Logo](/Arduino_dev_board/img/pcb.jpg)

**Сборка платы**

![Logo](/Arduino_dev_board/img/assemb.jpg)

**Демонстрация работы**

**Файлы**

[Проект в Kicad](/KiCAD_prj/ForKids/ArduinoDevBoard/2_0/ADB_KiCAD/ArduinoDevBoard_2_1/)

[Примеры кода платы](/Arduino_dev_board/Code/)

### Подробное описание платы

**Интегрированные устройста**

1. RGB светодиод - 1 шт.     Пины: D9-D11
1. Потенциометр 10 К - 1 шт. Пины: A7
1. Кнопки тактовые - 3 шт.   Пины: D13, D16 и D17
1. Buzzer - 1 шт.            Пин: D4
1. Преобразователь питания 12->5 V. LM78M05 smd

**Дискретные устройства**

1. Ультразвуковой сонар HC-SR04. Пины: A0-A1
1. Светодиодная матрица на контроллере MAX7219. Пины: D5-D7
1. LCD дисплей, монохромный 1602. Пины: A4-A5
1. Сервопривод SG90. Пины: D3
1. Мотор постоянного тока на 3-6 V. Пины: D2-D3
1. Bluetooth модуль HC-06. Пины: RX/TX
1. Датчие температуры LM35. Пины: A6

### Дополнительные материалы

[Кнструктор led-матриц 8х8](https://amperka.github.io/led-matrix-editor/#0000000000000000)


