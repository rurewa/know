[https://startingelectronics.org/tutorials/arduino/digispark/digispark-linux-setup/](https://startingelectronics.org/tutorials/arduino/digispark/digispark-linux-setup/)

## Работа DigiSpark в Linux (Ubuntu)

### 1. Перед началом убеитесь, что ОС видет подключённую плату DigiSpark, выполнив команду:

`lsusb`

**У меня консоль выдала эту запись. Это значит, что ОС видит подключённую плату**

`Bus 001 Device 025: ID 16d0:0753 MCS Digistump DigiSpark`

### 2. Внесите текущего пользователя в нужную группу командой:

`sudo adduser $USER dialout`

### 3. Создайте новый файл с правилами командой:

`sudo nano /etc/udev/rules.d/49-micronucleus.rules`


**Вставьте в него этот текст:**

[https://gitlab.com/rurewa/know/-/blob/master/ATtny/DigiSpark/49-micronucleus.rules](https://gitlab.com/rurewa/know/-/blob/master/ATtny/DigiSpark/49-micronucleus.rules)

### 4. Обновите правила командой:

`sudo udevadm control --reload-rules`

***Отключите плату DigiSpark от компьютера!***

### 5. Откройте Arduino IDE и выберите из примера скетч Blink

### 6. В настройках Ардуино внесите допланительную ссылку для плат

http://digistump.com/package_digistump_index.json

### 7. В Менеджере Плат установите Digistump AVR Boards, выберите плату Digispark (Default - 16.5MHz) и загрузите скетч

### 8. После приглашения

 Running Digispark Uploader...
Plug in device now... (will timeout in 60 seconds)

подключите плату. Произойдёт загрузка скетча.

Примерно такая запись:

`writing: 70% complete`

`writing: 75% complete`

`writing: 80% complete`

`> Starting the user app ...`

`running: 100% complete`

`>> Micronucleus done. Thank you!`

свитетельствует, что загрузка произошла успешно!