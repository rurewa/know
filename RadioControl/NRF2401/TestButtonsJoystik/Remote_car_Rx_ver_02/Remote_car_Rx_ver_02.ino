#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

const int CSN_GPIO = 9;
const int CE_GPIO = 8;

const int ENA = 10;
const int RIGHT_FORWARD = 4;  // Left side motor forward
const int RIGHT_BACKWARD = 5;  // Left side motor backward
const int LEFT_FORWARD = 6;  // Right side motor forward
const int LEFT_BACKWARD = 7;  // Right side motor backward
const int ENB = 3;

// Hardware configuration
RF24 radio(CE_GPIO, CSN_GPIO);                           // Set up nRF24L01 radio on SPI bus plus pins 7 & 8

const byte ADDRESS[6] = "00001";
unsigned char Received_Command = '0';

const int SPEED_LEFT = 255;
const int SPEED_RIGHT = 255;

void setup() {
  Serial.begin(9600);
  pinMode(ENA, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(RIGHT_FORWARD, OUTPUT);   //left motors forward
  pinMode(RIGHT_BACKWARD, OUTPUT);   //left motors reverse
  pinMode(LEFT_FORWARD, OUTPUT);   //right motors forward
  pinMode(LEFT_BACKWARD, OUTPUT);   //right motors reverse
  radio.begin();
  radio.openReadingPipe(0, ADDRESS);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
  Serial.println("START");
}

void loop() {
  if (radio.available()) {  
    delay(10);
    radio.read(&Received_Command, 1);
    Serial.println(Received_Command);
  }
  /*
  if(Received_Command == 1)            //move forward(all motors rotate in forward direction)
  {
    digitalWrite(LEFT_FORWARD,HIGH);
    analogWrite(ENA, SPEED_LEFT);
    digitalWrite(RIGHT_FORWARD,HIGH);
    analogWrite(ENB, SPEED_RIGHT);
  }
  else if(Received_Command == 2)      //move reverse (all motors rotate in reverse direction)
  {
    digitalWrite(LEFT_BACKWARD,HIGH);
    analogWrite(ENA, SPEED_LEFT);
    digitalWrite(RIGHT_BACKWARD,HIGH);
    analogWrite(ENB, SPEED_RIGHT);
  }
  else if(Received_Command == 3)      //turn right (left side motors rotate in forward direction, right side motors rotates in backward direction)
  {
    digitalWrite(LEFT_FORWARD,HIGH);
    analogWrite(ENA, SPEED_LEFT);
    digitalWrite(RIGHT_BACKWARD,HIGH);
    analogWrite(ENB, SPEED_RIGHT);
  }
  else if(Received_Command == 4)      //turn left (right side motors rotate in forward direction, left side motors rotates in backward direction)
  {
    digitalWrite(RIGHT_FORWARD,HIGH);
    analogWrite(ENA, SPEED_LEFT);
    digitalWrite(LEFT_BACKWARD,HIGH);
    analogWrite(ENB, SPEED_RIGHT);
  }
  else if(Received_Command == 0)      //STOP (all motors stop)
  {      
    digitalWrite(RIGHT_FORWARD,LOW);
    digitalWrite(RIGHT_BACKWARD,LOW);
    digitalWrite(LEFT_FORWARD,LOW);
    digitalWrite(LEFT_BACKWARD,LOW);
  }
  else                                  //STOP (all motors stop) , If any other command is received.
  {
    digitalWrite(RIGHT_FORWARD,LOW);
    digitalWrite(RIGHT_BACKWARD,LOW);
    digitalWrite(LEFT_FORWARD,LOW);
    digitalWrite(LEFT_BACKWARD,LOW);
  }
  */
}
