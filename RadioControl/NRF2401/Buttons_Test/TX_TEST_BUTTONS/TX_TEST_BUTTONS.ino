#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
RF24 radio(9, 10); // CE, CSN
const byte ADDRESS[6] = "00001";     //Byte of array representing the address. This is the address where we will send the data. This should be same on the receiving side.
int LEFT_LEFT_BUTTON_PIN = 6;
int RIGHT_LEFT_BUTTON_PIN = 4;

void setup() {
  Serial.begin(9600);
  radio.begin();                  //Starting the Wireless communication
  radio.openWritingPipe(ADDRESS); //Setting the address where we will send the data
  radio.setPALevel(RF24_PA_MAX);  //You can set it as minimum or maximum depending on the distance between the transmitter and receiver.
  radio.stopListening();          //This sets the module as transmitter
}
void loop() {
  bool leftButtonState = digitalRead(LEFT_LEFT_BUTTON_PIN);
  bool rightButtonState = digitalRead(RIGHT_LEFT_BUTTON_PIN);

  if (leftButtonState == HIGH) {
    leftButtonState == HIGH;
    //const char text[] = "Left button ON";
    const char text[] = "L";
    radio.write(&text, sizeof(text));                  //Sending the message to receive
    //Serial.println("Left button ON");
    Serial.print(1);
  }
  else if (rightButtonState == HIGH) {
    rightButtonState == LOW;
    //const char text[] = "Right button ON";
    const char text[] = "R";
    radio.write(&text, sizeof(text));                  //Sending the message to receiver
    //Serial.println("Right button ON");
    Serial.print(2);
  }
  else {
    //const char text[] = "Buttons OFF";
    const char text[] = "O";
    radio.write(&text, sizeof(text));                  //Sending the message to receiver
    //Serial.println("Right button ON");
    Serial.print(0);
  }
  radio.write(&leftButtonState, sizeof(leftButtonState));  //Sending the message to receiver
  radio.write(&rightButtonState, sizeof(rightButtonState));  //Sending the message to receiver
  delay (50);
}
