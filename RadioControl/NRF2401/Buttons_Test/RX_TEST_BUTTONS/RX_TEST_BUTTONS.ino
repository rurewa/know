#include "nRF24L01.h"
#include "RF24.h"

RF24 radio(8, 9); // CE, CSN
const byte ADDRESS[6] = "00001";

void setup () {
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(0, ADDRESS);
  radio.setPALevel(RF24_PA_MAX);
  radio.startListening();
}

void loop () {
  char text[32] = "";
  bool rightButtonPush = 0;
  bool leftButtonPush = 0;
  while (radio.available()) {
    radio.read(&text, sizeof(text));
    radio.read(&rightButtonPush, sizeof(rightButtonPush));
    radio.read(&leftButtonPush, sizeof(leftButtonPush));
    if (rightButtonPush == HIGH) {
      radio.read(&text, sizeof(text));
    }

    if (leftButtonPush == HIGH) {
      radio.read(&text, sizeof(text));
    }
    Serial.println(text);
  }
}
