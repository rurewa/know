// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// V 1.4
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

const int FREQUENCY = 60;

int pulseWidth(int angle);

void setup() {

  //начинаем общение с ардуино
  Serial.begin(9600);

  //иницилизация i2c
  pwm.begin();
  pwm.setPWMFreq(FREQUENCY);
  // Разгибаем пальцы ладони в начале программы
  pwm.setPWM(0, 0, pulseWidth(10));
  pwm.setPWM(1, 0, pulseWidth(10));
  pwm.setPWM(8, 0, pulseWidth(10));
  pwm.setPWM(9, 0, pulseWidth(10));
  pwm.setPWM(10, 0, pulseWidth(10));
  delay(250);
}

void loop() {

  //pwm.setPWM(1, 0, pulseWidth(100));
  //pwm.setPWM(10, 0, pulseWidth(-10));

  // Сгибаем все пальцы
  /*
  for (int i = 0; i < 120; ++i) {
    pwm.setPWM(0, 0, pulseWidth(i));
    pwm.setPWM(1, 0, pulseWidth(i));
    pwm.setPWM(8, 0, pulseWidth(i));
    pwm.setPWM(9, 0, pulseWidth(i));
    pwm.setPWM(10, 0, pulseWidth(i));
    delay(15);
  }
  // Разгибаем все пальцы
  for (int i = 115; i >= 0; --i) {
    pwm.setPWM(0, 0, pulseWidth(i));
    pwm.setPWM(1, 0, pulseWidth(i));
    pwm.setPWM(8, 0, pulseWidth(i));
    pwm.setPWM(9, 0, pulseWidth(i));
    pwm.setPWM(10, 0, pulseWidth(i));
    delay(15);
  }
  */
}

int pulseWidth(int angle) {
  const int MIN_PULSE_WIDTH = 650;
  const int MAX_PULSE_WIDTH = 2350;
  int pulse_wide, analog_value;
  pulse_wide   = map(angle, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
  analog_value = int(float(pulse_wide) / 1000000 * FREQUENCY * 4096);
  return analog_value;
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
