// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// V 1.4
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <SoftwareSerial.h>

// default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

//номер пальца на каторой идёт приём информации
short finger = 0;

//порты серв
const short fingers [5] = {0, 1, 10, 9, 8};

//максимальное сжатие
const int max = 180;

//минимальное сжатие
const int min = -10;


const int FREQUENCY = 60;

int pulseWidth(int angle);

//пин ардуино к которому присоеденён txd модуля
int gRxPin = 10;
//пин ардуино к которому присоединён rxd модуля
int gTxPin = 11;

//объект bluetooth модуля
SoftwareSerial BTSerial(gRxPin, gTxPin);


void setup() {
  //начинаем общение с модулям блютуз
  BTSerial.begin(9600);
  
  //начинаем общение с ардуино
  Serial.begin(9600);
  
  //иницилизация i2c
  pwm.begin();
  pwm.setPWMFreq(FREQUENCY);
  // Разгибаем пальцы ладони в начале программы
  pwm.setPWM(0, 0, pulseWidth(10));
  delay(1000);
  pwm.setPWM(1, 0, pulseWidth(10));
  delay(1000);
  pwm.setPWM(10, 0, pulseWidth(10));
  delay(1000);
  pwm.setPWM(9, 0, pulseWidth(10));
  delay(1000);  
  pwm.setPWM(8, 0, pulseWidth(10));
  delay(250);
}

void loop() {
  //если что то пришло на блютуз модуль
  if (BTSerial.available()){
    //принятие данных  
    char i = BTSerial.read();

    // получение ключего го символа который отправляеться перед отправкой данных для пальцев
    if (i == -128){
      finger=0;
      Serial.println("a");
    }

    //сжимание пальца и отправка информации об этом
    else{
    //отсылка данных на компьютер
    Serial.println(map(i, -128, 127, 180, 0));
    Serial.println(finger);
    

    //сжимание пальца
    pwm.setPWM(fingers[finger], 0, pulseWidth(map(i, -127, 127, min, max)));

    //переключение на принятие информации для другого паальца
    finger++;
    } 
  }

  // Сгибаем все пальцы
  /*
  for (int i = 0; i < 120; ++i) {
    pwm.setPWM(0, 0, pulseWidth(i));
    pwm.setPWM(1, 0, pulseWidth(i));
    pwm.setPWM(8, 0, pulseWidth(i));
    pwm.setPWM(9, 0, pulseWidth(i));
    pwm.setPWM(10, 0, pulseWidth(i));
    delay(15);
  }
  // Разгибаем все пальцы
  for (int i = 115; i >= 0; --i) {
    pwm.setPWM(0, 0, pulseWidth(i));
    pwm.setPWM(1, 0, pulseWidth(i));
    pwm.setPWM(8, 0, pulseWidth(i));
    pwm.setPWM(9, 0, pulseWidth(i));
    pwm.setPWM(10, 0, pulseWidth(i));
    delay(15);
  }
  */
}

int pulseWidth(int angle) {
  const int MIN_PULSE_WIDTH = 650;
  const int MAX_PULSE_WIDTH = 2350;
  int pulse_wide, analog_value;
  pulse_wide   = map(angle, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
  analog_value = int(float(pulse_wide) / 1000000 * FREQUENCY * 4096);
  return analog_value;
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
