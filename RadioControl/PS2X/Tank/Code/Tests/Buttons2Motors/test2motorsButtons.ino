#include "PS2X_lib.h"  //for v1.6
PS2X ps2x; // Создать объект класса

// в данный момент библиотека не поддерживает контроллеры с возможностью горячего подключения, что означает
// вы всегда должны либо перезапустить Arduino после подключения контроллера,
// или снова вызовите config_gamepad(pins) после подключения контроллера.
int error = 0;
byte type = 0;
byte vibrate = 0;

const int ENB = 3; //
const int ENA = 9; //
//-=-=-=-=-=-=-=-=-=-=-
const int motorIn1 = 4;
const int motorIn2 = 5;
//-=-=-=-=-=-=-=-=-=-=-
const int motorIn3 = 6;
const int motorIn4 = 7;
//-=-=-=-=-=-=-=-=-=-=-
const int LEFT_MOTOR_SPEED  = 255;
const int RIGHT_MOTOR_SPEED = 255;

void setup() {
  Serial.begin(57600);
  delay(200);
  pinMode(motorIn1, OUTPUT);
  pinMode(motorIn2, OUTPUT);
  pinMode(motorIn3, OUTPUT);
  pinMode(motorIn4, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(ENA, OUTPUT);
  analogWrite(ENB, LEFT_MOTOR_SPEED);  //
  analogWrite(ENA, RIGHT_MOTOR_SPEED); //
  //ИЗМЕНЕНИЯ для версии 1.6 ЗДЕСЬ!!! **************ДИАГНОСТИЧЕСКАЯ ЧАСТЬ КОДА*************
  // установочные пин-коды и настройки:  GamePad(clock, command, attention, data, Pressures?, Rumble?)
  error = ps2x.config_gamepad(10, 12, 11, 13, true, true); // Контроль ошибок
  if (error == 0) {
    Serial.println("Found Controller, configured successful"); // Найден контроллер, настройка выполнена успешно
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;"); // Попробуйте нажать все кнопки, X заставит контроллер вибрировать быстрее по мере того, как вы будете нажимать сильнее
    Serial.println("holding L1 or R1 will print out the analog stick values."); // удерживая нажатой клавишу L1 или клавишу R1, вы выведете значения с аналогового джойстика
    Serial.println("Go to www.billporter.info for updates and to report bugs."); // Перейдите по ссылке www.billporter.info для получения обновлений и сообщения об ошибках
  }
  else if (error == 1) {
    Serial.println("No controller found, check wiring");
    // Контроллер не найден, проверьте подключение, смотрите раздел readme.txt чтобы включить отладку
    // Отключи Arduino и контролер PS2, включи Arduino, затем включи PS2 и снова запусти Монитор порта для контроля
  }
  else if (error == 2) {
    Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");
    // Контроллер найден, но не принимает команды.
  }
  else if (error == 3) {
    Serial.println("Controller refusing to enter Pressures mode, may not support it. ");
    // Контроллер, отказывается переходить в режим нажатия кнопок, может не поддерживать его.
  }
  //Serial.print(ps2x.Analog(1), HEX);
  type = ps2x.readType();
  switch (type) {
    case 0:
      Serial.println("Unknown Controller type"); // "Неизвестный тип контроллера
      break;
    case 1:
      Serial.println("DualShock Controller Found"); // Найден контроллер DualShock (Наш контроллер!)
      break;
    case 2:
      Serial.println("GuitarHero Controller Found"); // Найден контроллер Guitar Hero
      break;
  }
}

void loop() {
  //go();
  ps2x.read_gamepad();// Обязательная строка. Она считывает показания с контроллера
  if (ps2x.Button(PSB_PAD_UP)) { // Если клавиша нажата
    Serial.print("The PAD_Up key is pressed: "); // Посылаем сообщение в Монитор порта
    Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC); // Посылаем ещё сообщение в Монитор порта
    go(); // Запускаем функцию движения вперёд
  }
  else if (ps2x.Button(PSB_PAD_LEFT)) {
    Serial.print("The PAD_LEFT key is pressed: ");
    Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
    left();
  }
  else { // Иначе, если ни одна из клавишь не нажата, то
    Serial.println("The keys not pressed"); // Сообщение: Ни одна клавиша не нажата
    Stop();
  }
  /*
    if (error == 1)
    return;
    if (type == 2) {
    ps2x.read_gamepad();
    }
    else {
    ps2x.read_gamepad(false, vibrate);          // считайте показания контроллера и установите большой двигатель на вращение со скоростью "вибрации"
    if (ps2x.Button(PSB_START)) {               // будет иметь значение TRUE до тех пор, пока кнопка нажата
      Serial.println("Start is being held");
    }
    if (ps2x.Button(PSB_SELECT)) {
      Serial.println("Select is being held");
    }
    if (ps2x.Button(PSB_PAD_UP)) {        //will be TRUE as long as button is pressed
      Serial.print("Up held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC);
      go();
    }
    else {
      Stop();
    }
    if (ps2x.Button(PSB_PAD_RIGHT)) {
      Serial.print("Right held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_RIGHT), DEC);
    }
    if (ps2x.Button(PSB_PAD_LEFT)) {
      Serial.print("LEFT held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
    }
    if (ps2x.Button(PSB_PAD_DOWN)) {
      Serial.print("DOWN held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_DOWN), DEC);
    }
    vibrate = ps2x.Analog(PSAB_BLUE); // это позволит установить высокую скорость вибрации двигателя в зависимости от того, с какой силой вы нажимаете синюю кнопку (X)
    if (ps2x.NewButtonState())        // будет равно TRUE, если какая-либо кнопка изменит состояние (вкл. на выкл. или выкл. на вкл.)
    {
      if (ps2x.Button(PSB_L3))    {
        Serial.println("L3 pressed");
      }
      if (ps2x.Button(PSB_R3))    {
        Serial.println("R3 pressed");
      }
      if (ps2x.Button(PSB_L2))    {
        Serial.println("L2 pressed");
      }
      if (ps2x.Button(PSB_R2))    {
        Serial.println("R2 pressed");
      }
      if (ps2x.Button(PSB_GREEN)) {
        Serial.println("Triangle pressed");
      }
    }
    // будет равно TRUE, если кнопка была только что нажата
    if (ps2x.ButtonPressed(PSB_RED))   {
      Serial.println("Circle just pressed");  // Клавиша Круга нажата
    }
    // будет равно TRUE, если кнопка была только что отпущена
    if (ps2x.ButtonReleased(PSB_PINK)) {
      Serial.println("Square just released");  // Клавиша Квадрата нажата
    }
    // это было бы TRUE, если бы кнопка была ТОЛЬКО что нажата ИЛИ отпущена
    if (ps2x.NewButtonState(PSB_BLUE)) {
      Serial.println("X just changed");  // Клавиша Х нажата
    }
    if (ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) // Джойстики работают только при нажатии любого из Stick Values
    {
      Serial.print("Stick Values:");
      Serial.print(ps2x.Analog(PSS_LY), DEC); // Левый джойстик, ось Y. Другие варианты: LX, RY, RX
      Serial.print(",");
      Serial.print(ps2x.Analog(PSS_LX), DEC);
      Serial.print(",");
      Serial.print(ps2x.Analog(PSS_RY), DEC);
      Serial.print(",");
      Serial.println(ps2x.Analog(PSS_RX), DEC);
    }
    delay(50);
    }
  */
}

void go() {
  digitalWrite(motorIn1, HIGH); // HIGH
  digitalWrite(motorIn2, LOW);  // LOW
  digitalWrite(motorIn3, LOW);  // LOW
  digitalWrite(motorIn4, HIGH); // HIGH
}

void left() {
  digitalWrite(motorIn1, LOW);
  digitalWrite(motorIn2, HIGH);
  digitalWrite(motorIn3, LOW);
  digitalWrite(motorIn4, HIGH);
}

void Stop() {
  digitalWrite(motorIn1, LOW);
  digitalWrite(motorIn2, LOW);
  digitalWrite(motorIn3, LOW);
  digitalWrite(motorIn4, LOW);
}
