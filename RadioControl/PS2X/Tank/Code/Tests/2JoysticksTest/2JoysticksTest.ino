#include "PS2X_lib.h"  //for v1.6
PS2X ps2x; // Создать объект класса

// в данный момент библиотека не поддерживает контроллеры с возможностью горячего подключения, что означает
// вы всегда должны либо перезапустить Arduino после подключения контроллера,
// или снова вызовите config_gamepad(pins) после подключения контроллера.
int error = 0;
byte type = 0;
byte vibrate = 0;
int RightX = 0;
int RightY = 0;
int Last_speed = 0;

const int ENB = 3; // Правый мотор
const int ENA = 9; // Левый мотор
//-=-=-=-=-=-=-=-=-=-=-
const int motorIn1 = 4;
const int motorIn2 = 5;
//-=-=-=-=-=-=-=-=-=-=-
const int motorIn3 = 6;
const int motorIn4 = 7;
//-=-=-=-=-=-=-=-=-=-=-
const int LEFT_MOTOR_SPEED  = 255;
const int RIGHT_MOTOR_SPEED = 255;

void setup() {
  Serial.begin(57600);
  delay(200);
  pinMode(motorIn1, OUTPUT);
  pinMode(motorIn2, OUTPUT);
  pinMode(motorIn3, OUTPUT);
  pinMode(motorIn4, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(ENA, OUTPUT);
  //analogWrite(ENB, LEFT_MOTOR_SPEED);  //
  //analogWrite(ENA, RIGHT_MOTOR_SPEED); //
  //ИЗМЕНЕНИЯ для версии 1.6 ЗДЕСЬ!!! **************ДИАГНОСТИЧЕСКАЯ ЧАСТЬ КОДА*************
  // установочные пин-коды и настройки:  GamePad(clock, command, attention, data, Pressures?, Rumble?)
  error = ps2x.config_gamepad(10, 12, 11, 13, true, true); // Контроль ошибок
  if (error == 0) {
    Serial.println("Found Controller, configured successful"); // Найден контроллер, настройка выполнена успешно
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;"); // Попробуйте нажать все кнопки, X заставит контроллер вибрировать быстрее по мере того, как вы будете нажимать сильнее
    Serial.println("holding L1 or R1 will print out the analog stick values."); // удерживая нажатой клавишу L1 или клавишу R1, вы выведете значения с аналогового джойстика
    Serial.println("Go to www.billporter.info for updates and to report bugs."); // Перейдите по ссылке www.billporter.info для получения обновлений и сообщения об ошибках
  }
  else if (error == 1) {
    Serial.println("No controller found, check wiring");
    // Контроллер не найден, проверьте подключение, смотрите раздел readme.txt чтобы включить отладку
    // Отключи Arduino и контролер PS2, включи Arduino, затем включи PS2 и снова запусти Монитор порта для контроля
  }
  else if (error == 2) {
    Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");
    // Контроллер найден, но не принимает команды.
  }
  else if (error == 3) {
    Serial.println("Controller refusing to enter Pressures mode, may not support it. ");
    // Контроллер, отказывается переходить в режим нажатия кнопок, может не поддерживать его.
  }
  type = ps2x.readType();
  switch (type) {
    case 0:
      Serial.println("Unknown Controller type"); // "Неизвестный тип контроллера
      break;
    case 1:
      Serial.println("DualShock Controller Found"); // Найден контроллер DualShock (Наш контроллер!)
      break;
    case 2:
      Serial.println("GuitarHero Controller Found"); // Найден контроллер Guitar Hero
      break;
  }
}

void loop() {
  ps2x.read_gamepad(false, vibrate); // Обязательная строка для считывания сигнала с контроллера
  //go(); // Проверка моторов
  //testJoyst(); // Проверка джойстиков
  if (ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) // Джойстики работают только при нажатии любого из Stick Values
  {
    if (ps2x.Analog(PSS_LY) <= 0) {
      Serial.println("Левый Вперёд!");
      analogWrite(ENA, LEFT_MOTOR_SPEED);  //
      digitalWrite(motorIn1, HIGH); // HIGH
      digitalWrite(motorIn2, LOW);  // LOW
    }
    else if (ps2x.Analog(PSS_LY) > 128) {
      Serial.println("Левый Назад");
      analogWrite(ENA, LEFT_MOTOR_SPEED);  //
      digitalWrite(motorIn1, LOW); // HIGH
      digitalWrite(motorIn2, HIGH);  // LOW
    }
    else if (ps2x.Analog(PSS_LY) == 127) {
      Serial.println("Стоп!");
      analogWrite(ENA, 0);
    }
    else if (ps2x.Analog(PSS_RY) <= 0) {
      Serial.println("Правый Вперёд!");
      analogWrite(ENB, RIGHT_MOTOR_SPEED); //
      digitalWrite(motorIn3, LOW);  // LOW
      digitalWrite(motorIn4, HIGH); // HIGH
    }
    else if (ps2x.Analog(PSS_RY) > 128) {
      Serial.println("Правый Назад!");
      analogWrite(ENB, RIGHT_MOTOR_SPEED); //
      digitalWrite(motorIn3, HIGH);  // LOW
      digitalWrite(motorIn4, LOW); // HIGH
    }
    else if (ps2x.Analog(PSS_RY) == 127) {
      analogWrite(ENB, 0);
    }
    else {
      Serial.println("Стоп!");
      analogWrite(ENA, 0);
      analogWrite(ENB, 0);
    }
  }
  delay(50);
}

void go() {
  analogWrite(ENA, LEFT_MOTOR_SPEED);  //
  digitalWrite(motorIn1, HIGH); // HIGH
  digitalWrite(motorIn2, LOW);  // LOW
  analogWrite(ENB, RIGHT_MOTOR_SPEED); //
  digitalWrite(motorIn3, LOW);  // LOW
  digitalWrite(motorIn4, HIGH); // HIGH
}

void left() {
  analogWrite(ENA, LEFT_MOTOR_SPEED);  //
  digitalWrite(motorIn1, LOW);
  digitalWrite(motorIn2, HIGH);
  analogWrite(ENB, RIGHT_MOTOR_SPEED); //
  digitalWrite(motorIn3, LOW);
  digitalWrite(motorIn4, HIGH);
}

void Stop() { // Стоп оба мотора
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
}

void testJoyst() {
  if (ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) // Джойстики работают только при нажатии любого из Stick Values
  {
    Serial.print("Stick Values:");
    Serial.print(ps2x.Analog(PSS_LY), DEC); // Левый джойстик, ось Y. Другие варианты: LX, RY, RX
    Serial.print(",");
    Serial.print(ps2x.Analog(PSS_LX), DEC);
    Serial.print(",");
    Serial.print(ps2x.Analog(PSS_RY), DEC);
    Serial.print(",");
    Serial.println(ps2x.Analog(PSS_RX), DEC);
  }
}

// Функция из кода робо-футболиста
void Joysticks() { // Левый джойстик вперед/назад. Правый джойстик право/лево
  if ((ps2x.Analog(PSS_LY) > 130) && (ps2x.Analog(PSS_RX) < 130) && (ps2x.Analog(PSS_RX) > 125)) { // Назад
    /*
      DCMotor_3->setSpeed(map(ps2x.Analog(PSS_LY), 130, 255, MIN_SPEED, MAX_SPEED));
      DCMotor_1->setSpeed(map(ps2x.Analog(PSS_LY), 130, 255, MIN_SPEED, MAX_SPEED));
      DCMotor_3->run(BACKWARD);
      DCMotor_1->run(BACKWARD);
      (*/
    analogWrite(ENB, LEFT_MOTOR_SPEED);  //
    digitalWrite(motorIn1, HIGH);
    digitalWrite(motorIn2, LOW);
    analogWrite(ENA, RIGHT_MOTOR_SPEED); //
    digitalWrite(motorIn3, HIGH);
    digitalWrite(motorIn4, LOW);
  }
  if ((ps2x.Analog(PSS_LY) < 125) && (ps2x.Analog(PSS_RX) < 130) && (ps2x.Analog(PSS_RX) > 125)) { // Вперед
    /*
      DCMotor_3->setSpeed(map(ps2x.Analog(PSS_LY), 125, 0, MIN_SPEED, MAX_SPEED));
      DCMotor_1->setSpeed(map(ps2x.Analog(PSS_LY), 125, 0, MIN_SPEED, MAX_SPEED));
      DCMotor_3->run(FORWARD);
      DCMotor_1->run(FORWARD);
    */
  }
  if ((ps2x.Analog(PSS_RX) > 130) && (ps2x.Analog(PSS_LY) < 130) && (ps2x.Analog(PSS_LY) > 125)) { // Вправо
    /*
      DCMotor_3->setSpeed(map(ps2x.Analog(PSS_RX), 130, 255, MIN_SPEED, MAX_SPEED)/SMOOTH_TURNING);
      DCMotor_1->setSpeed(map(ps2x.Analog(PSS_RX), 130, 255, MIN_SPEED, MAX_SPEED)/SMOOTH_TURNING);
      DCMotor_3->run(BACKWARD);
      DCMotor_1->run(FORWARD);
    */
  }
  if ((ps2x.Analog(PSS_RX) < 125) && (ps2x.Analog(PSS_LY) < 130) && (ps2x.Analog(PSS_LY) > 125)) { // Влево
    /*
      DCMotor_3->setSpeed(map(ps2x.Analog(PSS_RX), 125, 0, MIN_SPEED, MAX_SPEED)/SMOOTH_TURNING);
      DCMotor_1->setSpeed(map(ps2x.Analog(PSS_RX), 125, 0, MIN_SPEED, MAX_SPEED)/SMOOTH_TURNING);
      DCMotor_1->run(BACKWARD);
      DCMotor_3->run(FORWARD);
    */
  }
}

void steer() // Функция из кода Димы
{
  RightX = ps2x.Analog(PSS_RX);
  RightY = ps2x.Analog(PSS_RY);
  int Speed = constrain(RightY - 127, -127, 127);
  int Speed_left  = -constrain(((Speed + (RightX - 128))), -127, 127);
  int Speed_right = -constrain(((Speed - (RightX - 128))), -127, 127);
  int Speed_absL = abs(Speed_left);
  int Speed_absR = abs(Speed_right);
  analogWrite(ENB, Speed_absL);  // ENB
  analogWrite(ENA, Speed_absR); // ENA
  digitalWrite(motorIn1, constrain(map(Speed_right,  -1, 1, 0, 1), 0, 1));
  digitalWrite(motorIn2, constrain(map(Speed_right,  -1, 1, 1, 0), 0, 1));
  digitalWrite(motorIn3, constrain(map(Speed_left,  -1, 1, 1, 0), 0, 1));
  digitalWrite(motorIn4, constrain(map(Speed_left,  -1, 1, 0, 1), 0, 1));
  Last_speed = Speed;
  delay(100);
}
