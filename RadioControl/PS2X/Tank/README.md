## Проект управления моделью танка с конроллером PS2X

![PS2X](/RadioControl/PS2X/Tank/Images/i.webp)

### Наигация по проекту

[Назначение элементов управления на контроллере](/RadioControl/PS2X/Tank/Controller.md)

[Подключение радиокодуля к Arduino](/RadioControl/PS2X/Tank/ConnectToArduino.md)

[Исходные коды](/RadioControl/PS2X/Tank/Code/)

[Старые исходные коды от другого](https://gitlab.com/urnumra/robo12)

[Ещё старые коды](/home/user/some_rep/know/CARrobo/RadioControl/NRF2401/Tank_2WD/)

### Библиотека Arduino-PS2X

[Официальный репозиторий библиотеки](https://github.com/Lynxmotion/Arduino-PS2X)

[RC Car 4WD Stick PS2 Wireless](https://www.dickybmz.com/2018/06/rc-car-4wd-stick-ps2-wireless.html)

[Плавающий контакт: ложное нажатие кнопки](https://arduino.ru/forum/pesochnitsa-razdel-dlya-novichkov/plavayushchii-kontakt-lozhnoe-nazhatie-knopki)

