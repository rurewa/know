### PowerBank для мобильного робота!

#### Два аккумулятора из вейпов в сумме дают ~7,4 вольта, что достаточно для мобильного робота

[3D модели](/chargingModule/3D/)

[Схемы](/chargingModule/Schem/)

[Документы](/chargingModule/Doc/)

![Logo](/chargingModule/img/two_show.png)

![Logo](/chargingModule/img/three_show.png)
### Детали и материалы

[Зарядный модуль](https://aliexpress.ru/item/1005005976542467.html?sku_id=12000035135022810&srcSns=sns_Telegram&businessType=ProductDetail&spreadType=socialShare&tt=MG&utm_medium=sharing)

[Вольтметр](https://aliexpress.ru/item/1005007085889247.html?spm=a2g2w.detail.pers_rcmd.3.2fb57a7e63RAUd&mixer_rcmd_bucket_id=aerabtestalgoRecommendAbV2_testRankingBoostLogRevenue&pdp_trigger_item_id=0_1005005264794046&ru_algo_pv_id=ec771a-476f2e-599dcd-e33adb-1728378000&scenario=pcDetailBottomMoreOtherSeller&sku_id=12000039350022381&traffic_source=recommendation&type_rcmd=core)

Аккумуляторы берутся из вейпов

### Больше фото!

![Logo](/chargingModule/img/1.jpg)

![Logo](/chargingModule/img/2.jpg)

![Logo](/chargingModule/img/3.jpg)