const int arr[] = {3, 5, 6, 9, 10, 11, 12, 13};

void setup() {
  // Настраиваем все светодиоды в режим управления
  for (byte i = 0; i <= 7; ++i) {
    pinMode(arr[i], OUTPUT);
  }
  // Выключаем все светодиоды
  for (byte i = 0; i <= 7; ++i) {
    digitalWrite(arr[i], !LOW);
  }
  Serial.begin(9600); // Для диагностики
}

void loop() {
  static byte leds = 0;
  static int steps = 1;
  Serial.println(leds);
  leds += steps;
  digitalWrite(arr[leds], !HIGH);
  delay(400);
  digitalWrite(arr[leds], !LOW);
  delay(400);
  if (leds <= 0 || leds >= 7) {
    steps = -steps;
  }
}
