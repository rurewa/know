//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Программа Выводящая значение ШИМ с потэцантэометра
// V1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include <Wire.h>
#include <SoftwareSerial.h>
#include "RichUNOTM1637.h"
#include "RichUNOKnob.h"
const int KNOB_PIN = A0;
Knob knob(KNOB_PIN);

const int CLK = 10; // CLK I2C connect
const int DIO = 11; // DIO I2C connect
TM1637 disp(CLK,DIO);

void setup() {
  disp.init(); // The initialization of the display
  disp.point(1); // Turn on the clock point;
}

void loop() {
  int angle = knob.getAngle();
  int brightness;
  /*The degrees is 0~280, should be converted to be 0~7 to control the*/
  /*brightness of display */
  brightness = map(angle, 0, FULL_ANGLE, 0, BRIGHTEST); 
  disp.set(brightness);
  int8_t ListDisp[4] = {8, 8, 8, 8};
  disp.display(ListDisp); // Display array
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILI
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
