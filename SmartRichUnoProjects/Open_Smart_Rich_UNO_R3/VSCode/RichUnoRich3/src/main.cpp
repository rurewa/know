// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Пример применения в программах для Arduino перечислений и структур
// UNO: SDA - A4, SCL - A5
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Wire.h>
#include <TimerOne.h>
#include <SoftwareSerial.h>
#include "RichUNOLM75.h" // Датчик t
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,2);  // Устанавливаем дисплей

LM75 temper;  // Инициализация объекта датчика t

enum {
  button = 3,
};

struct getData {
  float temperature{0};
  bool onClick{0};
};

void setup() {
  Serial.begin(9600);
  Wire.begin();
  lcd.init();
  lcd.backlight();// Включаем подсветку дисплея
}

void loop() {
  getData tem;
  getData but;
  //getData lcdDicp = {tem.temperature = temper.getTemperatue(), but.onClick = digitalRead(button)};
  tem.temperature = temper.getTemperatue();//get temperature
  Serial.print(tem.temperature);
  but.onClick = digitalRead(button);
  Serial.print("; Button click: ");
  Serial.println(but.onClick);
  // Устанавливаем курсор на вторую строку и нулевой символ.
  lcd.setCursor(0, 0); // 0-я ячейка, 2-я строка
  // Выводим на экран количество секунд с момента запуска ардуины
  lcd.print(tem.temperature);
  lcd.setCursor(0, 1);
  lcd.print(but.onClick);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //