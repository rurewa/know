//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
// Программа для вывода текущего времна на дисплей 1637
// V1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
#include "TimerOne.h"
#include "RichUNOTM1637.h"

const bool ON = 1;
const bool OFF = 0;
  
/*
  Пин CLK модуля TM1637 привязан к пину D10 контролера Atmega328
  Пин DIO модуля TM1637 привязан к пину D11 контролера Atmega328
*/
const int CLK = 10;
const int DIO = 11;

/*Создаём объект disp класса TM1637 с указанием пинов подключения*/
TM1637 disp(CLK, DIO);

/*Массив TimeDisp хранит значение выводимое на дисплей*/
int8_t TimeDisp[] = {0x00, 0x00, 0x00, 0x00};

/*Переменые необходимые для работы таймера*/
unsigned char ClockPoint = 1;
unsigned char Update;
unsigned char halfsecond = 0;
unsigned char second;
unsigned char minute = 33;
unsigned char hour = 17;

void setup() {
  disp.init(); // Инициализация дисплея TM1637
  Timer1.initialize(500000); // Установка прерывания для TimerOne с периодичностью 500 миллисекунд
  Timer1.attachInterrupt(TimingISR); //  Подключаем функцию обработчик прерывания для TimerOne: TimingISR
}

void loop() {
  if(Update == ON) { // Обновлять значение на дисплее по состоянию переменной Update
    TimeUpdate();
    disp.display(TimeDisp);
  }
}

void TimingISR() {// Функция счета часов и минут
  halfsecond++;
  Update = ON;
  if(halfsecond == 2) {
    ++second;
    if(second == 60) {
      ++minute;
      if(minute == 60) {
        ++hour;
        if(hour == 24) hour = 0;
        minute = 0;
      }
      second = 0;
    }
    halfsecond = 0;
  }
  ClockPoint = !ClockPoint;
}

void TimeUpdate(void) { // Функция обновления значения выводимого на дисплей
  if(ClockPoint) {
    disp.point(POINT_ON);
  }  
  else {
    disp.point(POINT_OFF);
  }
  TimeDisp[0] = hour / 10;
  TimeDisp[1] = hour % 10;
  TimeDisp[2] = minute / 10;
  TimeDisp[3] = minute % 10;
  Update = OFF;
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILI
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
