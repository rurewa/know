#include <Temperature_LM75_Derived.h>
Generic_LM75 temperature;

void setup() {  
  Serial.begin(9600);
  Wire.begin();
  Serial.println("Включаем встроенный датчик температуры");
}

void loop() {
  Serial.print("Temperature = "); // Печатаем на экране сообщение
  Serial.print(temperature.readTemperatureC()); // Выводим показания датчика t
  Serial.println(" C"); // Печатаем символ Цельсия
  delay(250); // Небольшая задержка для стабилизации
}
