#include <Temperature_LM75_Derived.h>
#include "RichUNOTM1637.h"
Generic_LM75 temperature;
const int CLK = 10;
const int DIO = 11;
TM1637 disp(CLK, DIO);


void setup() {
  Serial.begin(9600);
  disp.init();
  Wire.begin();
}

void loop() {
  Serial.print("Temperature = ");
  disp.display(temperature.readTemperatureC());
  Serial.println(" C");
  delay(250);
}
