//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Программа Выводящая значение ШИМ с потэцантэометра
// V1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include "RichUNOTM1637.h"
const int CLK = 10;
const int DIO = 11;
TM1637 disp(CLK, DIO);


void setup() {
  Serial.begin(9600);
  disp.init();  
}

void loop() {
  const int KNOB = A0;
  int displayPWM = map(analogRead(KNOB), 0, 1023, 0, 255);
  Serial.println(displayPWM);
  disp.display(displayPWM);
  delay(100);  
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILI
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
