//-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Программа управления сенсерными кнопками
// V1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include "RichUNOTM1637.h"
const int CLK = 10;
const int DIO = 11;
TM1637 disp(CLK, DIO);

void setup() {
  Serial.begin(9600);
  disp.init();
}

void loop() {
  const int TOUCH1 = 3;
  const int TOUCH2 = 4;
  const int TOUCH3 = 5;
  const int TOUCH4 = 6;
 
  if (digitalRead(TOUCH1) == true) {
    disp.display(1000);
    Serial.print("Touch 1 ");
    Serial.print(digitalRead(TOUCH1));
}
  if (digitalRead(TOUCH2) == true) {
    disp.display(2000);
    Serial.print("Touch 2 ");
    Serial.print(digitalRead(TOUCH2));
}  
  if (digitalRead(TOUCH3) == true) {
    disp.display(3000);
    Serial.print("Touch 3 ");
    Serial.print(digitalRead(TOUCH3));
}  
  if (digitalRead(TOUCH4) == true) {
    disp.display(4000);
    Serial.print("Touch 4 ");
    Serial.println(digitalRead(TOUCH4));
  } 
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILI
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
