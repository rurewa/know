// You can learn how to turn on and turn off the LED. 
#define LED1 13
#define LED2 8
#define LED3 7
int led[]={LED1,LED2,LED3};

void setup() {
  pinsInit();
}


void loop() {
 
  for(uint8_t i=1;i < 4; i++)
  {
    ledon(i);//turn on LED i
    delay(500);
    ledoff(i);//turn off it.
  }
 
}

//--------------------------------
void pinsInit()
{
  for(unsigned int i=0; i < 3; i++){
	  pinMode(led[i], OUTPUT);
	  ledoff(i+1);
  }
}
//--------------------------------
void ledon(uint8_t num)//num = 1, 2, 3,
{
 if((num > 0) && (num < 4))
	digitalWrite(led[num-1], HIGH);
}
void ledoff(uint8_t num)//num = 1, 2, 3
{
 if((num > 0) && (num < 4))
   digitalWrite(led[num-1], LOW);
}