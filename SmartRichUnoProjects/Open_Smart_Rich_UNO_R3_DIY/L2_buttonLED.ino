//Functions: when you press the button, the corresponding led is on. Otherwise, when release it, the led is off.
// every time you press the button, the buzzer will beep. 

#define BUZZER 6

#define K1 4//
#define K2 3//
#define K3 2//
int KEY[] = {K1, K2, K3};

#define LED1 13
#define LED2 8
#define LED3 7
int led[]={LED1,LED2,LED3};

void setup() {
  // put your setup code here, to run once:
  pinsInit();

}

void loop() {
  // put your main code here, to run repeatedly:
  int button;
  button = get();

  if(button == 1) //if touch the TCH1 area
  {
    ledon(1);
	buzzeron();
	delay(50);
	buzzeroff();
  }
  else if(button == 2) //if touch the TCH2 area
  {
    ledon(2);
	buzzeron();
	delay(50);
	buzzeroff();
  }
  else if(button == 3) //if touch the TCH3 area
  {
    ledon(3);
	buzzeron();
	delay(50);
	buzzeroff();
  }
  else 
  {
	ledoff(1);  
	ledoff(2);
	ledoff(3);
  }
  delay(100);
}

//--------------------------------
void pinsInit()
{
  for(unsigned int i=0; i < 3; i++){
	  pinMode(led[i], OUTPUT);
	  ledoff(i+1);
	  pinMode(KEY[i], INPUT);
      digitalWrite(KEY[i], HIGH);
  }
  pinMode(BUZZER, OUTPUT);
  buzzeroff();
}
//--------------------------------
uint8_t get()                        
{
  for(uint8_t i=0; i < 3; i++){
	  if(!digitalRead(KEY[i])){
		delay(10);
		if(!digitalRead(KEY[i])) return i+1;
	  }
	}
  return 0;
}

//--------------------------------
void ledon(uint8_t num)//num = 1, 2, 3,
{
 if((num > 0) && (num < 4))
	digitalWrite(led[num-1], HIGH);
}
void ledoff(uint8_t num)//num = 1, 2, 3
{
 if((num > 0) && (num < 4))
   digitalWrite(led[num-1], LOW);
}
//--------------------------------
void buzzeron()
{
	digitalWrite(BUZZER, HIGH);
}
void buzzeroff()
{
	digitalWrite(BUZZER, LOW);
}
