const int LMS = 5;//За скорость левый
const int LMD = 2;//HIGH-вперед, LOW-назад
const int RMS = 6;//За скорость правый
const int RMD = 4;
const int SL = 9;//Крайний левый датчик
const int SM = 8;//Центральный датчик
const int SR = 7;//Крайний правый датчик

const int FORW_SPEED_LEFT = 125;
const int FORW_SPEED_RIGHT = 125;
const int BACK_SPEED_LEFT = 100;
const int BACK_SPEED_RIGHT = 100;
const int TURN_LEFT = 35; // Скорость поворота влево
const int TURN_RIGHT = 35; // Скорость поворота вправо

void setup() {
  Serial.begin(9600);
  pinMode(LMS, OUTPUT);
  pinMode(LMD, OUTPUT);
  pinMode(RMS, OUTPUT);
  pinMode(RMD, OUTPUT);
}
/*
  //000 - 
  
  001 - вперёд вправо
  010 - вперёд
  011 - вперёд еле-еле в право
  100 - вперёд влево
  110 - вперёд еле-еле влево
  
  //101 - такой ситеации не будет (наверное)
  //111 -
 */
void loop() {
  //ir();
  bool out2 = !digitalRead(SL), out3 = !digitalRead(SM), out4 = !digitalRead(SR);
  //go(FORW_SPEED_LEFT, FORW_SPEED_RIGHT, 1);
  //back(BACK_SPEED_LEFT, BACK_SPEED_RIGHT, 1);
  //leftTurn(TURN_LEFT, TURN_RIGHT, 1);
  //rightTurn(TURN_RIGHT, TURN_LEFT, 1);
  if (out2 == false && out3 == false && out4 == true) {
    Serial.println("Вправо"); 
    rightTurn(TURN_LEFT, TURN_RIGHT, 0.5);  
  }
  else if (out2 == false && out3 == true && out4 == false) {
    Serial.println("Вперёд");
    go(FORW_SPEED_LEFT, FORW_SPEED_RIGHT, 1);
  }
  else if (out2 == false && out3 == true && out4 == true) {
    Serial.println("Вперёд, право");
    go(FORW_SPEED_LEFT, FORW_SPEED_RIGHT, 1);
    rightTurn(TURN_LEFT, TURN_RIGHT, 0.5); 
  }
  else if (out2 == true && out3 == false && out4 == false) {
    Serial.println("Влево");
    leftTurn(TURN_RIGHT, TURN_LEFT, 0.5);
  }
  else if (out2 == true && out3 == true && out4 == false) {
    Serial.println("Вперёд, влево");
    go(FORW_SPEED_LEFT, FORW_SPEED_RIGHT, 1);
    leftTurn(TURN_RIGHT, TURN_LEFT, 0.5);
  }
}

void ir() {
  Serial.print(!digitalRead(SL));
  Serial.print(!digitalRead(SM));
  Serial.println(!digitalRead(SR));
}

void go(int FORW_SPEED_LEFT, int FORW_SPEED_RIGHT, int TIME) {
  digitalWrite(LMD, LOW); // Вперёд
  analogWrite(LMS, FORW_SPEED_LEFT);
  digitalWrite(RMD, LOW);
  analogWrite(RMS, FORW_SPEED_RIGHT);
  delay(TIME);
}

void back(int BACK_SPEED_LEFT, int BACK_SPEED_RIGHT, int TIME) {
  digitalWrite(LMD, HIGH); // Назад
  analogWrite(LMS, BACK_SPEED_LEFT);
  digitalWrite(RMD, HIGH);
  analogWrite(RMS, BACK_SPEED_RIGHT);
  delay(TIME);
}

void leftTurn(int TURN_LEFT, int TURN_RIGHT, int TIME) {
  digitalWrite(LMD, HIGH);
  analogWrite(LMS, TURN_LEFT);
  digitalWrite(RMD, LOW);
  analogWrite(RMS, TURN_RIGHT);
  delay(TIME);
}

void rightTurn(int TURN_RIGHT, int TURN_LEFT, int TIME) {
  digitalWrite(LMD, LOW);
  analogWrite(LMS, TURN_LEFT);
  digitalWrite(RMD, HIGH);
  analogWrite(RMS, TURN_RIGHT);
  delay(TIME);
}

void STOP(int TIME) {
  analogWrite(LMS, 0);
  delay(TIME);
}
