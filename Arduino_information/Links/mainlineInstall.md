## Установка новых ядер в Ubuntu

### Установка Mainline:

`sudo add-apt-repository ppa:cappelikan/ppa && sudo apt update && sudo apt install mainline`

**Из меню запускаем Mainline и выбираем из списка нужное нам для установки ядро. Нажимаем Установить**
