// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Счётчик с кратным нажатию приращением, ограничителем по количеству приращений и простым "антидребегом"
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>

void setup() {
  Serial.begin(9600);
}

void loop() {
  static int count = 0;
  static bool currient = false;
  static bool previous = false;
  bool buttonState = digitalRead(3);
  while (previous == true && buttonState == false) {
    while (++count) {
      currient = !currient;
      //Serial.println(currient); // Состояние флага кнопки
      Serial.println(count); // Счётчик
      if (count >= 5) { // Ограничение счётчика
        count = 0;
      }
      break;
    }
    break;
  }
  previous = buttonState;
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //

