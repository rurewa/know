#include <Wire.h>                             
#include <LiquidCrystal_I2C.h>        
LiquidCrystal_I2C lcd(0x27, 16, 2); 
      
byte rightP[8] = {
 B01100, 
 B01100, 
 B00000, 
 B01110, 
 B11100, 
 B01100, 
 B11010, 
 B10011
};

byte strP[8] = {
  B00110,
  B00110,
  B01111,
  B01111,
  B01111,
  B01111,
  B00110,
  B00110
};

byte leftP[8] = {
 B00110, 
 B00110, 
 B00000, 
 B01110, 
 B00111, 
 B00110, 
 B01011, 
 B11001
};

byte non[8]{
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};

void setup() {
  Serial.begin(9600);
  lcd.init();                        
  lcd.backlight(); 
  lcd.clear();            

  lcd.createChar(3, strP);
  lcd.createChar(1, rightP);
  lcd.createChar(2, leftP);
  lcd.createChar(4, non);
}

void loop() {
    int pos = 0;
    int line = 0;
    int left = digitalRead(3), right = digitalRead(6);
    int down = digitalRead(4), up = digitalRead(5);
    lcd.setCursor(pos, 0);

    if((down == 1)&&(up == 0)){
      --line;
      Serial.println("Go Down!");
      lcd.noDisplay();
      lcd.display();
      lcd.setCursor(pos, line);
      delay(92); 
    }

    if((right == 0)&&(left == 0)){
      delay(110);
      Serial.println("Standing");
      lcd.noDisplay();
      lcd.display();
      lcd.write(3);
      lcd.setCursor(pos, line);
      delay(92);
    }
  
    if((right == 1)&&(left == 1)){
      Serial.println("Wrong!");
      lcd.noDisplay();
      lcd.display();
      lcd.write(4);
      lcd.setCursor(pos, line);
      delay(92);
    }
    
    if((right == 1)&&(left == 0)){
      ++pos;
      Serial.println("Go Right!");
      lcd.noDisplay();
      lcd.display();
      lcd.write(1);
      lcd.setCursor(pos, line);
      lcd.scrollDisplayRight();
      ++pos;
      delay(92);
    }
    if((left == 1)&&(right == 0)){
      Serial.println("Go Left!");
      lcd.setCursor(pos, line);
      lcd.noDisplay();
      lcd.display();
      lcd.write(2);
      lcd.scrollDisplayLeft();
      --pos;
      delay(92);
    }
}
