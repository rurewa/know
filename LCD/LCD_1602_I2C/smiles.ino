// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Смайлы на дисплее 1602 с I2C
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include "LiquidCrystal_I2C.h"

LiquidCrystal_I2C lcd(0x27, 16, 2);  // устанавливаем адрес 0x27, и дисплей 16 символов в 2 строки (16х2)

uint8_t humanRight[8] = {
  B01100,
  B01100,
  B00000,
  B01110,
  B11100,
  B01100,
  B11010,
  B10011
};

uint8_t humanFront[8] = {
  B01110,
  B01110,
  B00000,
  B11111,
  B01110,
  B01110,
  B01010,
  B11011
};

uint8_t humanLeft[8] = {
  B00110,
  B00110,
  B00000,
  B01110,
  B00111,
  B00110,
  B01011,
  B11001
};
 
void setup() {
  Serial.begin(9600);
  lcd.init();                      // инициализация LCD 
  lcd.backlight();                 // включаем подсветку
  lcd.clear();                     // очистка дисплея
  lcd.createChar(1, humanRight);
  lcd.createChar(2, humanFront);
  lcd.createChar(3, humanLeft);
}
 
void loop() {
  int fi = digitalRead(3), s = digitalRead(4);
  lcd.setCursor(2, 0);
  lcd.write(1);
  lcd.setCursor(1, 0);
  lcd.write(2);
  lcd.setCursor(0, 0);
  lcd.write(3);

  if(fi) {
    lcd.scrollDisplayRight();
  }
  else if(s) {
    lcd.scrollDisplayLeft();
  }
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
