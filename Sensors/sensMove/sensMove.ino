// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Сигнализация на инфракрасном датике движения ПИР.
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
const int pir = 3; // Дачик движения
const int buzzer = 9; // Зуммер

void setup() { 
  pinMode(buzzer, OUTPUT);

}

void loop() {
   if (digitalRead(pir) == true) { // Когда датчик замечает движение
      digitalWrite(buzzer, HIGH); // Зуммер включён 
   }

   if (digitalRead(pir) == false) {
      digitalWrite(buzzer, LOW); // Зуммер выключен
   }
}
