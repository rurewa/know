[33mcommit 0a9c01ae14c32f15a87656d8e9072ebba0d0bbbd[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 13 15:48:27 2021 +0300

    Added KnobBrightDisplay to OpenRightSmart Uno R3

[33mcommit aaec33988a750ab107af6ee0a8752eab90f66aba[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 8 10:58:56 2021 +0300

    Add Balachenkov project for konkurs

[33mcommit 296482cb94b6ddb21930f95d58f52cecf7521505[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 8 10:17:14 2021 +0300

    Add Bashmakov, Polyakov, Dorofeev projects for konkurs

[33mcommit 1c7a4cb4cb858c010507a7111f555b974b9784c4[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 6 17:16:29 2021 +0300

    Add chem trigger to datasheet

[33mcommit b3f9b36e13354a62aee2dbf77ced4f677a60c276[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Oct 4 14:19:25 2021 +0300

    Update NRF24L01_adapter_holder: add lenght

[33mcommit 72518df3840cc9e3efc66ad3e5b89cb97a7db84f[m
Merge: 75f7187 2f8b427
Author: rurewa <rurewa@gmail.com>
Date:   Mon Oct 4 09:45:22 2021 +0300

    Merge branch 'master' of https://gitlab.com/rurewa/know

[33mcommit 75f718728573bf3ea641792611c4aab7d764abe1[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Oct 4 09:45:14 2021 +0300

    Update info PlanetaryScientist module project

[33mcommit 2f8b427a457a69d40e6d801f569bb2e92da6e32b[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Oct 3 13:18:52 2021 +0300

    Update NRF24L01_adapter_holder: add hatch to tech draw

[33mcommit 21abd20c488e827850273219976735baa73df932[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 1 11:29:06 2021 +0300

    Add button project Kicad

[33mcommit 1caf1ab55ba8301c6c411f21ec6dce8f73fbb7b8[m
Author: User2021-afk <you@example.com>
Date:   Fri Oct 1 10:25:43 2021 +0300

    Add clock display for Open_Smart_Rich_UNO_R3

[33mcommit 1b0046329ad7cb80e996487179081fee818a78a2[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Sep 29 20:57:12 2021 +0300

    Update 3D model for NRF24L01_adapter_holder project

[33mcommit 5d177ca9e7512587fbaf3d68b8d74bf81ed8c776[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Sep 29 16:45:37 2021 +0300

    Add stl NRF24L01 for tank 2WD

[33mcommit 7f5616debf8374170ac62aef372c7e75529217dd[m
Author: User2021-afk <you@example.com>
Date:   Wed Sep 29 08:55:41 2021 +0300

    Add temperature display for Open Smart Uno Right project

[33mcommit 3fb3992e0a8b3e04ddc72d9d77de400a9556bbe9[m
Author: Dimarics <dima.sikora.97@mail.ru>
Date:   Wed Sep 29 08:26:42 2021 +0300

    Update NRF24L01 project tank 2WD: add 3D model, restruct folders

[33mcommit 7b2f1539fb08645a0ba195286b70e9d61ec3f94a[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Sep 27 19:00:22 2021 +0300

    Add FreeCad project: add NRF24L01 shield

[33mcommit 3ed976c05daf07dc061546119c402efdbc83d809[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Sep 27 15:00:21 2021 +0300

    Add Freecad project: for NRF24L01 shield module

[33mcommit 6de920281cc02dfb6cc864317f797a5def2a5d67[m
Merge: 1ae064a 98b0ab2
Author: rurewa <rurewa@gmail.com>
Date:   Fri Sep 24 09:50:16 2021 +0300

    Merge branch 'master' of https://gitlab.com/rurewa/know

[33mcommit 1ae064af50840a6b540c12772717caa4e0951da7[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Sep 24 09:50:06 2021 +0300

    Add IR test, infp pdf to HC05_06

[33mcommit 98b0ab2ef26c33389ea0d7a6513636b39d0877dd[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Sep 17 14:36:12 2021 +0300

    Update Power_switch KiCad project: delete capasitor

[33mcommit 83ec5142c16768d768de886a41d834bd6db9f751[m
Merge: 0cb01fa a2f6c7d
Author: rurewa <rurewa@gmail.com>
Date:   Sun Sep 12 10:50:32 2021 +0300

    Merge branch 'master' of https://gitlab.com/rurewa/know

[33mcommit a2f6c7dd050f84609b81956a1dd84bf24cf276b3[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Sep 10 16:26:53 2021 +0300

    Added Clock for Open_Smart_Rich_UNO_R3 project

[33mcommit 0cb01fa15764faa18853df386173c3bde1db7c38[m
Author: User <you@example.com>
Date:   Thu Sep 9 09:53:07 2021 +0300

    Add buzzer, Knob, TuuchDisplay projects for SmartUnoRichR3 board

[33mcommit f0538461b3d6e99004905d25912bc39f7819d1f9[m
Author: User <you@example.com>
Date:   Thu Sep 9 09:53:07 2021 +0300

    Add buzzer, Knob, TuuchDisplay projects

[33mcommit 09ff6050773bed63d792c07d0082fad81238edc7[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Sep 9 09:00:02 2021 +0300

    Add AT mode for BT

[33mcommit 06971a478fe579b7f9e3576b530d6bb2b0bb16f6[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Aug 18 20:04:31 2021 +0300

    Update InstallBoardAttiny.odt: fix minor

[33mcommit 3608d453e01b200abfdd9a468698ff42cc661852[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Aug 18 09:35:19 2021 +0300

    Update prohect for Attiny chips: add README.md

[33mcommit d9d98c4d2b2ae3d3eba6cb269b0874702c4e2560[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Aug 18 09:14:04 2021 +0300

    Update manual for Attiny chips: add new info

[33mcommit f6a59cddfad3dcc4254a838a5393c0c8bec6460d[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Aug 18 08:41:42 2021 +0300

    Add manual for Attiny chips

[33mcommit abac5ec71c0352577fb0c490c4f6e0d63b41ed79[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Aug 5 07:16:53 2021 +0300

    Update Analog/struct_1/struct_1.ino: fix minor

[33mcommit 119601a3e1f85eb9c836b252c9906525d9b0dbe6[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Aug 5 07:16:07 2021 +0300

    Update Open_Smart_Rich_UNO_R3 project: lcd display whitch enum and struct

[33mcommit 7ea5160deb4487501908fb1a3c4589d1243098e8[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Aug 4 17:04:06 2021 +0300

    Add libre to Open_Smart_Rich_UNO_R3

[33mcommit dd9f49ba9f30d10092383b953bf1cbce1233f014[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Aug 4 17:00:53 2021 +0300

    Add example Serial whitch enum and func

[33mcommit e3c020c77c1eed92d7b668ffd27324d7755400b0[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Aug 4 16:57:13 2021 +0300

    Add examples leds whitch enums

[33mcommit 08ddd93b581b4aafa6ec87058b2427fbf25dbe87[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Aug 4 16:48:32 2021 +0300

    Add example whitch struct and enums

[33mcommit 1c4ed52b9d769afb2b96b583204e0bf78c059d16[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 20 08:26:59 2021 +0300

    Update SizeType.ino: add values

[33mcommit 009fddb22cf91bd0e150f352b585b7c97e5ef68c[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 19 18:31:32 2021 +0300

    Update SizeType.ino, example data size info

[33mcommit 47e89142e718f14b138cb001b074ddf77a993e28[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 19 15:23:30 2021 +0300

    Add SizeType.ino: example data size info

[33mcommit 80177dd558033ac732269407da33acb0f842b022[m
Merge: 25b3363 05c5e77
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jul 9 07:04:54 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 25b3363abf15bacc8c2b4146e2766900ef1cef51[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jul 9 07:04:45 2021 +0300

    Add Attiny project

[33mcommit 05c5e77b1de3e69db459c8f593adecebf066d285[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jul 8 14:05:36 2021 +0300

    Update Tank_2WD/NRF24L01: remake control

[33mcommit 3b27d232bfa31b7d9af39d0f1621e4b521678ec8[m
Merge: 2310e50 b7dc7d0
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jul 8 13:31:28 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 2310e50b55ed50984fa53a349f065af11e9a14b7[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jul 8 13:31:20 2021 +0300

    Add NRF24L01 Tanl_2WD project

[33mcommit b7dc7d0c4c2b23232d041a8bdbb4e701ba989bb4[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Jul 6 06:06:59 2021 +0300

    Add RF433 project

[33mcommit 95a986c73311bc64b73deed79fdea70544a907d7[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Jul 4 07:06:44 2021 +0300

    Add RF433 project

[33mcommit 84c03e8b0a7077cad3b48095bd36c5011ee94ca9[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jun 4 06:37:41 2021 +0300

    Added ch340g datasheet

[33mcommit a5058c8d44856a0ee827722cf1b43b7f63a79be7[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun May 30 09:55:05 2021 +0300

    Add datatSheet set

[33mcommit 019f7b29f558a5849556de084bf943792177b2c2[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu May 27 11:03:50 2021 +0300

    Add pins for Open_Smart_Rich_UNO_R3 project

[33mcommit 3b9b69e1c0dc9f3d37d597f8cc8eb172d5adf783[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue May 25 11:50:08 2021 +0300

    Added exampleSimplyLogicalOperations.ino: simply logical operations

[33mcommit da49708b1cb07ae3a4fe36662696c6b2899ac187[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue May 25 11:49:24 2021 +0300

    Added libry ot Fritzing parts

[33mcommit 416cb99f1cd2f3434c59cf4de2d126725a58f5b8[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat May 22 16:59:34 2021 +0300

    Update GIT myLesson

[33mcommit a00dcb19e757e93c0a283f3b0af155d8ea5a546e[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat May 22 13:15:42 2021 +0300

    Added GIT lesson project

[33mcommit 6cb4ef3391733c284d28da37cfbb8ac2dd5a722f[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed May 19 08:54:13 2021 +0300

    Added pot_control.ino

[33mcommit 63c5c52b047efec2753398e73d1d27aaeac269cf[m
Merge: dbea581 0e9ab40
Author: Alex <rise.sp@gmail.com>
Date:   Wed May 19 08:53:15 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit dbea581d5d9b39580974aec0dd812642f3a1df7b[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed May 19 08:52:42 2021 +0300

    Update AllTests.cpp: add complex expression

[33mcommit 0e9ab40fd4ff13e7fb9d1842f60dea55307bac91[m
Merge: b4a281f 67824a2
Author: rurewa <rurewa@gmail.com>
Date:   Tue May 18 11:45:33 2021 +0300

    Merge branch 'master' of http://github.com/rurewa/know

[33mcommit b4a281f70bdaab1042c64150e102eccc7907ca04[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue May 18 11:45:28 2021 +0300

    Update InfraredControl.ino Dima S.

[33mcommit 67824a2006ef5df20049b4e00c8a5e91cb9d3024[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue May 18 06:14:55 2021 +0300

    Added to FritzingParts components

[33mcommit 1b356000e2b9bfd9730fa64e8d42a2a8d3ffc8e0[m
Merge: 08490f9 553fdcd
Author: Alex <rise.sp@gmail.com>
Date:   Sat May 15 04:36:42 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 553fdcd43cbea18d32d5bc6700a575f3b013a306[m
Author: filipp34 <you@example.com>
Date:   Fri May 14 17:50:29 2021 +0300

    Added InfraredControl

[33mcommit 08490f9a72ca7aa8524efe0e21ee609febe435c7[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Apr 26 08:59:17 2021 +0300

    Added example project for my book

[33mcommit 05ed06f99ec1940424788afa299c2939cf6b919f[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 23 17:14:40 2021 +0300

    Fix: added sensors.begin() MenuLCDdisplyThreeButtons project

[33mcommit 3b753fc9c30c2a3bf9d65e6989780abaf58051c8[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Apr 21 17:20:02 2021 +0300

    Update BiathlonSimply_20mm_12V to Egor RoboCar project

[33mcommit 5cb586a3856bda2d2e84cc32bfe5914648b4362f[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Apr 21 12:55:51 2021 +0300

    Added programm BiathlonSimply_20mm_12V to Egor RoboCar project

[33mcommit 0240d17d4de4313b5c87c177de480cf5490a0bd3[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Apr 20 16:42:13 2021 +0300

    Fix child_bases_2 projectL: remove numbers a task

[33mcommit a523d4da1d41683166d356e6938552c460aad0b3[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Apr 20 12:03:42 2021 +0300

    Update MenuLCDdisplyThreeButtons project: added functions

[33mcommit 22d3c130c3f2165421da1aa6d5e147a69c3b34d5[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Apr 20 11:08:14 2021 +0300

    Update MenuLCDdisplyThreeButtons project

[33mcommit 00745da158daf1d9e83ff6dd081583e02e043138[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 16 10:55:51 2021 +0300

    Added MenuLCDdisplyThreeButtons for ADB 2

[33mcommit 59048cfd2986d226e53ab2eba0cd60fd969ccf42[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 16 10:14:41 2021 +0300

    Fix LedsButtomTwoControlWhitchCountLimit project: > limit

[33mcommit 2bc3aa0322bbf2a3bf0b0d4cb996b1ed30da90c5[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Apr 15 16:06:54 2021 +0300

    Update child bases 2

[33mcommit e89ca5c9422d6ce842db47d4fbae2448966c28d9[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Apr 15 15:58:12 2021 +0300

    Update Egor roboCAR KegelRingSymply 12V

[33mcommit a2ef6bbcbeee8b8881505f114f291b38eda372e0[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Apr 13 11:42:40 2021 +0300

    Added Pedagogy and Education magazine

[33mcommit 182d073f6c4bbfd92adb3fa7f050695886462913[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 9 13:34:34 2021 +0300

    Rename EgorDor KegelRingSymple to KegelRingSymple12V project

[33mcommit d1ea73d051430ff64989286a7541a2ad105e01bb[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 9 13:32:46 2021 +0300

    Added EgorDor KegelRingSymple7_4V project

[33mcommit 1c393e575e82b57eb5a147ddf61ab4a5a1826e17[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 9 13:29:54 2021 +0300

    Update EgorDor KegelRingSymple 12V

[33mcommit 97c2ac360f4f16d0fbdd5dd0287cf4f16f51df9f[m
Author: User2021-afk <davidmatry2007@gmail.com>
Date:   Fri Apr 9 10:16:22 2021 +0300

    LedsButtomTwoControlWhitchCountLimit

[33mcommit 393b07d9912d90b44c05a365ea33058a215c4572[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Apr 8 15:51:36 2021 +0300

    Restore AllTests.cpp for ADB 2

[33mcommit 03baa5cf6e78eefbb180e0e85eaadf2fab6908da[m
Merge: 0123263 639050a
Author: Alex <rise.sp@gmail.com>
Date:   Thu Apr 8 10:17:30 2021 +0300

    gMerge branch 'master' of https://github.com/rurewa/know

[33mcommit 012326353fd23fe48b8b7d17e726a752395e9350[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Apr 8 10:17:26 2021 +0300

    Added info.md file

[33mcommit 639050aac833ff96e5e33b57f111e044d37b5fd4[m
Author: User2021-afk <davidmatry2007@gmail.com>
Date:   Thu Apr 8 09:52:41 2021 +0300

    Added Leds Buttom Two Control Whitch Count Limit project for ADB 2

[33mcommit 1834afd9ae7b8053c6f4c398925ef8a9e0a877ae[m
Merge: 51f7418 1a1cdc3
Author: User2021-afk <davidmatry2007@gmail.com>
Date:   Thu Apr 8 09:48:15 2021 +0300

    Merge branch 'master' of http://github.com/rurewa/know

[33mcommit 1a1cdc3f1cddfe143a10ed49b0e5ca922b29f2e4[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Apr 7 12:40:15 2021 +0300

    Update Egor robotCar KegelRingSymple V 1.7  project

[33mcommit 61b96f9acbeb549830be7162b69a1b11e4c8f126[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Apr 7 12:35:58 2021 +0300

    Update Egor robotCar KegelRingSymple V 1.6  project

[33mcommit 2d398d21331f81d7c955096ebdaba6c1660cdd54[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Apr 7 11:56:21 2021 +0300

    Update Egor robotCar KegelRingSymple project

[33mcommit 51f74184ed34af67918a8cd685fd67f7624122ac[m
Merge: cd815ee 5bbe70d
Author: User2021-afk <davidmatry2007@gmail.com>
Date:   Tue Apr 6 16:21:30 2021 +0300

    Merge branch 'master' of http://github.com/rurewa/know

[33mcommit 5bbe70d003e82bdceca58966708c2d2ff29eb546[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Apr 6 11:21:52 2021 +0300

    Update Counter two button project

[33mcommit cd815ee1e62552aeb12f606977043c21d6194884[m
Merge: adc0f4d f2a8d08
Author: User2021-afk <davidmatry2007@gmail.com>
Date:   Mon Apr 5 17:16:01 2021 +0300

    Megre

[33mcommit adc0f4da63b95e62ba1f0a0e66311a490ba2024b[m
Author: User2021-afk <davidmatry2007@gmail.com>
Date:   Mon Apr 5 17:14:29 2021 +0300

    Deleted

[33mcommit f2a8d08de61a9d8c417b9242df16fd49e2afb36d[m
Merge: a99b700 d0cb8d0
Author: Alex <rise.sp@gmail.com>
Date:   Mon Apr 5 17:03:53 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit a99b7007d73b4f8842ad252773099d6b2605dd09[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Apr 5 17:03:32 2021 +0300

    Added Counter two button project

[33mcommit d0cb8d02bc1a7b3c3c1f97f6e4640e84f2fee5ac[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Apr 2 20:54:14 2021 +0300

    Added gif

[33mcommit f8798a191b1a8901eb71b959210cbd3427bfdf20[m
Merge: 5f80003 6d44cd5
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 2 16:44:37 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 5f800034ccfc31513b4746ca63177bb53ec293e6[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 2 16:44:09 2021 +0300

    Added Trarce.png file to NanoMotors project

[33mcommit f98eafeae0d7f97c65a6cf95b31bc06a6780300a[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Apr 2 12:07:41 2021 +0300

    Added Rocket Control project

[33mcommit 6d44cd54f960ee6ee236686464d50890a08f8611[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Mar 31 19:11:18 2021 +0300

    Update EgorDor KegelRingSymple project

[33mcommit 786d5bcb7cc086962c4342104c782d625d8e13ca[m
Merge: 077ffe8 6bb2cac
Author: Alex <rise.sp@gmail.com>
Date:   Fri Mar 19 11:56:06 2021 +0300

    Update Egors roboCar KegelRingSymple 1 project: fix speed move end false watch pins

[33mcommit 077ffe848d4c1b3f1cc9c1e66bf2b2e99399a37b[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Mar 19 11:19:28 2021 +0300

    Update Egors roboCar KegelRingSymple 1: fix speed move and false wath pins"

[33mcommit 6bb2cac720579127a3cc0cc501d63e30c1c36448[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Mar 19 11:19:28 2021 +0300

    Update Egors roboCar KegelRingSymple 1: fix speed move

[33mcommit d04aaa9c0d83e39d1fdeae211d7dd168c06de825[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Mar 18 14:34:16 2021 +0300

    Added KegelRingSymple_2 to Egors roboCar project

[33mcommit ee17015add00aa6e4acb229e489b28786c825b00[m
Merge: b8ab06e 59d52f2
Author: Alex <rise.sp@gmail.com>
Date:   Thu Mar 18 10:54:14 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit b8ab06e62982d32eda668c95f7fd23603e43ba16[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Mar 18 10:54:09 2021 +0300

    Update EgorDor KegelRingSymple robot car

[33mcommit 59d52f2d4350db2fbba4601a44b4083a15652bb2[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Mar 17 20:03:26 2021 +0300

    Added English_Russian dictionary_IT.pdf

[33mcommit c18e71e2e6f05210248b75c40fb4d4f8479f165f[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Mar 17 11:58:48 2021 +0300

    Add git book

[33mcommit 4120d8479feaf3534e64b80cfecda872f50be067[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Mar 17 11:58:21 2021 +0300

    Update chem to Child babes 1, 2 and 3

[33mcommit 21f7283863a76feb0b51865754340e022e800296[m
Merge: 65fb7e1 89f8e47
Author: Alex <rise.sp@gmail.com>
Date:   Tue Mar 16 17:18:37 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 65fb7e19d600b5feddffb6a9d429a3fd710341e1[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Mar 16 16:59:41 2021 +0300

    Update Child_bases_2 peoject: fix 7 part end update pdf file

[33mcommit 89f8e47e9743cc286f6ad08f963d2980212aacd0[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Mar 16 16:59:41 2021 +0300

    Update Child_bases_2 peoject: fix 7 part

[33mcommit 19c1f207bb0b9388d21d3ff1c91cd5a87b7585c2[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Mar 12 10:12:21 2021 +0300

    Added i_rob project: robocar from Iliy Let

[33mcommit 86e58cf6a01ca11b4cefb809be5b3b0215fa0d3e[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Mar 10 09:18:10 2021 +0300

    Added Symbols potenciometer remote wave to LCD1602 project

[33mcommit c52484f00c9f00f5e62cc93bf2faaa053c74d781[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Mar 10 08:23:57 2021 +0300

    Update AllTests.cpp fot ArduinoDevBoard project: tested rgbLed function

[33mcommit ab16efe212d94575cca8805620a0ee7b7f8e4e48[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Mar 9 11:45:07 2021 +0300

    Update AllTests for ArduinoDevBoard project: fix comments 8x8 func matrix, add func RGB led test

[33mcommit 5f0ea3dd60367da8f6d3264368cd10fc35ddde43[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Mar 5 11:22:42 2021 +0300

    Update NanoMotors project: fix pcd board and update schem pdf file

[33mcommit d0cf1ce8885e3931fb5a80aa1206fe860484580a[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Mar 5 09:51:52 2021 +0300

    Fix NanoMotors KiCAD project

[33mcommit 05adde63aea7d5a593200319326f2c757f2c57c3[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Mar 5 09:50:43 2021 +0300

    Update NanoMotors program project: added function end comments

[33mcommit 4feb27030cc8346ad8afe2f5cf5335a87dfe8e0b[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Mar 4 11:46:03 2021 +0300

    Update NanoMotors project: fix D10 D9 pins

[33mcommit dc02c8321a65437d4d12fa5092eabaffb8e78aac[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Mar 3 18:54:36 2021 +0300

    Fix ArduinoDevBoard 2.0 project: add resistor 100 R to knob

[33mcommit 318e5f31067f6cd512531a71cac1266b72a83125[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Mar 1 18:40:18 2021 +0300

    Added kk

[33mcommit ce19901815d78c10a19d578c72b134597ec5c1ab[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Mar 1 16:49:06 2021 +0300

    Update AllTests project fot AdruinoDevBoard 2.0

[33mcommit bf414b58718f61f9bce86cd256166c7313115f76[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Mar 1 16:22:41 2021 +0300

    Added ButtonSeitchCase project

[33mcommit f92801d39e3ec3615a5e05c70726004a53e58458[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Mar 1 16:06:05 2021 +0300

    Remove and update info to Egor robots project

[33mcommit 7cb0130901ed7ab5148d03942311b77ea859cec2[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Feb 26 16:17:22 2021 +0300

    Added pitches.h file to OpenSmartRichUnoR3 project

[33mcommit 29bc57c2f0fe0ce738c8f0d3e14fd5ea8b06dd30[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Feb 26 11:14:05 2021 +0300

    Added krimea project

[33mcommit 2101872b2b3cef0ef70ddc454ba4c6f9a23343db[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Feb 25 08:57:56 2021 +0300

    Update & added files PlanetaryScientist project

[33mcommit ad3404bdb00f2fcc04c67c1129910a386f80db2e[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Feb 25 08:55:16 2021 +0300

    Update & added files HandBot project

[33mcommit af0aaf64be8a23184e52dccccda5def6e52f0c61[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Feb 24 12:34:50 2021 +0300

    Update doc PlatetaryScienist project

[33mcommit f8c09210c6e8b6bae50afc82cf5f1517f13df66e[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Feb 24 12:30:07 2021 +0300

    Update HandBot doc project

[33mcommit a0cb633df7cb3455b48bd97f8d1fb1f1569ff25c[m
Author: Rurewa <rise.sp@gmail.com>
Date:   Sun Feb 21 14:51:09 2021 +0300

    Update text & .fzz files HandBot project

[33mcommit 77afea47e40932da6a8ac365874f7bfb5a8f927a[m
Author: Rurewa <rise.sp@gmail.com>
Date:   Sun Feb 21 14:13:33 2021 +0300

    Update text to HandBot project

[33mcommit 710d39a19ca7ab277ccb4bd52d9ec53cae1cdbff[m
Author: Rurewa <rise.sp@gmail.com>
Date:   Sun Feb 21 14:03:31 2021 +0300

    Added image files to PlanetaryScientist project

[33mcommit 38f541ec601e21644489110e035cd5c79f2f3659[m
Author: Rurewa <rise.sp@gmail.com>
Date:   Sun Feb 21 14:01:35 2021 +0300

    Update PlanetaryScientist: update text end fritzing sketch

[33mcommit 2fbfeed948804f0219697673978686d4d0c07f08[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Feb 19 16:47:33 2021 +0300

    Added file Planetary Scientist project

[33mcommit 5f5009af97d4571fe833d7029aa59512ef30b752[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Feb 19 16:45:48 2021 +0300

    Add info to handBot.odt

[33mcommit 31867066205078014c6dd2139e4a44268fb09ef4[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Feb 19 16:44:33 2021 +0300

    Update ArduinoDevBoard 2: ad .pdf file. Rename title to schem

[33mcommit 704563d4ec6571893d84d5c6d346a1132cff33a3[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Feb 18 17:45:25 2021 +0300

    Update Child bases2  chem project

[33mcommit a041e1e3502df004211ef42f379b40a152426f37[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Feb 17 13:03:52 2021 +0300

    Update Child bases2 project

[33mcommit 4c590de84c87549a886e094d5091188bf9b768c1[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Feb 17 09:26:26 2021 +0300

    Update HandBot project

[33mcommit c611077554bcab3ceba0dd9ce66c578af028bb72[m
Merge: e3b31d0 af07d27
Author: Alex <rise.sp@gmail.com>
Date:   Tue Feb 16 08:48:31 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit e3b31d0355b2ce86cc9bf8a98b99b90d17eaf168[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Feb 16 08:48:26 2021 +0300

    Update workspace OpenSmartRichShield project

[33mcommit af07d270ce7e740517d4f503e65902a05aaa9d57[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Feb 14 18:34:16 2021 +0300

    Update Egor robots KegelRingSimply

[33mcommit eeb6ad1fff2f2779bc161e934d387a1a7f5318ab[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Feb 14 14:12:38 2021 +0300

    Update HandBot project: added joystiks

[33mcommit 4c382031c6392a89ffd431b00a22fa7bf7accff7[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Feb 9 06:26:38 2021 +0300

    Added SwitchCaseIf_2 project

[33mcommit 7734236669ff46fbaa400c0589642ff08a22e472[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Feb 8 07:30:32 2021 +0300

    Added SwitchCaseIf to Arduino project

[33mcommit 4c29c57b0a0f49bdd58e81de2af357d489b8dbc1[m
Author: rurewa <rise.sp@gmail.com>
Date:   Wed Feb 3 13:31:16 2021 +0300

    Update ADB 2.1 end NanoMotors projects

[33mcommit 7257f8baa49bbf0f76c5e4f38b42860f71eb5a1e[m
Author: rurewa <rise.sp@gmail.com>
Date:   Wed Feb 3 06:30:00 2021 +0300

    Update NanoMotors project: .pdf file

[33mcommit 9a22e2df47546020c3e27644186447abc536bfd9[m
Merge: 3df0354 1e68211
Author: rurewa <rise.sp@gmail.com>
Date:   Wed Feb 3 06:26:03 2021 +0300

    Update NanoMotors project: add .pdf file

[33mcommit 3df03548217dec9d409dcf5806eae893458e0307[m
Author: rurewa <rise.sp@gmail.com>
Date:   Wed Feb 3 06:23:30 2021 +0300

    Update NanoMotors project

[33mcommit 1e6821149387ad25071a757fe2a69179a9f6ff48[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat Jan 30 15:12:14 2021 +0300

    Update Egors RoboCasr KegelRingSimoly end pdf file to NanoMotors project

[33mcommit db02f9751162244f0be6b7f65ad1c0d9cfd97448[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jan 28 14:06:30 2021 +0300

    Update Egors SportCarRobot KegelRing simply project

[33mcommit 43acdd8d43ebf98db227f34a04774ebaa25856f5[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jan 28 09:49:52 2021 +0300

    Create .pdf output file from AdruinoDevBoard 2.1 end create folder PlanetaryScientist project

[33mcommit 123d55b897dc3f5c99e869452af37d621675c69b[m
Author: rurewa <rise.sp@gmail.com>
Date:   Tue Jan 26 15:15:35 2021 +0300

    Update ArduinoDevBoard 2.1. Upgrade pcb

[33mcommit 20179ea27e045eb62d2d247297bb978f0d4a3fbc[m
Author: rurewa <rise.sp@gmail.com>
Date:   Tue Jan 26 08:27:22 2021 +0300

    Update ArduinoDevBoard 2.1 project

[33mcommit fe51da4b6ddb3215c5b86832f49b74c921ff87c8[m
Merge: 92bfa3b c680898
Author: rurewa <rurewa@gmail.com>
Date:   Sun Jan 24 19:41:26 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit c680898886e3074890a7fed2754ad77157c5e98b[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Jan 24 18:58:22 2021 +0300

    Update Egors roboCar KegelRingSimply

[33mcommit 7742a89b2c08a79b386848fcfffe035e16db9f03[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jan 22 12:02:49 2021 +0300

    Update Egors car KegelRingSimply: fixes

[33mcommit 0f4e2b590f9543f09277e52495ca324bea9d9cca[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jan 22 11:30:49 2021 +0300

    Update Egors robo-cars KegelRingSimply: fix speed for turn and move track

[33mcommit 43f3652a2217ec071a37a1d97903f1ba05daf101[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jan 22 10:43:07 2021 +0300

    Update Egor cars KegelRingSeple: added traking black line back

[33mcommit 851ad6e0e078ce4245fd5cfa45a6d883f5b4f8d4[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jan 22 09:33:06 2021 +0300

    Update Egor cars KegelRingSeple: added black line traking back

[33mcommit 92bfa3bf6ed0969faac8467854379236b0d71ca9[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jan 22 07:58:42 2021 +0300

    Added additional Information for RoboCar KegelRing

[33mcommit 1a9d71bbc7f1dee52c8938d628c19fbc19698a26[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jan 21 22:05:11 2021 +0300

    Added Ilia project for KegekRing

[33mcommit 1b516335dac8cae6e6adb788f8b1d680b186f2df[m
Merge: 3cf32ba 47effd9
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jan 21 06:53:33 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 3cf32bad283a06ce0762a6788937e554df7303a7[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jan 21 06:53:25 2021 +0300

    Added KegelRing info-files

[33mcommit 47effd94d7ae1b23e62e77a25cb7a53b23a309df[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Jan 20 17:49:19 2021 +0300

    Update Egor Robo Cars KegelRingSimply

[33mcommit 632361edc90e79984ab4fae0cbf09d96dec4c0ff[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Jan 20 11:57:52 2021 +0300

    Update Arduino Dev Board 2.0 KiCAD project: A6 -> sens t. D2 - free

[33mcommit 03a8308a8ef5754a10814973fe06679e6fca58e8[m
Merge: c82176e a174db3
Author: rurewa <rurewa@gmail.com>
Date:   Sat Jan 16 08:37:03 2021 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit c82176edf2b95c0b353a291ad2f2afc951b9a8ac[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat Jan 16 08:36:55 2021 +0300

    Delete KiCad.gitignore

[33mcommit a174db36dd2e06650e80fae277fb26ae58e9d3a1[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Jan 15 11:50:33 2021 +0300

    Update Egor robot cars: KegelRingSypmle

[33mcommit adc3cd506313a9783a2da6158ac2eb3f4cbd13c3[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Jan 14 10:48:18 2021 +0300

    Update Egor cars KegelRingSymple.cpp

[33mcommit 7de722d08d51ea426f15f8bf05e2d8a0c46f0556[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 30 11:45:29 2020 +0300

    Update Arduino Dev Board 2.1 fix resistor 4.7K

[33mcommit f275148ec8607108aac0c6e4a7906a09b0339757[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 30 08:54:45 2020 +0300

    Update GreenHouse project

[33mcommit 20647a871a028f56334c50bed793bc5684786181[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 30 06:26:45 2020 +0300

    Added module to Fritzing

[33mcommit fd672c02e070c4ac622853eb20353c745f923a31[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Dec 29 11:03:01 2020 +0300

    Update GreenHouse project: added components

[33mcommit c8973071ad12d16bddd44affade21fbfd3f540d8[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Dec 29 11:00:50 2020 +0300

    Added Fritzing parts folder

[33mcommit 4e5fadc7ec905cd881d39ba619777ae6de3a54e1[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Dec 29 07:51:26 2020 +0300

    Added Greenhouse project

[33mcommit 3c508d832f7e6c737e8ea217ef0fd951b5fd578b[m
Merge: baa0ded d9d2ef2
Author: rurewa <rurewa@gmail.com>
Date:   Tue Dec 29 06:14:22 2020 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit baa0ded2e0e0b36b1f665f21afbddf90984c1678[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Dec 29 06:14:17 2020 +0300

    Update Arduino Dev Board 2.1

[33mcommit d9d2ef2a4a9ef75ee2e58756db63c852259feea0[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Dec 28 11:32:38 2020 +0300

    Added to Arduino Dev Board V2 motor, ds18b20 modules

[33mcommit cf31475dbfb9162d90317d5596368f7f1c756019[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat Dec 26 11:31:19 2020 +0300

    Update KiCAD project Arduino Dev Board 2.1

[33mcommit 1ade2d4c400362d6b2f0ed883f8481b83192e749[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Dec 25 12:17:26 2020 +0300

    Added All Tests for Arduino Dev Board V.2

[33mcommit 6d5cb371eb4ddc05ac6581bfdbea30114fad8f8d[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 23 07:54:06 2020 +0300

    added ex1 KiCAD project

[33mcommit a44421deb8dc3a61c49a53f6ced25d614cc2f680[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 16 15:10:48 2020 +0300

    Add .gitignore file

[33mcommit 54029b6b5eee6d027df6df23a2a0c0b5f604e8fb[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 16 15:03:39 2020 +0300

    Added .pdf file to NanoMotors project

[33mcommit 3a1a50a6b7b734f17cad9822708157aed4aed83c[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 16 15:02:27 2020 +0300

    Update Nano Motors project

[33mcommit d24ea57951ebcf51283bc3b710dc4d5fc21ce45e[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 16 15:01:15 2020 +0300

    Add .pdf file to Child babes 2 project

[33mcommit a24b1092ecf6469fabda6c6b301d1272a876e418[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 16 14:59:43 2020 +0300

    Update AC_DC_Converter chem

[33mcommit dedd62cb12dc671c91c635bd5c3693a63e96d3e0[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 9 11:55:01 2020 +0300

    Added ac/dc converter KiCad project

[33mcommit 386db638a434c00c6dd005944cb7166ca5ceaa3b[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Dec 9 11:53:45 2020 +0300

    Update Ultraviolet KiCad project

[33mcommit 593a0df9ba6b0001ae3d944d4cc2bd60b785ec2c[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 29 20:09:07 2020 +0300

    Added workspace Code Workspace

[33mcommit e6e7c41f238b5521ab31c7fb1bad382912430c8a[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 29 20:07:43 2020 +0300

    Added UltraViolet project

[33mcommit 3a39846912e783acaa011bbe87f24e3a8e5756fd[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Nov 26 16:04:10 2020 +0300

    Update pdf output file for Egors CarRotot project

[33mcommit b60ff20d6f1f47a59f6f2b327e2a8bf0334b33e2[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 25 18:54:53 2020 +0300

    Added pdf file to Egors RoboCar KegelRing project

[33mcommit b34c1b828a811dacde488ef00798c2e9e538fc22[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 25 17:29:11 2020 +0300

    Update .dia for Egors RoboCar, KegelRing

[33mcommit a007eae885f798f8c577b1a5049ee1c5809f7109[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 25 10:13:07 2020 +0300

    Added Max car robot sport

[33mcommit 733d541cced55d4ab6e568093e0ee5fa4518aada[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 25 09:22:45 2020 +0300

    Added Sergy Il. RoboCar sport

[33mcommit ecc051f78956bcdcda70be1a50b16f8050f1786f[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 24 10:56:06 2020 +0300

    Update Led Driver RGB project: fix sintax

[33mcommit 6f612f5c8fd2ab7cf47f71a733efcf6073abde67[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 24 10:52:55 2020 +0300

    Update Led Driver RGB project

[33mcommit 66aa04ccd08a0c258386ac0a714495fe8522e20d[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 24 10:47:36 2020 +0300

    Added RGB Led Driver from Open Smart R3

[33mcommit acd2a5630249d5fb48eb579b4973d36f3cffcd5e[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 23 10:15:41 2020 +0300

    Add project VSCode for Uno Rish R3

[33mcommit 4820f45228c1d55fd364577e74d791ebb137ee22[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 22 09:15:05 2020 +0300

    Update OpenSmartRuchShield project

[33mcommit 222241c3e6d335c9359cacc84953c92cc0232426[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat Nov 21 15:12:48 2020 +0300

    Update Power switch project

[33mcommit cb9d527fc1b82ecaa8cf4c7ca66d8c0c1c1a3ed3[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 20 17:46:23 2020 +0300

    Update Egor robot car KegelRing

[33mcommit 74b9d7aa6417ba2e8b476b5da7d68acbc045e9e2[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 20 12:08:45 2020 +0300

    Update Egor roboCar KegelRing

[33mcommit 65688f7ceea168c75bce7d822c38afc382fadb85[m
Merge: b199e2e a19c1a6
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 20 09:34:18 2020 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit b199e2e80afa78fb60490ce1ed1dfbf2f83b3dba[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 20 09:34:13 2020 +0300

    Update end fix Child babes 1 project

[33mcommit a19c1a6211f11a4781759015738b6a2fc2a77d30[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Nov 19 15:17:10 2020 +0300

    Update Open Smart Rich Shield - new passive buzzer

[33mcommit 7c5ab7d0185d84c83d63e546223eb123cc23f906[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Nov 19 12:38:27 2020 +0300

    Update Egors robot car KegelRingSymply

[33mcommit 6b5d8897bd37e0a1c835b60e3557a34ce392b5a5[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 18 10:40:45 2020 +0300

    Added Stuck Bot

[33mcommit f568b8f2919a976e5f5a4b1e0c52d77f6eed793e[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 18 10:33:26 2020 +0300

    Fix to projects

[33mcommit f8917ff7a1021f75096183d966bd7dc673e900ee[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 18 10:17:33 2020 +0300

    Added Sonar Avoid Obstracles

[33mcommit 147137469637c384ed392140b026ae65bc53b987[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Nov 18 09:57:07 2020 +0300

    Added Simply Servo example

[33mcommit a94a03c17808d104a075ce797d5c8119fa47ab43[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 17 11:55:32 2020 +0300

    Add comments to Biathlon sketch

[33mcommit 3389cd72da2ed8ae83a213cc68d3c399a0e4ee84[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 17 11:44:13 2020 +0300

    Added Biathlon sketch

[33mcommit c6c2b455af3340594358e7d86f521066c9c5aff2[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 17 08:45:39 2020 +0300

    Delete KiCad.gitignore

[33mcommit 8b54037d2b048db186864e588c33db873c04b8f7[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 17 07:02:44 2020 +0300

    Update Child babes 1

[33mcommit 7ba067a205e132446a608bcddd24a8c1291b6925[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 17 06:59:39 2020 +0300

    Added .gitignore

[33mcommit 3cac5731b0b5b59f30862bf527e42a3002e0b4b6[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 16 11:57:56 2020 +0300

    Add Cleaner end LineBlackSensor projects

[33mcommit b809d7c7acf4b519e7e418c03e869f69bcab7fba[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 16 03:59:33 2020 +0300

    Update Arduino information

[33mcommit 0a8ed3de361aa4e447fecad719458e502eabc63e[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 13 13:44:31 2020 +0300

    Added MaxS roboCar project

[33mcommit 6fdc4d70a72e1ca26f67dba9e82929e38ef23f6a[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 13 09:20:08 2020 +0300

    Update Egor roboCar KegelRing

[33mcommit fb6da937c7882a1a6fdbae19a5171ccacc1859fa[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 13 05:27:01 2020 +0300

    Update KiCad child bases 2 project

[33mcommit 8ed126562fd202e3835b92c1a52daa3c1875b862[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Nov 12 07:23:55 2020 +0300

    Update pdf for Child Babes 2 project

[33mcommit f3c230035839f530f3e6b311acc71400d5ab0c71[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 10 17:03:33 2020 +0300

    Update Child Bases 2 project

[33mcommit c2899367697ba63f9a78042133b13d869bb34a1a[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 10 12:09:13 2020 +0300

    Added sketchs

[33mcommit 82cda0fc3155b9dde49e581014a3002efc025d8c[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Nov 10 10:25:11 2020 +0300

    Add IskraJS projects

[33mcommit becfa5bd1e1652740d7263c56fcd10732170e308[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 9 15:15:33 2020 +0300

    Update Open Smart Rich Shield

[33mcommit 7db4662afa4884b649c13c4523e70c5df65cd45b[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 8 21:21:38 2020 +0300

    Added Open Smart Rich UNO R3

[33mcommit b7a33bd1e6e1c81f2df980ddd989f5c3d7c02520[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 8 21:17:19 2020 +0300

    Delete temp files

[33mcommit aea1ae99c53473346080c782699579386aba7664[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 8 21:12:04 2020 +0300

    Delete temp

[33mcommit e0dc37bbc567f61b3b67b87af1ad69841bc3f1ec[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 8 20:48:21 2020 +0300

    Delete ccpp.yml

[33mcommit 11e12d47db119841dad6747cd45cb5fb8535972e[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Nov 8 20:43:15 2020 +0300

    Restore know

[33mcommit 44cacc20b7c5ffef6a8bd5d108563e96d39f98b7[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat Nov 7 17:58:18 2020 +0300

    Added Smart Uno Rich Lite project

[33mcommit 31f3b1cf13ea2a09204104da825b9197cdb1c85d[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 6 12:07:21 2020 +0300

    Update Egor Car sport projects

[33mcommit d332852e872932b7fdb4e4c43ccd7550fcc5c048[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Nov 6 11:33:30 2020 +0300

    Update Egor D. CarRobot  project

[33mcommit f65a9a0cd4eb19d754f86b4c243e3fca2a12f3a5[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Nov 5 11:44:08 2020 +0300

    Added Child babes 3 project

[33mcommit e4d169b90816ea79036f918b579998d8c8e1b2d1[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Nov 5 07:14:41 2020 +0300

    Rerame and moveng file Arduino Dev Board 2.0 project

[33mcommit b840bcfcff25095bb7f10a09993a67fc05a3248c[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 2 19:58:35 2020 +0300

    Update RGB Switch Case Set Night Light project

[33mcommit 627ef3be013acde64e03f40cb8680ac7b9b5c330[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 2 17:29:51 2020 +0300

    Update Fade Amount alternate project

[33mcommit f3241d48332e295148b7333901333cbab7469ade[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 2 17:17:41 2020 +0300

    Update Led Control whitch Pot project

[33mcommit 0d9b08867ea546ec36812a4733ba77dfbc23f027[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 2 09:53:04 2020 +0300

    Update robot Egor D. project and update Hand Bot man project

[33mcommit f9499a2f64acc943d4c3949ca3b1dc79900795bb[m
Author: rurewa <rurewa@gmail.com>
Date:   Mon Nov 2 06:41:53 2020 +0300

    Added Power Switch One Row

[33mcommit 0b6cf2a1a7d8de697e06b28e9f1a3d8ce006dae0[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 30 16:17:29 2020 +0300

    Update child_babes_2

[33mcommit 606b4c31005d249c767561eee6ef35ca55201a42[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 22 11:07:23 2020 +0300

    Update Car Kegel Ring Keyes:298P project

[33mcommit 5864ea2fddb49f87db50f4a255e4ae5089b83ca0[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 22 09:54:31 2020 +0300

    Added Car Kegel Ring project

[33mcommit 796eed19e385b7520756128e2ce8242bd81af883[m
Merge: 6bbcc61 b4d954b
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 22 06:57:28 2020 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 6bbcc615294da3b84674f9bb616a3d6480e1c51f[m
Merge: e2fd861 d7e06a2
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 16 21:11:25 2020 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit b4d954b3602f22db5aa9c4ba36439260f9edf4e7[m
Merge: e2fd861 d7e06a2
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 16 21:11:25 2020 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit e2fd86180d30dca834cf117653d25979d54748a6[m
Merge: b08fff0 7974bc2
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 16 06:57:29 2020 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit d7e06a205cb75625c8b7841e2677fd41d9000ee0[m
Merge: b08fff0 7974bc2
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 16 06:57:29 2020 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit b08fff0f72ae1f8f3f61e50b62c9ba931d19f894[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 16 06:57:23 2020 +0300

    Update Child bases 2 KiCad project

[33mcommit 7974bc2d33d05af142d5b7b4549501550a329905[m
Merge: 35f23c1 070530a
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 15 11:09:06 2020 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 35f23c10df3711e641c109bcc52b9414607a55a4[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 15 11:08:39 2020 +0300

    Update Group Leds For Pot

[33mcommit 070530a5d26deecf0647cfbf20fec31bec349efe[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 15 04:58:36 2020 +0300

    Update child bases 2 project

[33mcommit 977e0bc765ec13f499c0faf7cabe3308f67618e2[m
Merge: 59e3c68 b88c83d
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 15 04:56:12 2020 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 59e3c68d949a89080c66a8f68d22454cdbaa5d10[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 15 04:56:07 2020 +0300

    Update child bases 1 project

[33mcommit b88c83df8362b5bd313fbdc607f465e289e97fd1[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 14 19:11:27 2020 +0300

    Update Child bases 2 project

[33mcommit 614db705f3b2fc9d63a3b1d3e1010ae9a7ffd3be[m
Merge: 58dfae8 6bdde92
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 14 18:45:21 2020 +0300

    Merge branch 'master' of https://github.com/rurewa/know

[33mcommit 58dfae8b52002f14ee04ba0410599158695cb8b3[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 14 18:45:04 2020 +0300

    Update Child bases 2 project

[33mcommit 6bdde921b8f0f42cd6ba18228bb70f6731fd9036[m
Merge: 54d5cc2 b2db704
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 14 14:45:01 2020 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit b2db7045dba06158d3a6fa713aae4abbc99949b6[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 14 12:17:43 2020 +0300

    Added child project bases 2

[33mcommit 9d510e6b8d58fba064658c8496f0b9ebaf52cf66[m
Author: rurewa <rurewa@gmail.com>
Date:   Wed Oct 14 11:52:45 2020 +0300

    Added project for child - bases 1

[33mcommit 3e1324e116ad21220ac1d9be6fdb41b4e204c3b0[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Oct 13 12:11:09 2020 +0300

    Update program Group Leds For Pot

[33mcommit ccb7010c9291388ab11f3daf29d8fa87d72c8dbf[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Oct 13 10:50:53 2020 +0300

    Update README.md file

[33mcommit 54d5cc2e52c92d5313fcd99eab8baa21707545ce[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Oct 12 19:13:39 2020 +0300

    Update GroupLedsforPot.cpp

[33mcommit d4abc247dc240650e198128b2777225ff8ea30ba[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Oct 12 19:13:39 2020 +0300

    Update GroupLedsforPot.cpp

[33mcommit 1ac595920fd9a020dd7090d33b2797e588986422[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Oct 12 19:07:09 2020 +0300

    Added leds group for pot

[33mcommit 66dbaf1e561cf5a36952e272c26e000986eac911[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 9 07:46:31 2020 +0300

    Update file EgorD end Ilia projects

[33mcommit ae33728e94aadab1e74ce25a7525126fe76a090b[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 9 07:00:13 2020 +0300

    Update code SmartBin

[33mcommit a26e699820d4dc4901bafc82e1e1ecfd8f9f132b[m
Author: rurewa <rurewa@gmail.com>
Date:   Fri Oct 9 06:57:42 2020 +0300

    Added and update files for project Egor/Ilia

[33mcommit 5d7ffd0373de3956256be960f1525cbcfd65ebe3[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 8 14:07:35 2020 +0300

    Added readme file for SmartBox project

[33mcommit 5fde8f07e41b62a48cc1fd7bb28b3fa5b9a0386a[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 8 11:30:15 2020 +0300

    Added files for Egor robot

[33mcommit 7afca07d6dd57d5fc53afaecd311a6da1cae71ce[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 8 11:26:14 2020 +0300

    Egor robot

[33mcommit 15a82531fe6893edbeb301383450ebed44be7203[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 8 11:23:52 2020 +0300

    Deleted file car robot Rgor D

[33mcommit b88820a154c1ddaaafe651aa9db69197bb4858a5[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 8 11:19:36 2020 +0300

    Added text & photo for robot car Egor D

[33mcommit 8a9a70a56f7efbe9f3b9f4f29e8696df8a213152[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 8 10:12:51 2020 +0300

    Update Egor car biathllon code

[33mcommit 5b39f7df5e3d5411716f69adee3ab6cf5074539b[m
Author: rurewa <rurewa@gmail.com>
Date:   Thu Oct 8 09:01:34 2020 +0300

    Added Submarine project by Philipp

[33mcommit b0d5eabfe0d3e9952725b11c88f51fcdbd240911[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 7 12:11:29 2020 +0300

    Added robo car bialhlon for Egor D
    
    Added robo car bialhlon for Egor D

[33mcommit 91673aa1784b4c1ba704278b2725fa5d4198790c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 7 12:02:35 2020 +0300

    Create Readme.txt

[33mcommit 1ac9460405225cfc1b7e940a48c2217b638c545c[m
Author: rurewa <rurewa@gmail.com>
Date:   Tue Oct 6 05:55:53 2020 +0300

    Update project Hand Bot. V 1.2/ Added lib end example

[33mcommit 2b2ae25e091d45d897075f25313f3b9ccae4c527[m
Author: rurewa <rurewa@gmail.com>
Date:   Sun Oct 4 08:04:29 2020 +0300

    Added NanoMotors project

[33mcommit b74077a01b5a87c5560682289b25d0283fa621a5[m
Author: rurewa <rurewa@gmail.com>
Date:   Sat Oct 3 07:38:47 2020 +0300

    Add footprints for KiCad

[33mcommit 2e8ea23d54d8d3aa5b8f19881d3fa6be0f8af1d9[m
Author: rurewa <rise.sp@gmail.com>
Date:   Sun Apr 5 08:59:56 2020 +0300

    Update introductory lesson .pdf file

[33mcommit b980d51836ec2758838725cbdfe43eaa7012368c[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Mar 13 13:09:47 2020 +0300

    Add Morse programm

[33mcommit 020b400d39351635a98704cd4269072d2675f67f[m
Author: rurewa <rise.sp@gmail.com>
Date:   Thu Mar 12 06:53:27 2020 +0300

    Remove files Jun

[33mcommit 606d9ed562135e939a56a7b9512728ede6052a61[m
Author: rurewa <rise.sp@gmail.com>
Date:   Thu Mar 12 05:03:44 2020 +0300

    Update  program Leds Group One Wave 2

[33mcommit e1066baedc3e1e4862b1d3d7622c5a3be5509974[m
Author: rurewa <rise.sp@gmail.com>
Date:   Thu Mar 12 04:15:50 2020 +0300

    Update program Leds Group One Wave

[33mcommit 3f6dfed6f332f5285a0a81b6bd92f2b6285abd80[m
Author: rurewa <rise.sp@gmail.com>
Date:   Wed Mar 11 07:01:40 2020 +0300

    Add file for Provate Folder

[33mcommit 754fdd0f2bdeb3b149ecb7275b5fa005fc2378dd[m
Author: rurewa <rise.sp@gmail.com>
Date:   Tue Mar 10 05:56:01 2020 +0300

    Addd program for ArduiniDevBoard 2.0 leds group wave

[33mcommit e4138a33296ec1c478f7c169c190d86804bfe847[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Mar 6 06:00:00 2020 +0300

    Update lesson buttons_control.pdf

[33mcommit 86c577ee7549820182e37f8bed797306eb6c4137[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Mar 6 04:56:18 2020 +0300

    Update lesson ledsWave

[33mcommit 8d0acc5efc211c7f060bb2b35a09408134427d42[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Mar 6 04:53:31 2020 +0300

    Update lesson ledWave

[33mcommit edd6ae2d22b27339f36b242d2f5b5d732b8ec5e1[m
Author: rurewa <rise.sp@gmail.com>
Date:   Mon Mar 2 11:47:06 2020 +0300

    Update file Prilohenie

[33mcommit 795a2ba65534db76dc15f8b698b3d55d42612916[m
Author: rurewa <rise.sp@gmail.com>
Date:   Mon Mar 2 00:11:25 2020 +0300

    Update file buttons_contfol.pdf

[33mcommit d09b7a57c4cf870ad36c9978f834ee9daca9572c[m
Author: rurewa <rise.sp@gmail.com>
Date:   Mon Mar 2 00:04:00 2020 +0300

    Rename file ledsWave.pdf

[33mcommit b76aebc097f50cb03a6d5997937114a9f66bd48b[m
Author: rurewa <rise.sp@gmail.com>
Date:   Mon Mar 2 00:02:55 2020 +0300

    Update files button_contfol and ledsWawe

[33mcommit cc0b09f421b9dff00587ab0b5910e14f58b30599[m
Author: rurewa <rise.sp@gmail.com>
Date:   Sun Mar 1 23:45:20 2020 +0300

    Update file Prilogenie

[33mcommit adf361347b9937d8b8b9b0602ca9e9860a3abf5a[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Feb 28 06:23:16 2020 +0300

    Update file Prilogenie

[33mcommit 99ac56bd3b35d9da06b8fe2a886e7d9a01742e53[m
Author: rurewa <rise.sp@gmail.com>
Date:   Tue Feb 25 09:26:07 2020 +0300

    Delete files for attestat

[33mcommit c33a5e651a8f1eb5085d995581c31a088d7a5401[m
Author: rurewa <rise.sp@gmail.com>
Date:   Sun Feb 23 09:06:52 2020 +0300

    Update end remove files lessons Arduino

[33mcommit 714d706515c725aa63286d6215a32b2a673f7087[m
Author: rurewa <rise.sp@gmail.com>
Date:   Thu Feb 20 06:47:48 2020 +0300

    Add temporary files

[33mcommit 3ef31f0f794d10be1ec8cc42c0303ed8a40b935a[m
Author: rurewa <rise.sp@gmail.com>
Date:   Sun Feb 2 08:46:31 2020 +0300

    Add file sch Arduino Mega

[33mcommit 1fde031393e72728008a8622c69e0abba31dba5a[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Dec 27 15:16:59 2019 +0300

    Update ArduinoDevBoard 2.1 rev  1.0

[33mcommit 90d9b8ca21c3b8871d5a029e36d7875a3955d2f5[m
Author: rurewa <rise.sp@gmail.com>
Date:   Tue Dec 24 14:49:08 2019 +0300

    Update  ArduinoDevBoard 2.1 project

[33mcommit 81847ba6932b06813f7b042113f1cc6f857756c3[m
Author: rurewa <rise.sp@gmail.com>
Date:   Mon Dec 23 04:47:44 2019 +0300

    Update ArduinoDevBoard 2.1 pcb

[33mcommit 088bf46862538205aeb3f1e1ec98e5ea8daacde3[m
Author: rurewa <rise.sp@gmail.com>
Date:   Mon Dec 23 04:12:42 2019 +0300

    Update ArduinoDevBoard 2.1

[33mcommit 6b80b918d177462d32d0381307f7a064fd120c36[m
Author: rurewa <rise.sp@gmail.com>
Date:   Sun Dec 22 07:22:51 2019 +0300

    Add output file .pdf in Arduino Dev Board 2.0

[33mcommit 566ad77b279d0d4db1fc2b322ae61d0f3c84a16b[m
Author: rurewa <rise.sp@gmail.com>
Date:   Sun Dec 22 07:10:32 2019 +0300

    Update ArduinoDevBoard 2.0 in KiCad

[33mcommit 627437139e7153adee073979c85d79a13b0d2267[m
Author: rurewa <rise.sp@gmail.com>
Date:   Sat Dec 21 06:52:50 2019 +0300

    Add project ArduinoDevBoard 2.0 in KiCAD

[33mcommit 5ab4bf5c055461acc91909690fae50af35501c05[m
Author: rurewa <rise.sp@gmail.com>
Date:   Wed Dec 11 15:53:07 2019 +0300

    Add file .fzz for project WaterBot

[33mcommit dc70b9dbba32b392b05768a2ba41b27f067fbac7[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Nov 29 14:12:55 2019 +0300

    Update sketch Kirill project

[33mcommit 834bafb9a544b9ffa19130ccba8e99eb3b5a7046[m
Author: rurewa <rise.sp@gmail.com>
Date:   Fri Nov 29 14:09:26 2019 +0300

    Add picture in Kirill

[33mcommit 135012a9a8cf6d5d7884e08947d7342b0d01e30a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Nov 25 05:50:28 2019 +0300

    Update DevelopBoardKirill.cpp

[33mcommit af4574b9f5bf61d459eca359c4f80986721d2af2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Nov 25 05:48:17 2019 +0300

    Update DevelopBoardKirill.cpp

[33mcommit d8c695aee212b386478711d817647394284f5bc0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Nov 24 11:44:19 2019 +0300

    Create DevelopBoardKirill.cpp

[33mcommit b9caf866935160fa5a5ac96d0096eeca4872d336[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Nov 22 05:47:56 2019 +0300

    Create ccpp.yml

[33mcommit 384332c94d2a554e3f90d695776e21e14497d3b5[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Oct 11 07:04:41 2019 +0300

    Add arduino project SmartBin

[33mcommit f549e11f529bfb08539fd8cf475083e7d7f57408[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Oct 10 15:54:05 2019 +0300

    Add boolean .pdf lesson

[33mcommit e987419539a4a6491056bca47d05b475d35a2afe[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Oct 7 20:26:58 2019 +0300

    Rename file ppt my course

[33mcommit 5388ffc274efd9f976e9bffd7c256c3b1f3f1232[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Oct 7 07:31:40 2019 +0300

    Add pdf file WS

[33mcommit d8d5318acb889ff71300f22705b11804b72fd669[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Sep 24 10:06:59 2019 +0300

    Rename files Button Simple

[33mcommit 009f4b56934a72982e12e6a552335bdd1ce41eed[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Sep 20 08:03:10 2019 +0300

    To add a file buttons control

[33mcommit 743b182f7c6ac905f7695290c41bbd7b82759cd1[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Sep 16 10:13:26 2019 +0300

    Add files comix solders

[33mcommit dc8d9301f098962b04fad88484401bcaea967ee1[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Sep 16 07:26:31 2019 +0300

    Add lesson leds wave pdf file

[33mcommit d4ab5fad531dcdf7b654630ae591f8c55bf5a309[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Sep 12 12:13:42 2019 +0300

    Update project HandBot

[33mcommit 8f35d9b426fa1a195468869e40d7858c5092e947[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Sep 12 12:12:57 2019 +0300

    Update file HandBot.odt

[33mcommit d3b95d5ad780d76b295c0ad2cc190c39910ac866[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Sep 12 11:48:35 2019 +0300

    Add files project HandBot

[33mcommit 2f2d0853218d8288b7f08ca84310366c7b1f07e0[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Sep 12 10:17:12 2019 +0300

    Update sketch HandBot

[33mcommit 0e49d5a16effe0f7d27e5b8a17f3a0f8792b7b7b[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Sep 11 13:32:35 2019 +0300

    Update .pdf file

[33mcommit 840ed3a1fc47eef03c4c65881e0aed08f0f8c679[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Sep 11 13:30:32 2019 +0300

    Add bases Schem for kids

[33mcommit d771cec360d4176256e035eb1800825e73b6b06c[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Sep 8 21:15:54 2019 +0300

    Add files comp

[33mcommit 01d9d7422fe45ef6a8815edc86d4465b46c6eb48[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Sep 8 20:53:54 2019 +0300

    Rename file

[33mcommit eb431c88c2a5f6988c3a1fbcf1fa43574e66a8f2[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Sep 4 11:00:08 2019 +0300

    Update progarm

[33mcommit 20d80b3aab035401cfeb367db57c796b8e989c39[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Sep 3 09:33:24 2019 +0300

    Add file program

[33mcommit 6b5aa0241516ad0e3e023f24ca333f5546ac759c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 30 09:33:05 2019 +0300

    Update HandBot.cpp

[33mcommit 4c9a812d1ad0d53b8e63a3b036c0bb533421584c[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Aug 30 09:25:59 2019 +0300

    Add Hand Bot sketch

[33mcommit 5383c804b87d6524208c70bd9b7ad277ca59e391[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Aug 28 12:32:56 2019 +0300

    Update cpp.pdf and add Program

[33mcommit b28571966784c426d90ff4e8eef10ab894fa24d0[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Aug 17 19:49:05 2019 +0300

    Update and add book Svoren

[33mcommit cfcf5972d0db296441285c7d4c53bf87fe02f987[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Aug 14 09:45:53 2019 +0300

    Add ternar operator lesson pdf

[33mcommit 58d28e9e1d64ad015441ad380e80d89889908b77[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Aug 14 09:44:30 2019 +0300

    Add algoritm lessoon pdf

[33mcommit 3e3ac5b0e67ea622e1df6f59f74749eab0759f7a[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Aug 12 11:29:56 2019 +0300

    Add .png file Grad

[33mcommit 904826257a371cdf04b64f6e48bf046278098252[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Aug 8 19:28:20 2019 +0300

    Add sketch WaterBot

[33mcommit 3a73e1af7bfcdf658b74d646461b00b91827a76f[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Aug 8 19:21:35 2019 +0300

    Add and update sketch Serial

[33mcommit 11d366f051253b3398acee9dc923e5f801dc82ee[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Aug 6 11:18:53 2019 +0300

    Add images 3D for project KiCad

[33mcommit 3a39c9c0daca9b7e13cfa821462f1e3e8e12a674[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Aug 6 10:15:17 2019 +0300

    Add 3D image for Wheel of Fortune project

[33mcommit c49df19790be3129876d888016a003ef9baad2f7[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Aug 6 10:00:41 2019 +0300

    Add and update KiCad projects

[33mcommit 6f55eebfee79bebb91dbeea57ecf050fc98fb257[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Aug 2 10:07:31 2019 +0300

    Move folder links

[33mcommit ef651d5da485604b275da08deddb59e05f639094[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Aug 1 12:46:26 2019 +0300

    Many changes

[33mcommit 6009504bbdf881da34fe22896ebf05f408a7e6e5[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 31 13:51:10 2019 +0300

    Add files carton robots project

[33mcommit 49f5ea649ad109d6de785a7729eaba3f0ce42529[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 31 11:46:38 2019 +0300

    Add files Rand Rows.cpp

[33mcommit fab82fdba465b52b4cf7bac84c4a4ae29d2459ba[m
Merge: a0cb12f d4ca79a
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 31 11:30:25 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit a0cb12f3a893fd6f4f5bf2aa5be3caf859336f13[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 31 11:30:22 2019 +0300

    Add and moving sketch

[33mcommit d4ca79ad1bbcacdb02560de4db0f29476b0a1ecc[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jul 31 08:19:02 2019 +0300

    Update ArrayLedsMatrixRand.cpp

[33mcommit 04d6d2b53eefaf526b748c4b75938906eef24b5e[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 30 14:39:01 2019 +0300

    Update cpp pdf file

[33mcommit 2c96e81a1c5a5213b5e2858d79cff27b8bf0cb36[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 30 14:26:52 2019 +0300

    Add sketch Declaring Initializing multidimensional Arrays

[33mcommit f7c5a51628fbc969f35d4f4e0ad400d75c68c0a7[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 30 11:41:10 2019 +0300

    Update Power Switch project KiCAD

[33mcommit 924b5e05a76b41f926a5fe201c04c75789edd11c[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 30 11:40:44 2019 +0300

    Add sketch Writing To Array Elements Protection

[33mcommit ab56e8c9d5ffcaa0930e2725aee9b332c3cd5b1b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 29 22:41:01 2019 +0300

    Update Led_matrix_MAX7219_test_2.cpp

[33mcommit 623399099f43d70f077aea9d6b983c1f2b5f3b82[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 29 22:40:43 2019 +0300

    Update Led_matrix_MAX7219_test_1.cpp

[33mcommit 15c57bb9743c6233754bb66a17b6893a5fab6c92[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 29 22:40:24 2019 +0300

    Update Led_matrix_MAX7219_test_0.cpp

[33mcommit c3633213cf9d59356383059eafa55807960054f3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 29 22:39:53 2019 +0300

    Update ArrayLedsMatrixRand.cpp

[33mcommit 6929537b1d81616685443b799a1996d5ef1330f1[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 29 22:38:27 2019 +0300

    Add sketch ArrayLedsMatrixRand

[33mcommit a51e93acb19c416e1240e9bcc87325c0dbb7fe94[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 29 21:43:38 2019 +0300

    Add ASCII table

[33mcommit 4ed33468fd6ea24d453728731020d35d456ee3e6[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 29 21:38:43 2019 +0300

    Update sketch WritingToArrayElements

[33mcommit 7e68111c40d50afb02e0a6567fd89b965836c97e[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 29 11:01:57 2019 +0300

    Add and update sketch M7219 and Serial port

[33mcommit 28d631387d8b05c94f747b0ea4ed0970deca864b[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Jul 28 20:17:30 2019 +0300

    Add project Power Switch for Kids

[33mcommit acb12be86b7d19cded995e889a6d70e4d40478e5[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Jul 25 10:21:31 2019 +0300

    Add book RAO C++ djvu format end update sketch example flag 2
    
    A

[33mcommit 5c871606eb4ff07afee353c8c2a5af0e09869af8[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 23 15:51:06 2019 +0300

    Add book Sitharta Rao C++

[33mcommit f9a7e33bbd66801dc068d8b87f6ee8b88268083e[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 23 09:42:50 2019 +0300

    Update Arduino Dev Board

[33mcommit 6da361c7eb14a022d1f507393bee5a5957058c83[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 22 12:57:44 2019 +0300

    Add PCB project Garland NPN

[33mcommit 67072898806590aba59fa24ed34fd5c3a7093a5b[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 22 08:01:28 2019 +0300

    Add ArduinoDevBoard files and book C++

[33mcommit 223de258de9516d672a03aae194a393da9b5226a[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Jul 21 16:37:33 2019 +0300

    Delete and add book Dennis R. C

[33mcommit 83b69cba0b1bf93752fe4620dbbef69c80163ba0[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Jul 21 11:09:07 2019 +0300

    Update Keyes 5 buttons 2

[33mcommit 187f557aef1febde327e37796b400dea4084d1b8[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Jul 21 09:56:54 2019 +0300

    Add book to programming Prata C++

[33mcommit 0ee5da6091ca52b80c0d2dd6e4dbdbf22582b6ec[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 20 22:23:50 2019 +0300

    Add sketch Keyes one clock

[33mcommit 18fd0c7c90b95478fed5f5dc38fb0ca2d2b3bf6f[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 20 21:13:04 2019 +0300

    Add sketch Keyes key 5 buttons

[33mcommit 76cd214524a27f5d1438dc040a17bb23fbc80694[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 20 07:10:46 2019 +0300

    Add arduino notebook

[33mcommit d065640e7a09d9a40c26a8e789f217ffc907beaa[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 20 06:07:26 2019 +0300

    Add programming files

[33mcommit 5473f99fffa420a60649c894a47d4ff45c8a3a12[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 19 13:10:46 2019 +0300

    Add sketch Debounse Robodem 3

[33mcommit 197fbc5d6c7bd5199d5294d24cef821a63110abc[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 19 13:07:08 2019 +0300

    Add book C++ and sketch Debounce robodem 2

[33mcommit 68d2083a8eacb911263d7308bccf85d9a835c4ff[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 19 12:32:16 2019 +0300

    Update sketch Debounce robodem 1

[33mcommit bcac0d4140b459c735573f096c0adf8aa433c1cc[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 19 12:26:46 2019 +0300

    Move end add files debounce robodem 1

[33mcommit 82af84c577bb2e2c1ef2685e8c454564fad486b5[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 19 11:32:56 2019 +0300

    Many changes

[33mcommit eeff228c0acf1483942bcd2314fcfe1752bc36f7[m
Merge: 7d08937 33c080e
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 19 09:17:41 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 7d0893782b65bc74b1ce87e7b818f7a86786ae4e[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 19 09:17:34 2019 +0300

    Add servo bag robot #1

[33mcommit 33c080e1acb46437580bc66a3fc70b99835138c1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jul 19 06:47:58 2019 +0300

    Update Led_matrix_MAX7219_test_2.cpp

[33mcommit 14cb1a949ebc2821be97edeb1298a6cd47e90d16[m
Author: Alex <rise.sp@gmail.com>
Date:   Thu Jul 18 09:53:45 2019 +0300

    Update Balansing robot 1

[33mcommit 1191dea6f4db84c860fd33db95779d7ea6bea30b[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 17 13:10:52 2019 +0300

    Update sketch lesson example flag 2

[33mcommit 9ed21b25d83dd8eace85403e66e37871b663a33a[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 17 11:51:06 2019 +0300

    Update 2  sketch example 2

[33mcommit a4f2c66a50264a00eb8b4ea157dd8d4da5fe686a[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 17 11:47:47 2019 +0300

    Update sketch example flag 2

[33mcommit f5ac7d714e07d10211918307eb1b38d6e7a54323[m
Author: Alex <rise.sp@gmail.com>
Date:   Wed Jul 17 10:49:56 2019 +0300

    Add sketch lesson flag2

[33mcommit f0d6bf8e747f69a2c6163e53d3c47796f1c06a7c[m
Merge: 0f4f9b0 68deea4
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 16 07:53:40 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 0f4f9b05327d4e414ea248744eca265812dec0b3[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 16 07:53:38 2019 +0300

    Add sketch example flags end picture 8x8 led matrix 7219

[33mcommit 68deea48a2c98538f27cee4a8dd59e3439a9a9c3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 15 11:10:01 2019 +0300

    Update RGB_counter_if_switch_case_button_set.cpp

[33mcommit a2cb0c4ce5affa868868b36627ebc285d5a970ba[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 15 10:44:42 2019 +0300

    Update Example.cpp

[33mcommit 85adb1de9306e504620fb1dc0395e433b5323966[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 15 10:43:49 2019 +0300

    Add example sketch 7219 led matrix

[33mcommit 783add9322df2684489259b3a41b46b46ce60a57[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 15 10:35:18 2019 +0300

    Create table_simbols.txt

[33mcommit 84b2a0620ca4bdafcdd8589216637c380e19852e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 13 19:08:35 2019 +0300

    Update RGB_counter_if_switch_case_button_set.cpp

[33mcommit 5cac966ab1fb64a304356a204877afcff0119cf3[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 13 18:45:59 2019 +0300

    Add to lessons schemes tech

[33mcommit aa727bf39de46d0716daf5d350893e06e82c3b7e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 13 11:25:07 2019 +0300

    Update RGB_counter_if_switch_case_button_set.cpp

[33mcommit d473dd0e7effecea65615ba7c195ea1a4cd38be7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 13 11:23:25 2019 +0300

    Update RGB_counter_if_switch_case_button_set.cpp

[33mcommit 85109d07aff002f9da9bbadeb25d2e10fa621149[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 13 10:23:29 2019 +0300

    Delete sketch

[33mcommit bf24c421b2b31c6b3eded90aa6e2ad043c1ff65a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 13 10:20:09 2019 +0300

    Rename RGB_if_switch_case_button_set.cpp to RGB_counter_if_switch_case_button_set.cpp

[33mcommit bff96ad8a75252bd68eab98106718e331403906f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 13 10:19:33 2019 +0300

    Update RGB_if_switch_case_button_set.cpp

[33mcommit 6a9023ece0c0389ddfae21ab85c95c2f9e56c3eb[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 13 09:59:51 2019 +0300

    Update Counter_increment_one_button_set.cpp

[33mcommit 92e7bc414d7294d9fcd7179999fde94f6b118fe8[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 13 09:53:47 2019 +0300

    Rename sketch

[33mcommit 829e11a1f911d8953c58a46fe2b1c34b14f62947[m
Author: Alex <rise.sp@gmail.com>
Date:   Sat Jul 13 09:52:46 2019 +0300

    Add sketch counter increment one button

[33mcommit 076069af7d70748b6f06da29ccd2cf70049a7d3a[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 12 11:50:21 2019 +0300

    Add book programming

[33mcommit 6f3e7bd2887137795f7f557fa3a036d22e1da851[m
Author: Alex <rise.sp@gmail.com>
Date:   Fri Jul 12 11:48:25 2019 +0300

    Add files project balancing robot

[33mcommit 7b7acecba0b5ccc8042a9846a71446e2789b2aed[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 9 14:22:51 2019 +0300

    Update sketch RGB_if_switch_case_button_set.cpp

[33mcommit 66aa391a1a5c3a226aa46fc731c9d5277dbf3a6a[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 9 14:20:40 2019 +0300

    Add sketch RGB if switch case one button set end delete pdf file

[33mcommit f27b677aa5c7ae9e23255fa8f87a85797859b218[m
Author: Alex <rise.sp@gmail.com>
Date:   Tue Jul 9 08:52:09 2019 +0300

    Add book Horovits end Hill Shema

[33mcommit aaae185b984ca2996e67c90cb3c7602a511ec3b3[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 8 10:50:40 2019 +0300

    Add books for programming

[33mcommit 21b9e9cbfce8e59f7c5132436a21942873dde637[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 8 07:43:17 2019 +0300

    Update lesson Algoritms (12)

[33mcommit f621a90f496859960a0d7b2b231854e83f03f49c[m
Merge: 110bae4 fcef3b1
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 8 06:53:49 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 110bae4095cfb2553d187c0fa4fbad11a2261180[m
Author: Alex <rise.sp@gmail.com>
Date:   Mon Jul 8 06:53:43 2019 +0300

    Add book Skvoren

[33mcommit fcef3b1f715be7687735c0304f2abf77d432f08b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 7 22:42:54 2019 +0300

    Update RGB_if_switch_case_buttons_set.cpp

[33mcommit 01e7c1fa930ccd13a7f73cb2b39062dc8bb8bd58[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Jul 7 22:41:48 2019 +0300

    Add RGB sketch

[33mcommit 0d19324f35fd247d54aa0f6e72e85371e6e3ac77[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Jul 7 18:41:26 2019 +0300

    Add book Amperka

[33mcommit c7b78d6f5f769d9189bc1f1f356714de3f606924[m
Author: Alex <rise.sp@gmail.com>
Date:   Sun Jul 7 18:38:35 2019 +0300

    Update 12_algoritm lesson

[33mcommit 1b65bafadd96fd8cfec3a8d16cf1ed43c633dc99[m[33m ([m[1;31morigin/test[m[33m)[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Jul 7 08:42:10 2019 +0300

    Add Program "Sports of Robototechnix"

[33mcommit a1c3b1f8a3c4098cf82ebb28aabbc6d623f17581[m
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Jul 4 20:23:47 2019 +0300

    Add sketch RGB switch case usb 2

[33mcommit 2b40affa1cd360022348593d68556f463d4a2dbe[m
Merge: 8a457f7 e66f033
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Jul 4 19:11:04 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 8a457f7bfd61ae4495c36df11bab5bd82d820bb0[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Jul 2 08:25:37 2019 +0300

    Add RGB switch case usb set

[33mcommit e66f033954f7f82b652c194ddd47380064eb32f5[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Jul 2 08:25:37 2019 +0300

    02/07/2019

[33mcommit 54badea8539e3bcc4a9c7ca7509276ff59e93879[m
Merge: 7ad09ac 82124d5
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Jul 2 07:24:51 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 7ad09ac96c871cc2ca1908b572b285cd83c63d0a[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Jul 2 07:24:48 2019 +0300

    Add lessons 4

[33mcommit 0385a91d650f0c6453300bd618f87a1d3b34d876[m
Merge: 91d3df9 c2c859d
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Jul 2 07:14:26 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 82124d50cf3cdce818f426cdab45d9a9b26e3551[m
Merge: 91d3df9 c2c859d
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Jul 2 07:14:26 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 91d3df9c84224d685e6f8305760b09f80b118bad[m
Merge: 19f5e46 56b2cbe
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Jul 1 06:41:29 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit c2c859d3e27428d77ce968aa14b8729123c3b74e[m
Merge: 19f5e46 56b2cbe
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Jul 1 06:41:29 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 19f5e466ccfd0a671c6c8cc930b26263412f8e89[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Jun 26 08:29:57 2019 +0300

    Add RGB switch case set for Arduino Dev Board

[33mcommit 56b2cbef053979cdf45df53dae9300c2fd36ac30[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Jun 26 08:29:57 2019 +0300

    Update sketch RGB switch case set

[33mcommit 9f192852fc2ab26073f9c120cf78e221c02b2c9d[m
Merge: 9f843b6 dcceffc
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Jun 26 08:21:38 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 9f843b600931337ed37994622600f4c3486f8a70[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Jun 26 08:04:05 2019 +0300

    Add sketch RGB switch case button set

[33mcommit dcceffcfbebd508d2ae4c2c7b36948f36c2baace[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Jun 26 08:04:05 2019 +0300

    Add sketch RGB button set

[33mcommit 2a7e1c44bb1ce527046464a46849214b87020dfd[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Jun 19 06:24:12 2019 +0300

    Tap files Arduino Dev Boards 2.0

[33mcommit 928b3c9ad630aa5d24ece2a9c583a4d6eea604f6[m
Merge: a4ec41a 120bbe9
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Jun 15 09:55:01 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit a4ec41a6ff29387acef2221b5822fcf1d3a736e2[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Jun 15 09:24:18 2019 +0300

    Update files KiCAD mus555 .pdf

[33mcommit 120bbe94e0c4e13d193b381f7b2b62dace44ebf2[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Jun 15 09:24:18 2019 +0300

    Update files KiCAD mus555 zip

[33mcommit 0619e6bc5d2956b0eecbcfa8c23d92a389f31ea6[m
Merge: 437be64 0e46e40
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Jun 7 20:45:34 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 437be645845314176622da33e882f880b61e7664[m
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Jun 3 15:20:07 2019 +0300

    Update file Arduino infoamtion

[33mcommit 0e46e4089b4c7a8ad82bfcd1bbc4ec455d810bcd[m
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Jun 3 15:20:07 2019 +0300

    Delete file Arduino infoamtion

[33mcommit 94c53c246ade8f7473f91d04753a9c649f81e03f[m
Merge: e2e14af 56b3fc6
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Jun 3 15:18:23 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit e2e14af97d80f2878a1992eaaac44840a06abdef[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue May 28 10:04:31 2019 +0300

    Add and delete Linux information files

[33mcommit 56b3fc6e01d5c9042c1b2a1554df1e6b66dc2f5e[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue May 28 10:04:31 2019 +0300

    Add Linux information files

[33mcommit e9ec87c9da0fa55222fda649582bd207359f0a32[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun May 26 08:39:55 2019 +0300

    Add mus555 files

[33mcommit 16ba38ab5a792800c1d275fc0dc2e275f31bb186[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri May 24 07:48:26 2019 +0300

    Update comments

[33mcommit 47fd06d5cc51665277587545ac11eadd3d563a92[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri May 24 07:48:05 2019 +0300

    Add IR test function

[33mcommit 0f4f411846d8d5f69c328f5b4e498f0750647888[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri May 24 07:47:39 2019 +0300

    Update Arduino information 1 lesson

[33mcommit 7ab6a5039ea39d98684065afbaa1b3cad29c7499[m
Author: Unknown <rise.sp@gmail.com>
Date:   Mon May 20 12:47:49 2019 +0300

    Add KiCAD prj  NPN miltivibration

[33mcommit c0fa1fbc01b246b71deec0d66f831e4c85c37359[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun May 19 06:01:37 2019 +0300

    Add 1 lesson Arduino C++

[33mcommit 1256c5c643e38a490e2d17b5735fdc7e59a40e45[m
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 13:33:03 2019 +0300

    Update TX BT Keyes298 console

[33mcommit 3c177a82f5b22f507e5230272b2936596af4c1cc[m
Merge: c298420 a08a1d0
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 13:31:33 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit c2984205d6a720f3a5084264dce9f4378bdbd8c4[m
Merge: 3764a97 7e0c60d
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 13:17:10 2019 +0300

    Add sketch transmitter for BT Keyes298 console

[33mcommit a08a1d05cf83e38f4b9fc779dc1563785c37ae5f[m
Merge: 3764a97 7e0c60d
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 13:17:10 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 3764a9789cca878d0c2698decfbc85de0f9b6100[m
Merge: aa5071d 65fdd5d
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 12:57:22 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 7e0c60db79b47bb002099e43ee364aaf46518e7c[m
Merge: aa5071d 65fdd5d
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 12:57:22 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit aa5071d2132fb7705c7359467646d8e6eaa16ab9[m
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 08:44:48 2019 +0300

    Update Car BT Transmitter 4 button

[33mcommit 65fdd5d6c001954a5762d1e62fd68d7a2af38ef6[m
Author: Unknown <rise.sp@gmail.com>
Date:   Thu May 16 08:44:48 2019 +0300

    Update Car Labirint  Tim Keyes298

[33mcommit 441120161c839da82df63fc1c5be9b2158caceeb[m
Author: Unknown <rise.sp@gmail.com>
Date:   Mon May 13 23:22:49 2019 +0300

    Add sketch Led_PWM_potentiometer_simply

[33mcommit c2fb351131621da5deabcbb661971538f9433c25[m
Author: Unknown <rise.sp@gmail.com>
Date:   Mon May 13 23:15:06 2019 +0300

    Update sketch Led_PWM_potentiometer

[33mcommit 06045e775a8075dc26ca7fd4e9e91f49073b5b6c[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun May 12 11:37:36 2019 +0300

    Add sketch button_switch_case

[33mcommit d416ef52898e4df7d9f954a442aa496b7fa69880[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun May 12 10:07:17 2019 +0300

    Update sketch a parrot

[33mcommit da04aaebb13a45d0ad7fb3631d81f56ce8d0315a[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun May 12 01:09:19 2019 +0300

    Add c++ info

[33mcommit 70b2b5445a7f38f2148679b1f4b7a4c8f2e85aaa[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Apr 30 11:49:49 2019 +0300

    Add may course

[33mcommit ad7f2a0cc26f778e7c76027393d832434029f082[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Apr 30 10:11:27 2019 +0300

    Update Meccanoid add rgb led Eyes

[33mcommit c508fc2db3a4492516ca8527c7d625fa33337e79[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Apr 26 08:06:07 2019 +0300

    Add led control serial port

[33mcommit 89979770c33ee8d7a566b11931ba7450b347e795[m
Merge: 12ee3a6 4b36486
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Apr 26 07:01:17 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 12ee3a61f3c411845c0594886821d3c0f697e69c[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Apr 26 06:51:55 2019 +0300

    Update Leds switch case

[33mcommit 4b36486436c29d26681ead77085499e30496ab83[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Apr 26 06:51:55 2019 +0300

    Update Leds switch case

[33mcommit aeb33cc57e28bf8a520ba309624a2443bb66fb2f[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Apr 26 06:37:59 2019 +0300

    Rename all files .cpp

[33mcommit 01efdfe5023d33132ad1ac05b9ff1b467dd6aa5b[m
Merge: 8ae9cd9 88f993b
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Apr 25 08:52:14 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 8ae9cd94aa179c2beb07e98792751a04613c6378[m
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Apr 25 08:49:49 2019 +0300

    leds switch ... case

[33mcommit 88f993b9948645ca9fb01d92cdde09c4a7a5395e[m
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Apr 25 08:49:49 2019 +0300

    Rename files cpp

[33mcommit 6b15751eab48fcb87c06211a5692a34dcd44fa7e[m
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Apr 18 11:39:40 2019 +0300

    Add Open Smart Rich UNO-R3

[33mcommit e99bb7a88c31071c3952992edfc9116fe5f48633[m
Merge: cec1669 9200968
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Apr 18 10:00:00 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit cec166952af670b363d94bfd8440b24e96d9fdd1[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Apr 16 08:59:08 2019 +0300

    Rename files

[33mcommit 920096851ec6068ef681a0cd236b4d893768f2b1[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Apr 16 08:59:08 2019 +0300

    Rename files

[33mcommit b059da10f64ea06aeb11e5821375e3603a915ce7[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Apr 16 08:58:09 2019 +0300

    Add sketch NanoMotors from Timothy

[33mcommit 30247f6b77cad516183585740f0384e6d0c5acfc[m
Merge: ca6b19e c7faefc
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Apr 12 12:57:09 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit ca6b19ec9a59e13cdfd857ca9f416831058f63ed[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Apr 12 12:57:07 2019 +0300

    Update CARrobo_Keyes298_biathlon_frim_China2

[33mcommit 164cc9aadefe1c5ddb2e0a8647ef951d589b8d7b[m
Merge: 8eb38e2 c647f0a
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Apr 11 11:25:04 2019 +0300

    Add Car_Chine_298_biathlon'

[33mcommit c7faefca12c70165cda5d146b6b9cf9edf14b2c7[m
Merge: 8eb38e2 c647f0a
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Apr 11 11:25:04 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 8eb38e2f5d4d64a009e593b8b484257959b8f248[m
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Apr 8 12:58:12 2019 +0300

    Add Car_Chine_298_standart Bluetooth

[33mcommit c647f0aa34374c8a4e68fa3f9c1b85e25f7aecb7[m
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Apr 8 12:58:12 2019 +0300

    Update Nano_motors_Black_line add function & code optimization

[33mcommit 9b18a1a418b824f3b939d0f9385443284f376e7d[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Apr 7 11:08:13 2019 +0300

    Update ThreeIRObstacleDetection add func

[33mcommit 517f7656dddbb26c45f167c133e4f41deac0b06e[m
Merge: 13c5ace 5ad27d6
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Apr 7 11:00:49 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 13c5ace812e82aff7367e64121d42135a524b489[m
Merge: f2ebccc 1a6ce79
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Apr 7 10:57:38 2019 +0300

    Update ThreeIRObstacleDetectors.cpp

[33mcommit 5ad27d68890b212b8291035c0137aa8ed2b32539[m
Merge: f2ebccc 1a6ce79
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Apr 7 10:57:38 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit f2ebccc550f86c10d583a16c3673ce7075609786[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Apr 3 11:09:18 2019 +0300

    Moving file

[33mcommit 1a6ce79baa93f3ac6be86f4dea87aa948ae4e209[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Apr 3 11:09:18 2019 +0300

    Moving file

[33mcommit 1e3a38d7c7da0bbcc49efd9696fc3ba66ba27cb1[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Apr 3 11:07:42 2019 +0300

    Add TreeIRObstacleDetectors

[33mcommit 1e57521547368ca15bcb47bfb870aca59f503a0f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Apr 3 07:56:35 2019 +0300

    Rename Avoid_Obstacles_L298P_Shield.ino to Avoid_Obstacles_L298P_Shield.cpp

[33mcommit 87a9b28dee92641cc2c77e76f534d6e58e2a1b84[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Apr 3 07:25:02 2019 +0300

    Update Sonar track Avoid_Obstacles

[33mcommit 40f38c4385efa5ea1d09252a4ae27ff84f585457[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Apr 2 09:27:06 2019 +0300

    Add Meccanoid  smart servo

[33mcommit a43e4364a05f78b17c262a1873e8eba7f838d078[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Mar 31 10:38:37 2019 +0300

    Rename Random_blink_1 & blink_2 files

[33mcommit 611b9eeb7f2e05b1ff7ca26daf1eaf09e417bfcf[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 15:30:49 2019 +0300

    Add comments CARrobo biathlon China Keyes 298

[33mcommit 762974fea2ecda7be85e2c50fca5a9317a6c3b61[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 15:29:17 2019 +0300

    Update CARrobo biathlon China 2.0

[33mcommit a929ce8aa2dd3304caaf9e77d7120a3eb695e5fc[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 15:27:15 2019 +0300

    CARrobo biatlon Сhina 2.0

[33mcommit 4f7a3baee19d992bf031da7c79c8d6c14a3cfec0[m
Merge: 1ad6c99 47fe16d
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 15:20:38 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 1ad6c99b61d3639f0bfe9d1d4d980ca028818f4a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Mar 26 13:55:02 2019 +0300

    Rename Nano_motors_Black_line.ino to Nano_motors_Black_line.cpp

[33mcommit 47fe16de58ba7aeb4733f5aa88671a90db089c99[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Mar 26 13:55:02 2019 +0300

    Rename Nano_motors_Black_line.ino to Nano_motors_Black_line.cpp

[33mcommit 6030d79d20a525f9eea47f9c6a58e1cb2c0980f0[m
Merge: 61a343b c826d1b
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 13:52:56 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 61a343b534ba3d5400837a4492047d81fc868b22[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 10:27:48 2019 +0300

    Update Nano motors. Fix comments

[33mcommit c826d1bd5a3de194cfdb01e616c0fb27e6048d1f[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 10:27:48 2019 +0300

    Update Nano motors. Fix space

[33mcommit 348a298a20330bfbb5ec8741f8811760c5655bfa[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 10:26:21 2019 +0300

    Update Nano motors. Add spase

[33mcommit dfaa95dfbc1e29b3527881853c26954d5273ff96[m
Merge: a45b9d1 a45b9d1
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 10:19:23 2019 +0300

    Update. Nano motors black line. Delete space

[33mcommit a45b9d1edc8f87cfdc22e7b06a6db3a7ca6b82e5[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 26 08:36:45 2019 +0300

    Update Nano_motors black line

[33mcommit c4d012be80ea399fc95e30bc1ef9bca5b32163d2[m
Merge: b1eabe6 6ae9de8
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:32:14 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit b1eabe6e58ab0ccf0aa3bd0dee850564a6ee0bc6[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:30:33 2019 +0300

    Update meccanoid

[33mcommit 6ae9de812b4414af530204cb6ac5d9670908bc27[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:30:33 2019 +0300

    Update meccanoid

[33mcommit 1ff6f1e178b6f43e6c85f64f0b919fd6f38c0719[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:26:44 2019 +0300

    Update meccanoid add play voise & rename

[33mcommit db40a50271fc80b340c77ddb893dd9e2be3dc066[m
Merge: bbe7c09 59da5a0
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:24:15 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit bbe7c09865c3bc1e51f6d5557ec56fcd6030301f[m
Merge: 6ee6412 48d7510
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:22:58 2019 +0300

    Update Car_Two_Motor_Buttons_TX comments

[33mcommit 59da5a07ac17e0591841957b40a4478efd1a4d16[m
Merge: 6ee6412 48d7510
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:22:58 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 6ee6412a0bfa07494157d002589cfa104e4904ed[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:21:47 2019 +0300

    Rename Car_Two_Motor_Buttons_RX comments

[33mcommit 48d751000b9a7be2e983d950848c7b90a3b14978[m
Author: Unknown <rise.sp@gmail.com>
Date:   Fri Mar 22 12:21:47 2019 +0300

    Update Car_Two_Motor_Buttons_RX comments

[33mcommit ef0cc9cdefcafe1ae69352700da30e0b87336ee9[m
Merge: 4968a57 2323370
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Mar 20 08:39:16 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 4968a57d307b569bde9b36f487fe1ce67731a4d4[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Mar 20 08:31:40 2019 +0300

    Rename & add comments to file Fade_millis_alternate_1

[33mcommit 2323370dcee75e34e4ec231e431efd5ecff28bad[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Mar 20 08:31:40 2019 +0300

    Rename file Fade_millis_alternate_1

[33mcommit e3111eec0f171c4544b1d26aa59017d7fffeec53[m
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Mar 20 08:29:57 2019 +0300

    Add comments to file Fade_millis_alternate_1

[33mcommit cc47d797fd0d0aea59da6fdbd58fc9b136a3660a[m
Merge: 271fcbd 01ce96a
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Mar 20 08:26:58 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 271fcbd79ee70016a4583d95377583345a0ee29f[m
Merge: f22744a 7bb1049
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Mar 20 08:22:53 2019 +0300

    Rebame  file Pot_do_while

[33mcommit 01ce96ad35a69b53d861aecaf9aed834ed73f7ff[m
Merge: f22744a 7bb1049
Author: Unknown <rise.sp@gmail.com>
Date:   Wed Mar 20 08:22:53 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit f22744afd4d1e226075dcde2e0e8b11d1a2a11d4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Mar 19 22:39:17 2019 +0300

    Create Led_button_do_while

[33mcommit 7bb10495856e56e5de05428f5539071bbb574c51[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Mar 19 22:39:17 2019 +0300

    Create Pot_do_while_1.ino

[33mcommit ea53e4f1a9ecafe4d7fff7f53c5197c18865bb33[m
Merge: 04ee1cd ea606a4
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 19 12:20:27 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 04ee1cd66f3f37f5823fccdd44eb03171c136646[m
Merge: e110a19 b5e9727
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Mar 18 11:46:52 2019 +0300

    Meccanoid update servo & eyes

[33mcommit ea606a4d49b49323ba15ca534c8066295793d1f8[m
Merge: e110a19 b5e9727
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Mar 18 11:46:52 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit e110a19adadde333173e386b1bbb17469f3ae426[m
Merge: 8037f6b 95d9566
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Mar 18 11:44:16 2019 +0300

    Fade_alternate update comments

[33mcommit b5e97273a1a933b354ba363455651b9e92ba5235[m
Merge: 8037f6b 95d9566
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Mar 18 11:44:16 2019 +0300

    Update comments

[33mcommit 8037f6b1d335cfb0dee445bf650a547a7de10933[m
Merge: 9743498 a237806
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Mar 17 20:53:03 2019 +0300

    Update Millis_class_1

[33mcommit 95d956628b555819c4e3337526b1f07d75486565[m
Merge: 9743498 a237806
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Mar 17 20:53:03 2019 +0300

    Update Millis_class_1

[33mcommit 974349879cd0b4a6fdec096d59b85797f46e5268[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Mar 17 08:43:46 2019 +0300

    Millis_class_1

[33mcommit a237806544e1c5249d65df66cec368b418684e8f[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sun Mar 17 08:43:46 2019 +0300

    Millis_class_1

[33mcommit 1b4c85b7bd82e0105ddd350d9f20f493cc2d4885[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Mar 14 12:16:29 2019 +0300

    Update meccanoid.ino

[33mcommit ca83f1f20df3062e1390432c61599649c86ee7a1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Mar 14 12:15:51 2019 +0300

    Update meccanoid.ino

[33mcommit 34454759035204f14137eab98720d467215e3aa9[m
Merge: ffcda26 9f3a595
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Mar 14 12:13:51 2019 +0300

    Meccanoid 1.0

[33mcommit ffcda26dc3ca22ebdb5a64b402f1d70ca058eb76[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 12 07:44:42 2019 +0300

    Meccanoid 1.0

[33mcommit 9f3a595fda6c3a3089360fd16b728855b4cf6142[m
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Mar 12 07:44:42 2019 +0300

    Meccanoid 1.0

[33mcommit d9499aef562106c8e6f1e906e1fb5357161dce37[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Feb 23 07:02:35 2019 +0300

    Fade an LED v 1.0

[33mcommit 03e394494de5aec5d86fd8c7912055b9dd84e91f[m
Merge: d490a5c aa27265
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Feb 23 06:56:26 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit d490a5cef92e43bddfd162e317f42dcc3dd48150[m
Merge: 6b72c0b ce041f7
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Feb 19 07:11:07 2019 +0300

    Random_Blink_2.ino

[33mcommit aa272653736a9794402eac6435e1bfe180f55f6a[m
Merge: 6b72c0b ce041f7
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Feb 19 07:11:07 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 6b72c0bc07b67d387226b1d7931ad385805894a5[m
Merge: 5e91604 7bbe3cb
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Feb 19 06:57:13 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit ce041f7dbf240c2445f1f60fa55fa55a287df109[m
Merge: 5e91604 7bbe3cb
Author: Unknown <rise.sp@gmail.com>
Date:   Tue Feb 19 06:57:13 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 5e916049bbd7c154d0b0ef73a6e6cf707436e644[m
Merge: a8d2220 de241c2
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Feb 18 06:48:09 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 7bbe3cb732eb166440157f62617206e6fcf83025[m
Merge: a8d2220 de241c2
Author: Unknown <rise.sp@gmail.com>
Date:   Mon Feb 18 06:48:09 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit a8d2220a615a276994e89fdd100b22353a6a784f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Feb 16 07:10:51 2019 +0300

    Create Fade_alternate_1.ino

[33mcommit de241c234464f25a5018e3323ba1982e3c73acd6[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Feb 16 07:10:51 2019 +0300

    Create Fade_alternate_1.ino

[33mcommit 5d7ad9edc19d075e8140949f2f0dfdc7a819cfaf[m
Merge: 5e232ae 08c8219
Author: Unknown <rise.sp@gmail.com>
Date:   Thu Feb 14 08:25:29 2019 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit 5e232ae0cc5470f2e13561f494eddb880651ebda[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Jan 5 17:02:05 2019 +0300

    Create LED_and_PWM_3.ino

[33mcommit 08c8219fc676a0d0705dfb4a9b990fe7a0ee1ab8[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Jan 5 17:02:05 2019 +0300

    Create LED_and_PWM_3.ino

[33mcommit 6a2803c6c06f84e2b2118c14604d45b2327d3415[m
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Jan 5 10:08:05 2019 +0300

    Add function

[33mcommit 573782bed2f01c050bfeee5702b4a50640484479[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Dec 13 06:20:03 2018 +0300

    Update Led_matrix_MAX7219_test_1.ino

[33mcommit abc425e1be04be07a0cbb6fbfb777b9cdc2d1c09[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Dec 13 06:19:48 2018 +0300

    Update Led_matrix_MAX7219_test_2.ino

[33mcommit edd1398fde547dbe4b143f82dbbe4ff145b5a669[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Dec 13 06:17:54 2018 +0300

    Update Led_matrix_MAX7219_test_1.ino

[33mcommit b8a5ed2799f723f8f4c3410d4df3e7e9a7f42359[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Dec 13 06:13:17 2018 +0300

    Create Led_matrix_MAX7219_test_0.ino

[33mcommit f9afe789bf56b0b1946bbe25d751e90f59420eab[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Dec 2 08:46:46 2018 +0300

    Update Sonar_leds.ino

[33mcommit 639206ba399eb1dbc9b59387ce0a95653bd9961c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Dec 2 08:43:42 2018 +0300

    Update Sonar_leds.ino

[33mcommit 96158f3bfc4986454268b4a51305c1b4a9e9efb4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Dec 2 08:42:11 2018 +0300

    Create Sonar_leds.ino

[33mcommit 4dc9fe1650a216279514402ea24c1b42bb02d13b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Dec 2 07:23:55 2018 +0300

    Update Test buttons_&_leds.ino

[33mcommit 09fb687c7ad6df6cc4facfbc3019e0f42c51d840[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Dec 2 07:23:14 2018 +0300

    Update Test buttons_&_leds.ino

[33mcommit 9025c891f34b729ca976e07125b62d22d2bde63a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Nov 24 06:35:32 2018 +0300

    Rename Car_Two_Motor_4Buttons_TTX.ino to Car_Two_Motor_4Buttons_TX.ino

[33mcommit da584bcb26d7bb37ed00d74b3304b9269a52dd7a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Nov 24 06:32:36 2018 +0300

    Rename Car_Two_Motor_4Buttons_TX.ino to Car_Two_Motor_4Buttons_RX.ino

[33mcommit d17698413220fba27ad904c86ea96f7e59e6635e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Nov 24 06:32:14 2018 +0300

    Rename Car_Two_Motor_4Buttons_RX.ino to Car_Two_Motor_4Buttons_TTX.ino

[33mcommit 3eed9a0e83805c49b9f6cd55ccc671a3aab129ec[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Nov 22 07:20:51 2018 +0300

    Create Car_Two_Motor_4Buttons_RX.ino

[33mcommit 6a20544385b2aab24bf6b88af69db97508204b32[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Nov 22 07:19:11 2018 +0300

    Create Car_Two_Motor_4Buttons_TX.ino

[33mcommit 9a74e448cd3ef9e9a446dfd7e02cf71573b3a0e2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Nov 22 07:16:33 2018 +0300

    Create Readme.txt

[33mcommit 9c2a59eaa1c3a1c41170cb14161a8d546fd340b0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 21:42:29 2018 +0300

    Create Car_Two_Motor_Buttons_RX.ino

[33mcommit 91c1602cc1649306dfcbf73f2903f6ac979e9ce4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 21:41:23 2018 +0300

    Create Car_Two_Motor_Buttons_TX.ino

[33mcommit 10d3204dbb91b0301462b1ec6a6fa77da1643fb0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 21:40:17 2018 +0300

    Create Readme.txt

[33mcommit e8b688eaa2bc6026fb02b747f2bc9f820a81bb18[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 09:44:22 2018 +0300

    Update Car_Two_Motor_Joystic_RX.ino

[33mcommit 720a8482694e86e3518664e011083bd57416d91a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 09:34:25 2018 +0300

    Update Car_Two_Motor_Joystic_TX.ino

[33mcommit efeb54c0e65b562ed250e68fe230f09cb87c6685[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 09:34:09 2018 +0300

    Create Car_Two_Motor_Joystic_RX.ino

[33mcommit eda5c62f30f0920e9a771c717a2e420658d192d0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 09:32:33 2018 +0300

    Create Car_Two_Motor_Joystic_TX.ino

[33mcommit 1dedf52804708ef45840adeec70de523e5403394[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Nov 21 09:29:46 2018 +0300

    Create Readme.txt

[33mcommit 9504a9e3ba1e6e892e3b72d65a3d4945048591b4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Nov 19 04:51:25 2018 +0300

    Rename Avoid_Obstacles_L298P Shield.ino to Avoid_Obstacles_L298P_Shield.ino

[33mcommit 0a9e451fb11a114e18528ae78e4ff8d60e5cfc26[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Nov 19 04:50:46 2018 +0300

    Create Avoid_Obstacles_L298P Shield.ino

[33mcommit cd7bfc67d7efb503eb74b12e9ec1f38a8e8ddaf3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Nov 2 08:08:43 2018 +0300

    Create logical.ino

[33mcommit 6dbb901d2ba95b0f5e9bcf4963fff38101925d46[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Oct 25 06:31:03 2018 +0300

    Update Excavo_whith_lcd_2.ino

[33mcommit 2ea3ab640252fcbc58400a0ad52a5ae9024fdbba[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Oct 25 06:30:12 2018 +0300

    Create Excavo_whith_lcd_2.ino

[33mcommit 989a17715bbfce18ebc83e073a3155c93dbffa08[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 24 07:47:56 2018 +0300

    Update Excavo_whith_lcd.ino

[33mcommit 176b9fc7e32326a5126e89cf0943790ed6349e84[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 24 07:46:29 2018 +0300

    Update Excavo_whith_lcd.ino

[33mcommit 4118640d93b9dae1b3be1633e32280b5a16c80f7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 24 07:45:42 2018 +0300

    Create Excavo_whith_lcd.ino

[33mcommit d42bb384476cea77870a5f9f838ca8981cafa927[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 24 06:22:19 2018 +0300

    Update Excavo2.ino

[33mcommit ae8533322c2acd2142987663e8ce2743022a42b0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 24 05:58:30 2018 +0300

    Update Excavo2.ino

[33mcommit d92e0b0230f8d598b923f7c45c38e061bfa077a3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 24 05:58:14 2018 +0300

    Update Excavo2.ino

[33mcommit 93ee25f1a8c89af7d86291af0c9ecde46fe5f7ad[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Oct 21 08:56:52 2018 +0300

    Update Excavo2.ino

[33mcommit 628b2bdb1d361d34a22a5d0b530e8125f623a200[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Oct 21 08:56:16 2018 +0300

    Update Excavo2.ino

[33mcommit 82c40c3c1c8e9612f841f79f64357656fb15eb27[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Oct 20 17:56:50 2018 +0300

    Update Excavo2.ino

[33mcommit 85446a3dbc378c3e7fb944563a54dba8970cb023[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Oct 20 12:16:07 2018 +0300

    Update Excavo2.ino

[33mcommit 6050db4cad166cc6c1ac9dabfd1b12ee7173f951[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Oct 20 12:14:47 2018 +0300

    Create Excavo2.ino

[33mcommit e7e5229a87435e3944a5d42b4ff18910f7e0cca9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Oct 20 12:07:07 2018 +0300

    Update Excavo1.ino

[33mcommit 0ab3c7eff6b3e80743ad309d3951d58ed5b32052[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Oct 20 12:05:57 2018 +0300

    Create Excavo1.ino

[33mcommit 94aa4019e2c24c8bed339695dc8d26810b137e25[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Oct 20 07:34:59 2018 +0300

    Update Leds_wave_2.ino

[33mcommit 2b64e286cd2cfbf3a524e0add6c0e1adfdce9115[m
Merge: b835adf f1b6fba
Author: Unknown <rise.sp@gmail.com>
Date:   Sat Oct 20 07:32:10 2018 +0300

    Merge remote-tracking branch 'origin/master'

[33mcommit b835adf8e6e2081e54de349be8f223e2af3e26d1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 3 11:53:40 2018 +0300

    Update Leds_Switch_Case.ino

[33mcommit f1b6fba43b33fc85847664b61cc5dd5d9acf610a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Oct 3 11:53:40 2018 +0300

    Update Leds_Switch_Case.ino

[33mcommit ade12168cfe4c5960f3bf415152479476081b183[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Sep 29 04:50:02 2018 +0300

    Update Leds_Switch_Case.ino

[33mcommit e204c53b63721e4cbb5dca0f4cab1fdeb21fac6b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Sep 28 04:30:43 2018 +0300

    Update Leds_Switch_Case.ino

[33mcommit 3f9d65eddade86221a4ee1e74dc30e0620a5dbbf[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Sep 28 04:01:54 2018 +0300

    Create Leds_Switch_Case.ino

[33mcommit e2ff3974301f8bd60c3cbae28da6f8d271331a90[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Sep 19 05:01:26 2018 +0300

    Update While_increment_1.ino

[33mcommit de03d7a978083d50310c9aee2aa792476aece06c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Sep 14 05:11:17 2018 +0300

    Update While_increment_1.ino

[33mcommit 9996a5ca59ced65eae3bd39b5b6f6379ac08eddd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Sep 11 08:06:13 2018 +0300

    Create While_increment_1.ino

[33mcommit 626a9e4cbbf411c6635f8aec4222645d2ff92579[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Aug 13 08:35:24 2018 +0300

    Update Set_RGB_led_one_button.ino

[33mcommit 9b76ae8cc12e544da00e5e496a2c5ba24ff383a7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 9 02:19:13 2018 +0300

    Update Led_matrix_MAX7219_test_2.ino

[33mcommit 5c889814fc150553cc951390ff32a3841db0c2dd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 09:02:32 2018 +0300

    Create ControlLeds.h

[33mcommit 47d10b9b38ad820d994fa2c13323a7be65badd45[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 09:02:08 2018 +0300

    Create ControlLeds.cpp

[33mcommit 6f0b3685ec2b3485842b93846debe7f3779a0c54[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 09:00:29 2018 +0300

    Create mail.cpp

[33mcommit 963345667d51acb0b8e3039523dcdd97b46fa0a3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 08:59:22 2018 +0300

    Create Readme.txt

[33mcommit 4f0acbb39b06c5ebcaba6d4676bf66d0aa7bc124[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 08:58:56 2018 +0300

    Create Readme.txt

[33mcommit bd9c340d47009d840ff98253bd3986652e67facb[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 08:30:43 2018 +0300

    Delete Test_led_matrix_7219.ino

[33mcommit 572d5188f6b238208b2c81f3b5ef2ddae8d5b7a1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 08:26:42 2018 +0300

    Update Led_matrix_MAX7219_test_1.ino

[33mcommit ba29e9e8c3c5dc9a4724f0cb80488e06c4e88e04[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Aug 5 08:25:59 2018 +0300

    Update Led_matrix_MAX7219_test_1.ino

[33mcommit 4d1ea819031ae3796ee355534d2d76b14efa1640[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 12:24:05 2018 +0300

    Create Led_matrix_MAX7219_test_2.ino

[33mcommit 107d5f09ba6bdb85ec7a905619ace83652cec370[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 10:31:01 2018 +0300

    Create DebButton.h

[33mcommit afd5259fe4e1b4f338e08017acc09375d2de98ed[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 10:29:59 2018 +0300

    Create Debbutton.cpp

[33mcommit af02413a3b73174b07b9612201e5f95e344f6cd3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 10:28:44 2018 +0300

    Create main.cpp

[33mcommit 591c31d16cbccf746227f437d6e9e095a7271e57[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 10:26:57 2018 +0300

    Create Readme.txt

[33mcommit b6b251bcad3cdd5371267bb8b4066d88d3d97172[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 10:26:35 2018 +0300

    Create Readme.txt

[33mcommit 24c52bd446691549dfad2ddf4930f5e1c40bb15a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 10:26:01 2018 +0300

    Create Readme.txt

[33mcommit ea78c3f45113b1b85d0d380d13d2cabe1737e206[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:12:34 2018 +0300

    Update main.cpp

[33mcommit debd5cd245de9b10f120953cf2504715eb37d475[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:11:58 2018 +0300

    Create Button.h

[33mcommit 42f3839b5b7e9730098d74cd3e14e591ed1bdc09[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:11:36 2018 +0300

    Create Button.cpp

[33mcommit 7d4f5fb4868dbad607d49ba1ec07d26c9d774627[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:10:54 2018 +0300

    Create Readme.txt

[33mcommit 8a592daa77e9f0f50a9fd53a856acb58d1d7a933[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:10:37 2018 +0300

    Create Readme.txt

[33mcommit 0a3c48d5dd741f5b28ee1c906902672229786203[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:10:08 2018 +0300

    Create Readme.txt

[33mcommit c4a1e24bf91c684c1034600baf7e5754ff6e5d59[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:09:43 2018 +0300

    Rename Button/Debounce_button_2/main.cpp to Button/Debounce_button_2/src/main.cpp

[33mcommit 084f30551c11320246ad637ac35a200d8b084525[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:09:24 2018 +0300

    Create Readme.txt

[33mcommit 60892ab8a12f4111dca1f7a7606d2cd6aa7b1568[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:08:23 2018 +0300

    Create main.cpp

[33mcommit e24a8258b3439bbefb6a2097427d55310993fc26[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:07:48 2018 +0300

    Create Readme.txt

[33mcommit ff87180617a48642ab9fe34c2cecc684d0f73d15[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Aug 4 09:07:05 2018 +0300

    Update and rename Buttons_leds_rattling_sticking_1.ino to Debounse button_1.ino

[33mcommit 776401ca828b678e6830d7ec945476e0b366e78b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 10:38:48 2018 +0300

    Update Debounсe_button_3.ino

[33mcommit ce011008532decd21420f9f7415d3c11656cdbd1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 09:17:17 2018 +0300

    Update Debounсe_button_3.ino

[33mcommit f8e3905f9927dd556b0993c459f48e8eadf8e2eb[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 09:10:20 2018 +0300

    Update Debounсe_button_3.ino

[33mcommit 3472fb77414b40bd5725a9ef9bd4eeb5a4e35cb4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 09:06:49 2018 +0300

    Update Debounсe_button_3.ino

[33mcommit 6bb2a9f12f1ac60a31788aa1ed4a2176c8707a0b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 09:01:04 2018 +0300

    Update Debounсe_two_button_using_Class_ Metod.ino

[33mcommit 78d1ca3d72b3505d92e3cdc389657a15e26199d2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 08:58:56 2018 +0300

    Create Debounсe_two_button_using_Class_ Metod.ino

[33mcommit da07c0a199d63a017a465731ebc6a8cc3afa01d7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 08:47:37 2018 +0300

    Update Debounсe_button_3.ino

[33mcommit 6ce4e4633fae0482d6f53ef8ec9f05447879953f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 08:43:22 2018 +0300

    Update Debounсe_button_3.ino

[33mcommit c8e3fc262c803c291bd852090bff62413254d756[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 08:00:43 2018 +0300

    Update Debounсe_button_3.ino

[33mcommit 42ea1b34f2450c9f5f4b1ba474c1d6d1f77c6afc[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 07:57:15 2018 +0300

    Create Debounсe_button_3.ino

[33mcommit dec790337901e7bbfe3914821b5ef23036f33f93[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Aug 3 06:45:32 2018 +0300

    Create Debounсe_button_2.ino

[33mcommit 03d042463bbcb0f0c89744d4eb69e1702ef2be77[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 16:54:21 2018 +0300

    Update Readme.txt

[33mcommit 2816a5eb460286194eb03b358b85b6a22bc19272[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 09:50:42 2018 +0300

    Update Debounсe_button_1.ino

[33mcommit 95181a57a13418c1626f59c9975b8340e62ed15a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 09:50:28 2018 +0300

    Update Debounсe_button_1.ino

[33mcommit d716f9059e98e18b9174079bc2a0f25859f6e061[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 09:50:14 2018 +0300

    Rename Arduino_dev_board/Debounсe_button_1.ino to Arduino_dev_board/Version_2/Debounсe_button_1.ino

[33mcommit 0b95a5b59d2eeb3e4c2e39e4e570a042a86e0c10[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 09:49:17 2018 +0300

    Create Debounсe_button_1.ino

[33mcommit cf617c83ac14b043f2515d3e7ed4adaeb4e08b98[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 09:11:27 2018 +0300

    Update Set_RGB_led_one_button.ino

[33mcommit c6bb5232c22df4a76022934f315ea0d43d7c2350[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 09:07:07 2018 +0300

    Update Set_RGB_led_one_button.ino

[33mcommit 5a5d1357d39626fb736149e49628344baa8b3ea4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Aug 2 08:58:56 2018 +0300

    Create Set_RGB_led_one_button.ino

[33mcommit e6e85e06a7040e94b6c03e0f1f2a30618cba6145[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Aug 1 11:15:13 2018 +0300

    Create Leds_Massive_wave_2.ino

[33mcommit d47c21f63c859377604f73f07308c5ca8c253ece[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 31 08:27:05 2018 +0300

    Update RGB_LEDs_using_arrays_1.ino

[33mcommit c3b2c50fd5d444cd2656b0a8efebd568d688af58[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 31 08:26:36 2018 +0300

    Create RGB_LEDs_using_arrays_1.ino

[33mcommit e99b31a01c47dbc5e30b5ae675ad2df508f13fab[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 30 06:41:52 2018 +0300

    Update Led_matrix_MAX7219_test_1.ino

[33mcommit dc33c49057a413f8e3f312aecb74818a520b33a8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 17:05:16 2018 +0300

    Create Test_led_matrix_7219.ino

[33mcommit d5d2caaa8741418abe4199fbc74099fd03af817a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 16:34:56 2018 +0300

    Update Led_matrix_MAX7219_test_1.ino

[33mcommit beb37345f3730a27dabab3a4bc62555dc925fc09[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 16:09:56 2018 +0300

    Create  Led_matrix_MAX7219_test_1.ino

[33mcommit 82d327188b28da0f3b191269e96c5e0df633a8c0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 16:09:26 2018 +0300

    Create Readme.txt

[33mcommit 4ef310aa57567c9ce22d4683d3f248fc917425fa[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 08:20:59 2018 +0300

    Update LCD_Sonar.ino

[33mcommit f0e504bddaffe080e5297f49cd6c68ec4b39c5c7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 08:19:52 2018 +0300

    Create LCD_Sonar.ino

[33mcommit bae6b08d6a8b403f6146cd3e739bbe144ee4560d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 07:32:41 2018 +0300

    Update Motor_PWM_ whith_ potencometer.ino

[33mcommit b7b4efe6159e9b79bc687a5dfba0d8241162c6a3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 29 07:32:12 2018 +0300

    Create Motor_PWM_whith_potencometer.ino

[33mcommit 5552c6815000b0a585f761f996334861cdb70190[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 28 22:19:04 2018 +0300

    Update Buzzer_Piezo Speaker_1.ino

[33mcommit 73b6f303e98bf1dbc8906881bfb5b9ba5c5eccc3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 28 22:17:42 2018 +0300

    Create Buzzer_Piezo Speaker_1.ino

[33mcommit 3c1b52d364b3ad0c6839bb2169430c75b75e3551[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 28 22:16:39 2018 +0300

    Delete Piezo_Speaker_1.ino

[33mcommit 8ceccdcb15d40db759f6c4dc38807ceb6f7f786e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 28 22:16:20 2018 +0300

    Create Piezo_Speaker_1.ino

[33mcommit c1918830a2841682a0a39757da505c167249f41d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 28 22:06:33 2018 +0300

    Create Blinking LED control with potentiometer_1.ino

[33mcommit b1cd79aa1050cac6e2367f4f80908429ad385176[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 28 21:52:44 2018 +0300

    Create Test buttons_&_leds.ino

[33mcommit ce64fe0d50b37a49ef89eeabfd9ab9ec51032e5f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 28 21:08:48 2018 +0300

    Create Readme.txt

[33mcommit 4387d9853bad08a4b3497de87f44ac46281db39a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jul 27 10:55:56 2018 +0300

    Update LCD_Display_with_I2C_witch_POT_end_motor.ino

[33mcommit cc77e8b828b0c2d531de2f7a7f377ee7a312a68c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jul 27 08:24:07 2018 +0300

    Update LCD_Display_with_I2C_witch_POT_end_motor.ino

[33mcommit 7c310f3fbe9e12f9a50aacb62838b0dc497ad105[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jul 27 08:22:53 2018 +0300

    Create LCD_Display_with_I2C_witch_POT_end_motor.ino

[33mcommit 21dd247584c05749c6b3876b4dd625b2d50c23b7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 22 03:26:57 2018 +0300

    Create LCD1602_I2C_Joystick_PWM.ino

[33mcommit 7373478fc0b28683943fdc39f85c2fccbe3b96e0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 22 02:47:36 2018 +0300

    Update Arduino_board.md

[33mcommit 6734c3f996b6ec0e232ee76c99cbe1577441bd8a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 22 02:44:44 2018 +0300

    Update Arduino_board.md

[33mcommit 3cb021531ed32a916656cba649af1a4d19fafd44[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 22 02:43:59 2018 +0300

    Update Arduino_board.md

[33mcommit 49b1828f8a15c4de010e0eaa7caf9ef7fb843bef[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 22 02:43:20 2018 +0300

    Update Arduino_board.md

[33mcommit f37b40fd7de66c41426e7a3e69b97d58b66995a5[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 22 02:42:27 2018 +0300

    Update Arduino_board.md

[33mcommit 96afc724d58e140ef5434a9d668f2db8a1b73a70[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 22 02:41:24 2018 +0300

    Create Arduino_board.md

[33mcommit f77fe99ee3f3c3f0994887d6561f90d6ba9b96a4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jul 19 16:27:54 2018 +0300

    Update Readme.txt

[33mcommit 161fe86e0497f8cc5dd0b0d383361a570ace8082[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 15 12:14:52 2018 +0300

    Create Blink_alternate.ino

[33mcommit bcda24f2781604f786479a18f39f972e5ceddc6e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jul 14 17:19:59 2018 +0300

    Create Relay_button.ino

[33mcommit 3158398e3315367ae3089f47dbbdc42e5f006e08[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jul 12 07:39:23 2018 +0300

    Update Button_Leds_switch_case_1.ino

[33mcommit 0f23a30be4be38b4fde5891dec826775948cd0d9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jul 11 17:27:23 2018 +0300

    Update Button_Leds_switch_case_1.ino

[33mcommit 05eba3b01bad3a879ce439f3d8e74810ffaa8b3b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 22:05:29 2018 +0300

    Update Button_Leds_switch_case_1.ino

[33mcommit b3eba08a5b6c04c46f49d77f0e1b03b7424a162e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 22:04:08 2018 +0300

    Rename Button_Leds_switch_case.ino to Button_Leds_switch_case_1.ino

[33mcommit 90b75fb02bf7fafbb7521b9926c59bb5b1bceb71[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 22:03:39 2018 +0300

    Create Button_Leds_switch_case.ino

[33mcommit 844991f866b39dc8ac64898d24f9416b0236b79c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 11:19:47 2018 +0300

    Update Readme.txt

[33mcommit 17224f7267feaee702e50e8d9de4ec244d5d1d65[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:51:44 2018 +0300

    Update Basics_of_Programming_Arduino_in_C.md

[33mcommit 828f758fdbcdc70743bc3e889843d3cdddfbd1e2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:48:39 2018 +0300

    Update Basics_of_Programming_Arduino_in_C.md

[33mcommit f5f96a11ed44db46c6f3904b3860c6513c067c97[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:48:01 2018 +0300

    Update Basics_of_Programming_Arduino_in_C.md

[33mcommit cb7702d6bbe4e5cfa45890afa0cf75c7c7a73a4f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:43:36 2018 +0300

    Update Basics_of_Programming_Arduino_in_C.md

[33mcommit 645d9092cf6329be2706a5dac6e2705ac3f4fc78[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:40:11 2018 +0300

    Update Basics_of_Programming_Arduino_in_C.md

[33mcommit ae03a92a057cee795312ba7be67b6bc97ae3f0b2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:38:45 2018 +0300

    Update Basics_of_Programming_Arduino_in_C.md

[33mcommit 39b4345b5429cbc6bc916b4f15d0ffbf64aff644[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:37:33 2018 +0300

    Create Basics_of_Programming_Arduino_in_C.md

[33mcommit 2e1548bd9c2e87030cd11f0a104b948608a937ea[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jul 10 07:36:27 2018 +0300

    Create Readme.txt

[33mcommit 968928bdfb13d8963d51c25bc764d924ce68f198[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 2 08:17:30 2018 +0300

    Rename Leds_wave_revers_1.ino to Leds_buttons_wave_revers_1.ino

[33mcommit e47043ffc63b4d3e3406bf7b9eb71935e56d2601[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 2 08:17:19 2018 +0300

    Update Leds_wave_revers_1.ino

[33mcommit a12c57e6bb3c4dfb69803dafbed4a98dcf5c9191[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 2 08:16:10 2018 +0300

    Update Leds_Massive_buttons_revers_wave_1.ino

[33mcommit 12da33dfb42a5de4944263394738c41af2ea3481[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jul 2 08:15:55 2018 +0300

    Rename Leds_Massive_revers_wave_1.ino to Leds_Massive_buttons_revers_wave_1.ino

[33mcommit 513ebbd0f05b6f29f95c279a9fb45928b14fd0cd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:52:32 2018 +0300

    Update README.md

[33mcommit a28bce4880efc1e5046dfc9d6243c3ffd5c25f33[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:52:14 2018 +0300

    Delete main.html

[33mcommit 193c06996a55096ef48e4bdc6b4ec215d51b1eb0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:50:49 2018 +0300

    Delete Readme.txt

[33mcommit fe2f7669dda24d0f83991d719fb40fd8e0461033[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:50:36 2018 +0300

    Rename LCD_1602_I2C/LCD_1602_I2C_test_1.ino to LCD/LCD_1602_I2C/Option_1/LCD_1602_I2C_test_1.ino

[33mcommit 8c14f9f0578e58c071a256cf6ae73c4215470556[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:50:12 2018 +0300

    Rename LCD_1602_I2C/1602_LCD_I2C_sonar_pot_and_servo.ino to LCD/LCD_1602_I2C/Option_1/1602_LCD_I2C_sonar_pot_and_servo.ino

[33mcommit fd5adf3ea2263491c67570cb48cc47583f600dcc[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:49:30 2018 +0300

    Create Readme.md

[33mcommit b25653e39304faf5f54f3f45194016e8ca957144[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:48:41 2018 +0300

    Update Readme.md

[33mcommit 5661946d0fde14d3ee97f8556629c8800071e8f1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:48:16 2018 +0300

    Update Readme.md

[33mcommit 0a3f32489318ffe9c95a98d45190de7ee1faca92[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:48:04 2018 +0300

    Update Readme.md

[33mcommit be9eab9fd4733ab9aa623378b41ad4d88151cf2c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:47:53 2018 +0300

    Update Readme.md

[33mcommit d378e9c6fe01d0d3ea57f11fbe16435fd8484bee[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:47:40 2018 +0300

    Update Readme.md

[33mcommit a6ddd09988e69f739fb0b131093f9cf689993d9b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:47:20 2018 +0300

    Update Readme.md

[33mcommit 25bf3b9d240360c353d5ec54c812cc9890cd72ae[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:47:01 2018 +0300

    Update Readme.md

[33mcommit c89465f52ff6332cfa229faa7a675c9df7f4b0c3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:45:05 2018 +0300

    Create Readme.md

[33mcommit 2b63ef36a09233f52076e55114412fb693f85dc6[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:44:10 2018 +0300

    Update README.md

[33mcommit b9ba542415edc656185bb2a8d93fd04526b85819[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:42:00 2018 +0300

    Create README.md

[33mcommit b498bc94294631d6d570e545db3621f7c08a164e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:41:30 2018 +0300

    Create main.html

[33mcommit a56acdc9ed9b840398de2ac2974bc2065dbf9da0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:38:43 2018 +0300

    Delete Readme.txt

[33mcommit 4dfeb575f8d6a4b759e1e78e7f501789d952c7e2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:38:33 2018 +0300

    Rename CARrobo/Momot/motor.h to CARrobo/CARroboChina/Car_IR/Momot/motor.h

[33mcommit 3a5c4e31876a083296a6cd8ce3a1c7fd43367ca4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:38:15 2018 +0300

    Rename CARrobo/Momot/IR_test.ino to CARrobo/CARroboChina/Car_IR/Momot/IR_test.ino

[33mcommit db3a08fcdf649fb17534a81ad8cb740e00b93aaa[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:37:47 2018 +0300

    Rename CARrobo/Momot/Car_IR_momot.ino to CARrobo/CARroboChina/Car_IR/Momot/Car_IR_momot.ino

[33mcommit 535cdc96f8f12db4bfea41745c3c281e2dfb0bd0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:36:17 2018 +0300

    Rename CARrobo/CARroboChina/Car_IR/IR_test_1.ino to CARrobo/CARroboChina/Car_IR/Noname/IR_test_1.ino

[33mcommit a44e6aa0fc0bdf8da79d8b7a2ee05393f2729f39[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:35:50 2018 +0300

    Rename CARrobo/CARroboChina/Car_IR/CAR_IR_robo.ino to CARrobo/CARroboChina/Car_IR/Noname/CAR_IR_robo.ino

[33mcommit 3d70eb3a5b5656ed26b8726f19fd79ca63ed73bd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:34:34 2018 +0300

    Create Readme.txt

[33mcommit 4b0141f3a3cb8982bc6466ff3cea16077113404a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:34:06 2018 +0300

    Create Readme.txt

[33mcommit e1f4c470709924b14a0cb45d89ccc4453b23bd37[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:33:12 2018 +0300

    Delete Readme.txt

[33mcommit 43df5ac27a46363fa1e0d95fe9d16696c9af51f1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:33:02 2018 +0300

    Rename CARrobo/CAR_IR/IR_test_1.ino to CARrobo/CARroboChina/Car_IR/IR_test_1.ino

[33mcommit 6dcc53356236371c88219ec73e15bb0809eb19a7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:32:40 2018 +0300

    Rename CARrobo/CAR_IR/CAR_IR_robo.ino to CARrobo/CARroboChina/Car_IR/CAR_IR_robo.ino

[33mcommit 6254460fedbef909e291954e5da45182f0f50d1d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:32:05 2018 +0300

    Create Readme.txt

[33mcommit 8379e96a5743383e9b365a14b1f476af345c1a37[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:31:08 2018 +0300

    Rename CARrobo/CARroboChina/CARrobo_kegelRing_from_China_1.ino to CARrobo/CARroboChina/Car_Sport/CARrobo_kegelRing_from_China_1.ino

[33mcommit 99a81d73336b4922f4afbd53b099e1be4dd75722[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:30:51 2018 +0300

    Rename CARrobo/CARroboChina/CARrobo_biatlon_from_China_1.ino to CARrobo/CARroboChina/Car_Sport/CARrobo_biatlon_from_China_1.ino

[33mcommit 86993d1215a94914b4cf8c48b96ff84260ace9fe[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:30:05 2018 +0300

    Create Readme.txt

[33mcommit f17fdaf85fdf54afef978fcf70718f5e65e51e84[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:29:22 2018 +0300

    Rename CARrobo/CAR_IR_Momot/motor.h to CARrobo/Momot/motor.h

[33mcommit f8b37fe5439ead6322031b55e48249c824bad8f4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:29:01 2018 +0300

    Rename CARrobo/CAR_IR_Momot/IR_test.ino to CARrobo/Momot/IR_test.ino

[33mcommit 4395f001bd64bd458fb224ce3c3dbca2b7ce8d25[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:28:40 2018 +0300

    Rename CARrobo/CAR_IR_Momot/Car_IR_momot.ino to CARrobo/Momot/Car_IR_momot.ino

[33mcommit c4287acb6916ec6cc6ee9b5b86f58c8a30b8ff4e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:28:14 2018 +0300

    Create Readme.txt

[33mcommit e2b5d7c04610ecb96f3acd99791cc8f1b5326602[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:27:18 2018 +0300

    Delete Readme.txt

[33mcommit e2fae4e20001306557d5ca6afda4051ac9cbdd07[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:27:07 2018 +0300

    Rename CARrobo/CAR_nano_motors/Nano_motors_test.ino to CARrobo/Car_Sport/Nano_motors/Nano_motors_test.ino

[33mcommit c21919557175d2a4cf3d5169abb905d42de8a78d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:26:41 2018 +0300

    Rename CARrobo/CAR_nano_motors/Nano_motors_reflection_sensor.ino to CARrobo/Car_Sport/Nano_motors/Nano_motors_reflection_sensor.ino

[33mcommit 4899860dcccf15be646d9af9ae4fdf2547fab6af[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:26:16 2018 +0300

    Rename CARrobo/CAR_nano_motors/Nano_motors_Black_line.ino to CARrobo/Car_Sport/Nano_motors/Nano_motors_Black_line.ino

[33mcommit 506e2e3e6937e6c136e912390e067e850f00cd93[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:25:37 2018 +0300

    Rename CARrobo/CAR_nano_motors/CARrobo_kegelRing_for_Nano_motors.ino to CARrobo/Car_Sport/Nano_motors/CARrobo_kegelRing_for_Nano_motors.ino

[33mcommit d329baf4f1e0082e70f8e7a43841ae7db7364e9f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:24:56 2018 +0300

    Create Readme.txt

[33mcommit 94c02c247e336c112b8f14eb45bfe7f12c0c28e9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:23:17 2018 +0300

    Rename CARroboChina/Bluetooth_Robocar_1.ino to CARrobo/RadioControl/Bluetooth/Option_1/Bluetooth_Robocar_1.ino

[33mcommit 65e8b60eaa6c40bb9505de7b5f5de3bf7c9f4a7d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:22:37 2018 +0300

    Create Readme.txt

[33mcommit 1023ae9c1c7319d016ae6cbb4433e962d90bc5e6[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:22:00 2018 +0300

    Create Readme.txt

[33mcommit 97775d4d38e1b4f2ad7dc1d22b3f6377d103e699[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:20:57 2018 +0300

    Delete Readme.txt

[33mcommit 29d51559fa2d73163f058212f98b79418b17a5d5[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:20:49 2018 +0300

    Delete Readme.txt

[33mcommit cf5dbfa59d24f3867c4fce8c19cbba693d61dcfc[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:20:33 2018 +0300

    Create Readme.txt

[33mcommit 19688f6456864f49eb00671afb15eaad94dc927d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:16:47 2018 +0300

    Create Readme.txt

[33mcommit 616ddda5d66ed8373b715fd328847b5596a350df[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:13:48 2018 +0300

    Delete Readme.txt

[33mcommit 475dbb8d5f7deae741fbf38082d339c10f1e5397[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:12:45 2018 +0300

    Rename RadioControl/NRF2401/Car/Motor_two/Transmitter.ino to CARrobo/RadioControl/NRF2401/Motor_two/Option_1/Transmitter.ino

[33mcommit 7283d0810d4843310d227a9273b99e1ce1733310[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:11:41 2018 +0300

    Rename RadioControl/NRF2401/Car/Motor_two/Receiver.ino to CARrobo/RadioControl/NRF2401/Motor_two/Option_1/Receiver.ino

[33mcommit 2f1503a51c4f957d83d7abddd62e43ff037d4f1b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jul 1 08:08:39 2018 +0300

    Delete Readme.txt

[33mcommit 88ac680355a51b9d9d8455716b39340600724447[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 16:42:53 2018 +0300

    Update LCD_1602_I2C_test_1.ino

[33mcommit c596a53b2d0bbc302d8bbf032cb354edd2e30099[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 16:12:00 2018 +0300

    Create Readme.txt

[33mcommit fb081affffbbec121fd1384bacc8ce110bb4024a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 08:35:01 2018 +0300

    Create Receiver.ino

[33mcommit 140eeea92aee8d92ba75717524e5edad6d8e0db8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 08:33:48 2018 +0300

    Create Transmitter.ino

[33mcommit a209455c7e4ce1f34d1ee89678b7fcf1209d308a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 08:33:19 2018 +0300

    Create Readme.txt

[33mcommit 957caab243717e5b5a99e8fe36ccad7147f181b0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 08:32:46 2018 +0300

    Create Readme.txt

[33mcommit 15a84a1c481ef39f367b6dfd75f20740c0b0a28e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 08:32:19 2018 +0300

    Create Readme.txt

[33mcommit f4d074d47e79fed3c0b07087ec6c84346dc87861[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 08:31:59 2018 +0300

    Create Readme.txt

[33mcommit 345c5ff7872be1d62f286da0adef32b5a37be8c2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 05:23:01 2018 +0300

    Update Motor_PWM_ whith_ potencometer.ino

[33mcommit fe6d3c679f20c66920236d17c875e19f240582d5[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 05:22:40 2018 +0300

    Update Motor_PWM.ino

[33mcommit a646b1c408ddee6531a0662bfdfd7910b33a2881[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 05:22:02 2018 +0300

    Create Motor_PWM_ whith_ potencometer.ino

[33mcommit d80509cabd6f3991924b336e769cf387f86726f8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 29 05:17:13 2018 +0300

    Create Motor_PWM.ino

[33mcommit a26c051bb607c57659029d7c08a0d64816fea09e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 15:44:39 2018 +0300

    Update Servo_POT.ino

[33mcommit cb76f12ecce217d0ec32ef46f7ee1c22e828218d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 15:37:54 2018 +0300

    Delete Joystick_Servo_3.ino

[33mcommit 08d67840b2bf9d1f0456e08ac66352b6903fc3b7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 15:37:03 2018 +0300

    Delete Joystick_Servo_2.ino

[33mcommit 5f6378dd2dcf77d8ed0bfa4c4e645fdeea3539f3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 15:36:09 2018 +0300

    Update Joystick_Servo_1.ino

[33mcommit 004234ce220d322f12719d898db4b77bd9897d79[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 15:09:20 2018 +0300

    Update Joystick_One_motor_PWM_1.ino

[33mcommit 6c7bb17df897b668702f021f31a4c054b4ff1901[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 15:09:06 2018 +0300

    Update Joystick_One_motor_no_PWM_1.ino

[33mcommit d9bcbd666db50d25e2029500fdaf7a822209524f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 15:07:13 2018 +0300

    Update Joystick_One_motor_PWM_1.ino

[33mcommit d26dfaa8f4b8cafab7d7cb773d3d4d368206a343[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 09:50:19 2018 +0300

    Create Joystick_test_with_LCD1602_I2C _1.ino

[33mcommit 85fdcecb2d34cc74e1e22a10dae688af79e40992[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 09:18:39 2018 +0300

    Update Joystick_test_1.ino

[33mcommit 366c59e7df50406899b678f0c9608d6226c645e1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 09:14:23 2018 +0300

    Update 1602_LCD_I2C_sonar_pot_and_servo.ino

[33mcommit c7b252af0d09e27daec5c5a25a4bc80aeeda4e19[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 08:54:04 2018 +0300

    Update Joystick_test_1.ino

[33mcommit f1a0c9a904d18610a48f7235121a17479191eb50[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 08:53:47 2018 +0300

    Update Joystick_test_1.ino

[33mcommit b0fbbbad5df94ca2aa175d1e752e476f49781839[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 08:18:19 2018 +0300

    Update Joystick_Two_Apart_motor_no_PWM_1.ino

[33mcommit 89c702ff34f3fff32fc379eb07a45d7a189a1e99[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 08:09:36 2018 +0300

    Update Joystick_Two_Apart_motor_PWM_1.ino

[33mcommit 798827446670868d71c4319876e421ba568d677a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 08:08:49 2018 +0300

    Update Joystick_Two_Apart_motor_PWM_1.ino

[33mcommit ac48d51deb12acec99615a331ac683d1c5e516b0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 08:08:40 2018 +0300

    Update Joystick_Two_Apart_motor_PWM_1.ino

[33mcommit 55f4c09dea6e553f386d31aef0feaee326f50d5c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 26 06:50:16 2018 +0300

    Update TM1638_LED-KEY_test1.ino

[33mcommit 41b0a0e0b3b12707dc56b342606fac0c601a2ea8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 25 17:03:44 2018 +0300

    Rename Serial/Serial_port_control_motor_1.ino to Serial_port/Serial_port_control_motor_1.ino

[33mcommit d080c83dac35e21adefe555e06558c16626d99cc[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 25 17:03:23 2018 +0300

    Rename Serial/Serial_port_contrl_LED_1.ino to Serial_port/Serial_port_contrl_LED_1.ino

[33mcommit 840434c12f303f5394b5c390dc195a62e047337a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 25 17:03:01 2018 +0300

    Rename Serial/Serial_port_Switch_case_1.ino to Serial_port/Serial_port_Switch_case_1.ino

[33mcommit 7d08e49f63130eff9f9b4d7d2fe14c917475f9a8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 25 17:02:39 2018 +0300

    Rename Serial/Serial_port_Dialog_1.ino to Serial_port/Serial_port_Dialog_1.ino

[33mcommit bb9b2586bd0d6348863fd1e257da79d12d7b2065[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 25 17:02:01 2018 +0300

    Rename Serial/Serial_port_Controlling_the_frequency_of_flashing_LED_1.ino to Serial_port/Serial_port_Controlling_the_frequency_of_flashing_LED_1.ino

[33mcommit 26d64fd9450656b25c073dc60e2e9d07775d29e1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 23:31:23 2018 +0300

    Create Leds_massive_wave_witch_potenciometer.ino

[33mcommit 83a2e1c89f6d161b12ef497a427c4068de9562b4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 23:30:42 2018 +0300

    Update Leds_control_witch_potenciometer.ino

[33mcommit 382556a252917f9eaff2407a3e24123bf807bf62[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 23:28:22 2018 +0300

    Create Leds_control_witch_potenciometer.ino

[33mcommit 861b8073b7de4d592bdb0d4fc4104e948432c21f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 17:45:29 2018 +0300

    Rename PWM_potentiometer_led.ino to Led_PWM_potentiometer.ino

[33mcommit 41d6246cfabb1ae5893fb26fb541a72085740414[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 17:45:06 2018 +0300

    Rename PWM_led.ino to Led_PWM.ino

[33mcommit 542efc320820ca2bf63b3ea8e931bc578e08f904[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 17:44:44 2018 +0300

    Rename Massive_whitch_PWM_fade_brightness.ino to Leds_Massive_whitch_PWM_fade_brightness.ino

[33mcommit a38d7c0b35498f661e2d3f088c4dda7b542235e0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 17:44:21 2018 +0300

    Rename Massive_revers_wave_leds_1.ino to Leds_Massive_revers_wave_1.ino

[33mcommit ab856ba216adcd2206f1ccfd838c5eb3c501c873[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 17:43:58 2018 +0300

    Rename Massive_leds_wave_1.ino to Leds_Massive_wave_1.ino

[33mcommit 777a8856c17cc05f193e3575728ed9187edc39bb[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 17:43:23 2018 +0300

    Create Leds_wave_revers_1.ino

[33mcommit 8dabcdf172e54492a8c4b7838f037d89c7d2401d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 17:21:35 2018 +0300

    Create Massive_revers_wave_leds_1.ino

[33mcommit 2aa310b3f8b65cb69b33943dcc216efecd0c38e2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 13:56:44 2018 +0300

    Update Readme.txt

[33mcommit 05594bf99321a0b19efc351bc06ab69c0e27816b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 07:20:42 2018 +0300

    Update Massive_whitch_PWM_fade_brightness.ino

[33mcommit 43bd2cac7d3d86bbe9dea14b3fbc6010a9f167db[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 07:19:19 2018 +0300

    Create Massive_whitch_PWM_fade_brightness.ino

[33mcommit 4ea841e15975a4c636d2332d7f1b54ee099ef2d9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 07:14:28 2018 +0300

    Update Massive_leds_wave_1.ino

[33mcommit 3767343aad00bd55afb5e1476773c23fd5d8864e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 24 06:49:06 2018 +0300

    Create Massive_leds_wave_1.ino

[33mcommit 49e2eaed248f8326596f7ec4297c750f23fb2b05[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 19:48:53 2018 +0300

    Rename Lesson_buttons_Leds_1.ino to Buttons_Leds_1.ino

[33mcommit 3870f04ebbcf2aaa1aeb5a4cc427c9b3415179a2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 19:48:10 2018 +0300

    Update Leds_wave_1.ino

[33mcommit a9cddc722ed0d6bfcf88d88c830027f8b2382a03[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 19:47:56 2018 +0300

    Update Leds_wave_2.ino

[33mcommit 6f23bedcabb57bda6c0634fb2eb31178d6576d5a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 19:47:32 2018 +0300

    Create Leds_wave_2.ino

[33mcommit 91015fea0cbc1b8ec8b7883c130cf185aec9e2fe[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 19:46:29 2018 +0300

    Rename Leds_wave.ino to Leds_wave_1.ino

[33mcommit fca00eecaab54d279b11a36ad2260f204b280c00[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 18:43:01 2018 +0300

    Create Blinking_LED_control_with_potentiometer_for_Arduino_dev_board.ino

[33mcommit 77321cec865ff9262638d41b2ad796832f65f5d7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 18:39:03 2018 +0300

    Create Blinking LED control with potentiometer.ino

[33mcommit 6ce717e5212b9d2753af3000511b54d33361b152[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 18:29:40 2018 +0300

    Create A_parrot.ino

[33mcommit b2ecb76e7631541e82ac2fba04b290cf42c45ca9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 18:24:14 2018 +0300

    Create Readme.txt

[33mcommit f62adaee6c6a6bb5167cf7abc25c7a6d48338b8a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 18:16:03 2018 +0300

    Update Lesson_buttons_Leds_1.ino

[33mcommit 25cb3a2f58d881de3bd6f644c270898e0c9cd3f0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:21:57 2018 +0300

    Update and rename Controlled by LED brightness using PWM LED 1 to LED/Controlled_by_LED_brightness_using_PWM_LED_1.ino

[33mcommit 138585d44b796fa9016bb43f15b1aa6648b1e978[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:19:00 2018 +0300

    Update and rename Change the flashing frequency of the LED with the help of WHILE 1 to LED/Change_the_flashing_frequency_of_the_LED_with_the_help_of_WHILE_1.ino

[33mcommit aeb88e255a544efc6c50320d0535b8a8ba9d7633[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:16:57 2018 +0300

    Delete Change the flashing frequency of the LED 2

[33mcommit d24bd3f98aeaf5bf7f342a0b66405ce61098e0b3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:16:42 2018 +0300

    Delete Change the flashing frequency of the LED 1

[33mcommit 6e6a457e1c10ddf1e2abcfc40c99054e8b9c9823[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:16:20 2018 +0300

    Delete Button_rattling 1

[33mcommit 4dd15299ebfd8bf6359d9d38dc90cc0bda254655[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:14:45 2018 +0300

    Delete Button_Led_no_if 1

[33mcommit 637effc0eaf9070aca9c1f873161e534e778e25c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:14:24 2018 +0300

    Delete Button_Led_if 6

[33mcommit f3286386869d30c9e353be4cba515584183bb34c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:14:00 2018 +0300

    Delete Button_Led_if 5

[33mcommit 14a3ecc68851c5644e03da13448b2f4f5282c09f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:06:32 2018 +0300

    Delete Button_Led_if 4

[33mcommit 154575e2e7f2f682cc5190f652285e90b36b563d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:06:11 2018 +0300

    Delete Button_Led_if 3

[33mcommit 22f43a38f16875c588a753c245a29a7670f47d44[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:05:56 2018 +0300

    Delete Button_Led_if 2

[33mcommit 6d9eaa2f62f98807549ae2ebd9e69529c6d267e7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:05:34 2018 +0300

    Delete Button_Led_if 1

[33mcommit 5480b8ec0e40cacfbd2cfb0fda23ba0982ce50be[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:05:14 2018 +0300

    Update and rename Button_4_motor_2 1 to Button/Button_4_two_motors_1.ino

[33mcommit a7a05d58312ff1fd85e116b1263fe422f7aa2191[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:03:23 2018 +0300

    Update and rename Bluetooth Robocar 1 to CARroboChina/Bluetooth_Robocar_1.ino

[33mcommit d84a2bbccd93f4bf8fec6375d76f40727bf95c21[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:01:30 2018 +0300

    Rename Analog input volt POT 2 to Analog/Analog_input_volt_POT_2.ino

[33mcommit 6e5f068689bbe46c62289fe986cf8a0f40213ece[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:00:33 2018 +0300

    Update Analog_input_POT_1.ino

[33mcommit 437c9917ba75abcf8d126722012dd18834dca1af[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 17:00:00 2018 +0300

    Update and rename Analog input POT 1 to Analog/Analog_input_POT_1.ino

[33mcommit be6c3ee67c7cb825b9cb1a320798beded280217b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 16:59:10 2018 +0300

    Create Readme.txt

[33mcommit f8d31e31797243f279ec5124a3290df89f988140[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 16:56:17 2018 +0300

    Create Buttons_leds_rattling_sticking_1.ino

[33mcommit cbb3213231ed14229b0b4a48d462f7cf0e90e1ec[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 16:08:04 2018 +0300

    Update Buttons_leds_no_rattling_2.ino

[33mcommit 00afb4b4d118765d640b7e1fee0708b465a9d482[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 16:07:50 2018 +0300

    Update Buttons_leds_no_rattling_1.ino

[33mcommit e0715c278c11d690e702e16f0473419d2e7ec619[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 16:04:55 2018 +0300

    Create Buttons_leds_no_rattling_2.ino

[33mcommit eed411d9f87e43095fcc64c8e5c633d396d39d22[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 16:04:21 2018 +0300

    Rename Buttons_leds_no_rattling.ino to Buttons_leds_no_rattling_1.ino

[33mcommit 0c1bedd2f74e307b6a45ad12f7e643f73c20862c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 15:50:05 2018 +0300

    Create Buttons_leds_no_rattling.ino

[33mcommit 3ff986ebf7594c4a09c9d311ae581222919cd100[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 12:47:59 2018 +0300

    Delete Lesson_buttons_Leds_2.ino

[33mcommit a8dc785b457d22a25719e0778b47013e950624db[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 12:42:19 2018 +0300

    Update Lesson_buttons_Leds_1.ino

[33mcommit 34b38cb2b5216b610c79d7ff17c1a2746f8d5480[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 12:30:23 2018 +0300

    Update Lesson_buttons_Leds_2.ino

[33mcommit 2f28ca402c7521b770626f91cc36b32fde4639cf[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 12:29:45 2018 +0300

    Update Lesson_buttons_Leds_2.ino

[33mcommit 9bf7e4ec50ce5ed1edf36cb22494ff5554a92468[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 12:26:21 2018 +0300

    Update Lesson_buttons_Leds_2.ino

[33mcommit f7dd53803161bf1c62ea700e4f015c0deada8341[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 12:07:08 2018 +0300

    Update Lesson_buttons_Leds_2.ino

[33mcommit 5a5e9ca45d1b24126362ebff886db7b4701c994e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 09:32:26 2018 +0300

    Update Leds_wave.ino

[33mcommit 5f3145e0a2a30b50f850eefd1b2aa946cc294f1f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 09:30:47 2018 +0300

    Create PWM_potentiometer_led.ino

[33mcommit cb7e2a9431d281cb12967caaa3b1779167b41978[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 09:25:08 2018 +0300

    Update PWM_led.ino

[33mcommit d660b6cc675df4e6588bc9e125786f2547cf0140[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 09:24:21 2018 +0300

    Create PWM_led.ino

[33mcommit 8f042baa1d0eb099a2847fd850fadaa9cb2c9f95[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 23 07:37:24 2018 +0300

    Update Nano_motors_test.ino

[33mcommit 93dff259691bc071271db3c393663e6c15e35230[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 09:36:07 2018 +0300

    Update and rename Lesson_buttons_Leds.ino to Lesson_buttons_Leds_1.ino

[33mcommit 6582344f81ee4f51366bb46e560890981baa4d33[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 09:35:47 2018 +0300

    Rename Lesson_buttons_Leds_1.ino to Lesson_buttons_Leds_2.ino

[33mcommit f5b92afe9347fb8385edc863043684cd8c41bb5a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 09:35:33 2018 +0300

    Create Lesson_buttons_Leds_1.ino

[33mcommit b059352516befed31b2f2e72484c3125edd6f2e7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 08:41:42 2018 +0300

    Create Potentiometer_test.ino

[33mcommit d5fe1533d7792478d1b8be6cc935ccdd8f06efa0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 06:35:25 2018 +0300

    Delete Buttons_leds.ino

[33mcommit 32e576cac395d215ed333488bee8e07d39e004e3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 06:35:07 2018 +0300

    Rename Lesson_buttons.ino to Lesson_buttons_Leds.ino

[33mcommit 074e548c9a7a97ab084271b5af119dcbafe6f763[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 06:33:54 2018 +0300

    Update Test buttons.ino

[33mcommit f584c3b538dea565f3fc216509063383a3998753[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 06:28:57 2018 +0300

    Create Lesson_buttons.ino

[33mcommit 8aea4b3de721d60683bbd85a0787b1cf05420c01[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 06:09:09 2018 +0300

    Update Test buttons.ino

[33mcommit d21b8f7c09f5b0017be6e7db7a9d3f9fb6ec0846[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 22 05:49:25 2018 +0300

    Create Test buttons.ino

[33mcommit 5b39a8218e40575a51dbc333c5b2b9a7541a319c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 20 17:44:48 2018 +0300

    Update Readme.txt

[33mcommit 09f2892d9fc8059f2dc3541e4002816b17331113[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 18 09:40:03 2018 +0300

    Create Readme.txt

[33mcommit 43d1375747940ef8c8577ba32260db776bcf1647[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 17 21:59:42 2018 +0300

    Create Morse code_3.ino

[33mcommit 733ed104477bf6d8f03862d347db5346c5fb3910[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 17 20:42:11 2018 +0300

    Update Blink LED the specified number of times.ino

[33mcommit 357ade8f837bfe6d9b713e0681937a00ae235f8d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 17 20:36:18 2018 +0300

    Update Blink LED the specified number of times.ino

[33mcommit c9998b578ad9d5946789dc5a6616eccdf545cc82[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 17 20:21:07 2018 +0300

    Update CARrobo_kegelRing_for_Nano_motors.ino

[33mcommit d308d559cc79cbe4f23b5f55dbc1a73db8fa2933[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 17 18:33:03 2018 +0300

    Update Nano_motors_Black_line.ino

[33mcommit e0dcff034c4a390faa7c77b3b2dda5d4e69751ad[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:03:06 2018 +0300

    Rename Joystick_One_motor_PWM 1 to Joystick/Joystick_One_motor_PWM_1.ino

[33mcommit 74365cc93ca2d17d0d139f4e896101fbbe6f0f3f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:02:47 2018 +0300

    Rename Joystick_One_motor_no_PWM 1 to Joystick/Joystick_One_motor_no_PWM_1.ino

[33mcommit 088ebf14b481378705a7c8fdc55922ef4633b392[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:02:21 2018 +0300

    Rename Joystick_Servo 1 to Joystick/Joystick_Servo_1.ino

[33mcommit ab8bb47e988737213e592a2eede57b9a4347e0f5[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:02:00 2018 +0300

    Rename Joystick_Servo 2 to Joystick/Joystick_Servo_2.ino

[33mcommit 9397d95abfe746016274d7ce208322bbfa9bcef4[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:01:39 2018 +0300

    Rename Joystick_Servo 3 to Joystick/Joystick_Servo_3.ino

[33mcommit d633af02c5f3373e7b4eaf5f99f7f08dc2cda260[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:01:11 2018 +0300

    Rename Joystick_Two_Apart_motor_PWM 1 to Joystick/Joystick_Two_Apart_motor_PWM_1.ino

[33mcommit cb91fb72c2ef773c66d365f7b0e7c7a4a5fcf83d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:00:48 2018 +0300

    Rename Joystick_Two_Apart_motor_no_PWM 1 to Joystick/Joystick_Two_Apart_motor_no_PWM_1.ino

[33mcommit 97c402e8de3b8de7c75bae9f0a09d8a5ce866eb7[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 07:00:19 2018 +0300

    Rename Joystick_test 1 to Joystick/Joystick_test_1.ino

[33mcommit b68ef0d86f39a0035913146b5e67bd0b25f834b0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:59:33 2018 +0300

    Rename LED_night light 1 to Button/LED_night light_1.ino

[33mcommit 23b780de38b101daea02c18bc403d622b78b6296[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:57:48 2018 +0300

    Rename Morse code 1 to LED/Morse code_1.ino

[33mcommit 7b110bc47737cfbf57f1b80343fa1e428d8d702a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:57:16 2018 +0300

    Rename Morse code 2 to LED/Morse code_2.ino

[33mcommit 14c6be089a79123cd097399dae36f79bc0bd1b06[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:56:44 2018 +0300

    Rename Serial_port_Dialog 1 to Serial/Serial_port_Dialog_1.ino

[33mcommit 51229b4882cd12336a0a7ebc81f0d6072581700a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:56:21 2018 +0300

    Rename Serial_port_Switch_case 1 to Serial/Serial_port_Switch_case_1.ino

[33mcommit 31d626b30a596c8cccca15aad8b91a9b232995c9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:55:48 2018 +0300

    Rename Serial_port_contrl_LED 1 to Serial/Serial_port_contrl_LED_1.ino

[33mcommit ce7ec06746edb18b8f9e10e588eb485dc74eefa3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:55:19 2018 +0300

    Rename Serial_port_control_motor 1 to Serial/Serial_port_control_motor_1.ino

[33mcommit 3a23e343ce6fb580839cfb2e70bfdea54a6d666a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:54:39 2018 +0300

    Rename Serisl_port_Controlling the frequency of flashing LED 1 to Serial/Serial_port_Controlling_the_frequency_of_flashing_LED_1.ino

[33mcommit 5cf6fe4a4714500fb60d4f8fec32ab0a193750c2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:53:07 2018 +0300

    Rename Servo_test_1.ino to Servo/Servo_test_1.ino

[33mcommit e2006f9c11a89ce387cd954a361ebd1615a44998[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:52:37 2018 +0300

    Rename Servo test 1 to Servo_test_1.ino

[33mcommit 597ccaa3625916da2a467466f5af45671f58b823[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:52:13 2018 +0300

    Rename Servo test 2 to Servo/Servo_test_2.ino

[33mcommit d8a596438cf42af9f94ac26da97f37345daecd8c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:51:36 2018 +0300

    Rename Servo_POT to Servo/Servo_POT.ino

[33mcommit a6b014fd1c545bb783834b5f5f1137e6bd81bfba[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:50:25 2018 +0300

    Rename CARrobo/CARrobo_biatlon_from_China_1.ino to CARrobo/CARroboChina/CARrobo_biatlon_from_China_1.ino

[33mcommit a4a31492c1304681fe74ee9e045663175c302ebd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:49:51 2018 +0300

    Rename CARrobo/CARrobo_kegelRing_from_China_1.ino to CARrobo/CARroboChina/CARrobo_kegelRing_from_China_1.ino

[33mcommit eac3baa912989f898514b472bada236246abb603[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:48:34 2018 +0300

    Create Readme.txt

[33mcommit 95db2f41832c7bffa1216db374c6dabb845f0424[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 15 06:47:43 2018 +0300

    Update CARrobo_kegelRing_from_China_1.ino

[33mcommit 189375ea1384146ab6ac08d96a460ac0df0f6e03[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 13 22:01:08 2018 +0300

    Create IR_test.ino

[33mcommit b0a7fa5906e1b4691527781e47ec1b7d275156cd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 13 22:00:10 2018 +0300

    Create motor.h

[33mcommit 07d8cb94e29e55187c5167f5db92ec271139448c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 13 21:57:48 2018 +0300

    Create Car_IR_momot.ino

[33mcommit ab0b4b586481a608c2e3524663246b7e158811f6[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 19:29:31 2018 +0300

    Update Blink_through_millis_static_while.ino

[33mcommit 68b8b823e11fbc8196524fdb4e4444432d660f29[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 19:29:15 2018 +0300

    Rename Blink_through_millis_While.ino to Blink_through_millis_static_while.ino

[33mcommit fbbe75c511a30d9ef2d9ad50ec92c6fa042684af[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 19:09:52 2018 +0300

    Update Blink_through_millis_static.ino

[33mcommit 860e46209eb880cf6610fdb220629944d3323875[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 19:06:52 2018 +0300

    Rename Blinl_through_millis_While.ino to Blink_through_millis_While.ino

[33mcommit 1cf3b9b6ba7332fb6b7f9742fa3cfd5f522a2f4c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 19:06:35 2018 +0300

    Create Blink_through_millis_static.ino

[33mcommit 549c41655dc8854716b30b0c800ff54c28dbd446[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 08:42:00 2018 +0300

    Update and rename Blinl_through_While.ino to Blinl_through_millis_While.ino

[33mcommit d17c0d0485486b81c12f11770308b6a00c27d23e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 08:39:21 2018 +0300

    Rename Blinl_through_While.ino to LED/Blinl_through_While.ino

[33mcommit 2c1503c1157144c8614c6dbc9b8b1592a1c3acbf[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 08:38:36 2018 +0300

    Create Blinl_through_While.ino

[33mcommit e2e5dc994c2e06b04fb15f0e67f645e30e36ed01[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 03:40:08 2018 +0300

    Create TM1638_LED-KEY_test1.ino

[33mcommit 25266f23ce1448059a4ffc3c34057f279ff92147[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 12 03:39:18 2018 +0300

    Create Readme.txt

[33mcommit 1264be96aa52bfe48fae7eb328584616cbf8d294[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 11 13:45:48 2018 +0300

    Create 1602_LCD_I2C_sonar_pot_and_servo.ino

[33mcommit 83a36c3177b3c697714de19e9203ef24a904ab8e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 9 21:26:23 2018 +0300

    Update CARrobo_kegelRing_for_Nano_motors.ino

[33mcommit 411c5ab2538b15c281b88fb17e08f9964552da0a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 9 19:43:22 2018 +0300

    Create CARrobo_kegelRing_for_Nano_motors.ino

[33mcommit e65c519666a64bc01546c00c522038341274ec43[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sat Jun 9 19:42:13 2018 +0300

    Update Nano_motors_Black_line.ino

[33mcommit 079a8299efe3e86300f883fa356598ae3d37a962[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 12:00:08 2018 +0300

    Rename CARrobo_biatlon_from_China_1 to CARrobo_biatlon_from_China_1.ino

[33mcommit 38a30dedcf31aeaeed64d08dd73b7334150cde81[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 07:27:35 2018 +0300

    Update Nano_motors_test.ino

[33mcommit 308aeae6d69b7d0e333bac2d43f4ca38a044c70b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 07:09:59 2018 +0300

    Create Nano_motors_Black_line.ino

[33mcommit 0ac957048385eb3f676516d47ba07bc2068ba0ae[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 07:06:09 2018 +0300

    Update Nano_motors_reflection_sensor.ino

[33mcommit d928e939ab35f4539dbeb3960c54374551c7567d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 07:05:54 2018 +0300

    Update Nano_motors_test.ino

[33mcommit 917e71b7f348c7a47a18cb3197acde732fc239e9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 07:04:55 2018 +0300

    Create Nano_motors_reflection_sensor.ino

[33mcommit 33463739de9e692395d6be3741a7283bc1b53102[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 07:01:41 2018 +0300

    Create Nano_motors_test.ino

[33mcommit ec57c19895d0cab78717668f745fb5ae69336153[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Thu Jun 7 07:00:41 2018 +0300

    Create Readme.txt

[33mcommit f4630dd03854ca56b75742340375dcf21c5e1617[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 6 18:22:10 2018 +0300

    Create motor.h

[33mcommit 3d42daf71c38245ca18f2d1a570eb267cd5734c0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 6 18:21:09 2018 +0300

    Create CAR_IR_robo.ino

[33mcommit f87a3ec6e3f7442bbc32b4980743f6bb72d27268[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 6 18:20:24 2018 +0300

    Create IR_test_1.ino

[33mcommit 8825031df36971c447a024f21f0e53b9a04b6d27[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Wed Jun 6 18:19:54 2018 +0300

    Create Readme.txt

[33mcommit fc82d199df912cf841f548f68bf5b742d00b0fe1[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:35:50 2018 +0300

    Update LED_night light_1.ino

[33mcommit d8e8a5749bebab0e60bc5715653477f325875b1e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:35:25 2018 +0300

    Create LED_night light_1.ino

[33mcommit 1dd39a06c453f677e3942b2203f018bdf16fbb3d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:34:50 2018 +0300

    Delete LED and PWM 1

[33mcommit 96c29adff8bf4fc57f02dad4e8739ea1134e15dc[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:34:38 2018 +0300

    Update LED_and_PWM_1.ino

[33mcommit a533f36aebfcfe17421856bde1c3b94355b42075[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:34:11 2018 +0300

    Create LED_and_PWM_1.ino

[33mcommit 5abdeeb11aa1799b6e624448ec98d2f4c1f65501[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:25:45 2018 +0300

    Delete Blink LED the specified number of times

[33mcommit a6dfa968800987a1e51812b8d033d80e057f5214[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:25:30 2018 +0300

    Update Blink LED the specified number of times.ino

[33mcommit 4559e5a2a130e3324708b54006cf82f23b315a19[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:24:45 2018 +0300

    Create Blink LED the specified number of times.ino

[33mcommit 41445284422fabf89aeb08e226f999a426fefc61[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:22:16 2018 +0300

    Update LCD_1602_I2C_test_1.ino

[33mcommit 9b0d09a13bb0e99829a6c7271a1e0f95523f557e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:18:00 2018 +0300

    Create LCD_1602_I2C_test_1.ino

[33mcommit b94897b4b767ab4fcfba93a880066026783702f9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 21:17:28 2018 +0300

    Create Readme.txt

[33mcommit a92531421913100f4a705cec03887fb83ec83ab5[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 19:35:03 2018 +0300

    Update CARrobo_kegelRing_from_China_1.ino

[33mcommit 185d5383997f62b8a5d0b7f1abaf7338146e9908[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 19:34:11 2018 +0300

    Update CARrobo_kegelRing_from_China_1.ino

[33mcommit 40162657ac56fc92436d67794e947b6252826d52[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 19:33:27 2018 +0300

    Update CARrobo_kegelRing_from_China_1.ino

[33mcommit a56cd097ff24d09d9c0916fabed0496e9d090eec[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 19:33:01 2018 +0300

    Rename CARrobo_kegelRing_from_China_1 to CARrobo_kegelRing_from_China_1.ino

[33mcommit 1bde62e311b392f5d423abc3725cd2b6d09a2d95[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue Jun 5 19:32:46 2018 +0300

    Update CARrobo_kegelRing_from_China_1

[33mcommit c47eda2e7a3ca9a5b16afe9a8c3e033e8aa69106[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 22:19:10 2018 +0300

    Create Buttons_leds.ino

[33mcommit e0b31b054e2bbdbf5cf9f9760d81616cf1c4e915[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 21:33:46 2018 +0300

    Rename Leds_wave to Leds_wave.ino

[33mcommit 614a889afa7a71a8cf8ac9d21c48c7e565e7494e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 21:33:23 2018 +0300

    Create Leds_wave

[33mcommit 79a3ad2f67687c54a553672425bd592a0525eb7e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 20:38:34 2018 +0300

    Create Readme.txt

[33mcommit 6a3d4fa3bc27f2318f7431a64c5700f1e6df72f8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 08:01:37 2018 +0300

    Update CARrobo_kegelRing_from_China_1

[33mcommit 349bc456134104c67b14ce37676dfa1147c17a9e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 06:38:20 2018 +0300

    Create CARrobo_kegelRing_from_China_1

[33mcommit 5e6aeba8f5f52afcd3fd00bfe4346b3d37ed2e49[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 06:37:28 2018 +0300

    Rename CARrobo_from_China_1 to CARrobo_biatlon_from_China_1

[33mcommit f70b5b8f8e10b10369fd6c960d8ebd46a81c31f2[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon Jun 4 06:36:38 2018 +0300

    Update CARrobo_from_China_1

[33mcommit f194b09152ba34cc0adaa94123eb5ab793e57c84[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 3 06:44:09 2018 +0300

    Create CARrobo_from_China_1

[33mcommit 278b3eac57c30900b57be1fba55fe86864d85a07[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 3 06:43:03 2018 +0300

    Create Readme.txt

[33mcommit 7870daf56c6e568d441cc3814baf2c80c78ef45f[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 3 06:42:30 2018 +0300

    Delete CARrobo

[33mcommit e045602eb79532b225a0673cbb329c39c8b42438[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 3 06:41:22 2018 +0300

    Create CARrobo

[33mcommit 247ec6c0c57b06d5a53a693b1f9c601b4929aba9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 3 06:41:06 2018 +0300

    Delete CARrobo

[33mcommit 8614be1008373e978a7d8763a2862a580fdd1e12[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Sun Jun 3 06:40:45 2018 +0300

    Create CARrobo

[33mcommit d2c945db90682eb7307ffcd0150ef564220660c9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Fri Jun 1 06:06:09 2018 +0300

    Create README.txt

[33mcommit 0af127c95b01ac7efab23d74ec625cfc7f110fbf[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 07:04:10 2018 +0300

    Create Button_4_motor_2 1

[33mcommit 306afd77f62ff1c0a52848fac63b037312e4bf21[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 07:02:45 2018 +0300

    Create Button_rattling 1

[33mcommit 398fcd5a4a5cb6c98eb0f11b54f1056f8b01090a[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 07:01:30 2018 +0300

    Create LED_night light 1

[33mcommit d008d0a5d1e046e4c5f3bd28eae7a5c62e40ce95[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 07:00:26 2018 +0300

    Create Button_Led_if 6

[33mcommit fc2443012b6cd501f259826a1f4c8fedfac1abf5[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:59:56 2018 +0300

    Create Button_Led_if 5

[33mcommit 946240b40002fa3d674641274a22222b330c77a0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:59:29 2018 +0300

    Create Button_Led_if 4

[33mcommit 2664355237e6ab601e766608e7934931ead2510b[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:58:50 2018 +0300

    Create Button_Led_if 3

[33mcommit 4933b5e352f8893bedd15a45c81d0b711bda9f19[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:57:51 2018 +0300

    Create Button_Led_if 2

[33mcommit da27d42d3483d52a9b7b7e767342ba4d287e7ae8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:57:14 2018 +0300

    Create Button_Led_if 1

[33mcommit 2ecfb4893d9a110ac02f3705b80e6aa0826f9f79[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:56:44 2018 +0300

    Create Button_Led_no_if 1

[33mcommit c74f0b747e8f2a32907b03f47eff244bc253ea74[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:43:30 2018 +0300

    Create Serial_port_control_motor 1

[33mcommit 78c2676078eefe0b320f32d8a854632d6f496325[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:42:39 2018 +0300

    Create Serial_port_Switch_case 1

[33mcommit 96ff0157bd136e4f5696295c3c5548ddaea5eefd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:41:05 2018 +0300

    Create Serial_port_Dialog 1

[33mcommit 4b916cb636edfe4496f32b613f55a7e3e3a4673d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:40:08 2018 +0300

    Create Serisl_port_Controlling the frequency of flashing LED 1

[33mcommit d45823d7b824cef6e4c1cfd9239afb0f9342ee84[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:38:41 2018 +0300

    Create Serial_port_contrl_LED 1

[33mcommit 36db634b1d5dc076e9cf4f2b176da382de0bd85d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:35:09 2018 +0300

    Create Joystick_Two_Apart_motor_PWM 1

[33mcommit 284f006f50ed56cccfac1d213b707256f0938691[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:34:09 2018 +0300

    Create Joystick_Two_Apart_motor_no_PWM 1

[33mcommit bde0f46cc3d9a78ae2e374d45f28525d249b1815[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:31:46 2018 +0300

    Create Joystick_One_motor_PWM 1

[33mcommit da46e01faa5ccbec1fe58d8bdbf65b3b59f03b81[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:31:05 2018 +0300

    Create Joystick_One_motor_no_PWM 1

[33mcommit 33f932f75c9bda6c9ce04662258101a318522a42[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:29:51 2018 +0300

    Rename Joystick_Servo 2 to Joystick_Servo 3

[33mcommit 3d595bddb948cdc1fabef7f26b2e02fab020ce87[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:28:37 2018 +0300

    Rename Joystick_test to Joystick_test 1

[33mcommit dd9963b8c053b62220ff079f61caadcef796d2d9[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:28:09 2018 +0300

    Create Joystick_test

[33mcommit 763daefe2ccc4855ef81149fb51217e371ba0ef3[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:27:23 2018 +0300

    Create Joystick_Servo 2

[33mcommit f14368fd96d3b5a8cd713633b7092ecc4bb271b0[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:25:47 2018 +0300

    Create Joystick_Servo 1

[33mcommit bca05c8795e434a46954efa8a8fd00310550e7d8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Tue May 29 06:24:23 2018 +0300

    Create Servo_POT

[33mcommit 474e0fbb92b233a4646d15c1ea9fb09e805cc7f8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:27:34 2018 +0300

    Create Servo test 2

[33mcommit 6aa9d774b3aded5bda25fee676e8bfe179edf1f8[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:27:14 2018 +0300

    Create Servo test 1

[33mcommit 5bb8136c78c8eb1a3c04f05794f40a67f1bb5220[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:26:35 2018 +0300

    Create Bluetooth Robocar 1

[33mcommit 3717e3625469c21a18612e20f450898e9024f236[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:23:31 2018 +0300

    Create Analog input volt POT 2

[33mcommit 776249c3d7cb81d589e601ba7a2bdd68a98f853d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:22:42 2018 +0300

    Create  Analog input POT 1

[33mcommit 1c9c6306948b5db98013653e3c34286ac43df758[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:21:41 2018 +0300

    Create LED and PWM 1

[33mcommit b62c92ef0f4280b5871ee4caa5d54950d3be0b9d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:20:50 2018 +0300

    Create Blink LED the specified number of times

[33mcommit 64caf7f2424b35a93308c3752ed02cfc71768316[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:19:49 2018 +0300

    Create Morse code 2

[33mcommit 974877da694f3d15177fc617925c718ef7a01486[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 10:19:14 2018 +0300

    Create Morse code 1

[33mcommit 11fa674b3df47dc8bab69c06847e973d6f152f0c[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 07:40:56 2018 +0300

    Create Change the flashing frequency of the LED with the help of WHILE 1

[33mcommit db885731dca3a8ee31f2f4f97df6b6ff65f08136[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 07:22:35 2018 +0300

    Create Controlled by LED brightness using PWM LED 1

[33mcommit 1a8bd1ee7d82ff496323e2d19ef87cb217961b50[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 07:21:26 2018 +0300

    Create Change the flashing frequency of the LED 2

[33mcommit df0a3f543777ac140a25bc6f83be1d7eeac6728e[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 07:19:13 2018 +0300

    Delete led_for.gitignore

[33mcommit 58f059f5ac261d4bbcdc65d137d54da4535cb86d[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 28 07:18:50 2018 +0300

    Create Change the flashing frequency of the LED 1

[33mcommit 58dcc11e09ee1a07ddec5056cc6b4bc3682201fd[m
Author: rurewa <34348805+rurewa@users.noreply.github.com>
Date:   Mon May 21 12:56:03 2018 +0300

    No comment
