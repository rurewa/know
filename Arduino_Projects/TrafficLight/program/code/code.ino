// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
//Это СВЕТОФОР
//«ТВП» - табло вызывное пешеходное
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
const int BUTTON = 8;
const int RED = 6;
const int YELLOW = 5;
const int GREEN = 4;
const int BUZZER = 11;

void setup() {
  pinMode(GREEN, OUTPUT);//green
  pinMode(YELLOW, OUTPUT);//yellow
  pinMode(RED, OUTPUT);//red
  pinMode(BUZZER, OUTPUT);
}

void loop() {
  bool buttonState = digitalRead(BUTTON);
  if (buttonState == true) {
    digitalWrite(GREEN, LOW);
    blinks(3, 500);
    digitalWrite(RED, HIGH);
    blinks_1(10, 500);
    blinks_2(7, 250);
   // delay(5000);
    digitalWrite(RED, LOW);
    delay(500);
  }
  else {
    digitalWrite(GREEN, HIGH);
    digitalWrite(BUZZER, HIGH);
  }
}

void blinks(int b, int times) {
  for (int i = 0; i <= b; ++i) {
      digitalWrite(YELLOW, !digitalRead(YELLOW));
      delay(times);
    }
}

void blinks_1(int b, int times) {
   b*=2;
  for (int i = 0; i <= b; ++i) {
      digitalWrite(BUZZER, !digitalRead(BUZZER));
      delay(times);
  }
}
void blinks_2(int b, int times) {
   b*=2;
  for (int i = 0; i <= b; ++i) {
      digitalWrite(BUZZER, !digitalRead(BUZZER));
      delay(times);
  }
}


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
