#include <NewPing.h>
// подключение сонаров к пинам ардуино
const int TRIG1 = 2;
const int ECHO1 = 3;
const int TRIG3 = 23;
const int ECHO3 = 24;
const int TRIG4 = 22;
const int ECHO4 = 21;
const int TRIG2 = 4;
const int ECHO2 = 5;
const int MAX_DISTANCE = 50; // максимальная дистанция для работы сонаров

int timer = 0;  //переменная хранящая значения таймера
int t_car = 30; //переменная для задания времени для разгрузки машинопотока

//инициализация сонаров в библиотеке NewPing
NewPing sonar1_people (TRIG1, ECHO1, MAX_DISTANCE); //сонар для пешеходов №1
NewPing sonar2_people (TRIG3, ECHO3, MAX_DISTANCE);
NewPing sonar1_car (TRIG2, ECHO2, MAX_DISTANCE);
NewPing sonar2_car (TRIG4, ECHO4, MAX_DISTANCE);

// подключение светодиодов к пинам ардуино
const int red_car = 6;
const int green_car = 7;
const int blue_car = 8;
const int red_people = 9;
const int green_people = 10;
const int blue_people = 11;

void yellow () {                     // функция включения желтого света на светофоре для людей
  analogWrite (red_people, 220);
  analogWrite (green_people, 250);
}
void yellow2 () {                    // функция включения желтого света на светофоре для машин
  analogWrite (red_car, 220);
  analogWrite (green_car, 250);
}
void yellow_off () {                 // функция выключения желтого света на светофоре для людей
  digitalWrite (red_people, LOW);
  digitalWrite (green_people, LOW);
}
void yellow2_off () {                // функция выключения желтого света на светофоре для людей
  digitalWrite (red_car, LOW);
  digitalWrite (green_car, LOW);
}
void blink_yellow (int a) {          // функция моргания желтым светом для всех (в скобочках указывается кол-во циклов включения/выключения)
  for (int i = 0; i < a; i++)
  {
    yellow ();
    yellow2 ();                      // включаем желеые сигналы
    delay (500);                     // ждем
    yellow_off ();
    yellow2_off ();                  // выключаем желтые сигналы
    delay (500);                     // ждем
    i++;                             // увеличиваем счетчик морганий на 1
  }
}
void people_green_light (int t) {    // функция включения зеленого людям и красного машинам (в скобочках пишем СЕКУНДЫ, А НЕ МИЛИСЕКУНДЫ)
  digitalWrite (green_car, LOW);     // зеленый машинам выкл
  digitalWrite (green_people, HIGH); // зеленый людям вкл
  digitalWrite (red_car, HIGH);      // красный машинам вкл
  digitalWrite (red_people, LOW);    // красный людям выкл
  delay (t * 1000);                  // время необходимое людям для перехода через дорогу (В СЕКУНДАХ)
  timer = 0;                         // сброс таймера для нового отсчета (необходимо для разгрузки машинопотока)
}
void car_green_light () {            // функция включения зеленого машинам и красного людям 
  digitalWrite (green_car, HIGH);    // зеленый машинам вкл
  digitalWrite (green_people, LOW);  // зеленый людям выкл
  digitalWrite (red_car, LOW);       // красный машинам выкл
  digitalWrite (red_people, HIGH);   // красный людям вкл
  delay (1000);                      // время выполнения функции 
  timer++;                           // увеличение таймера на 1 для машинопотока
}
void setup() {
  Serial.begin(9600);
  pinMode (red_people, OUTPUT);
  pinMode (green_people, OUTPUT);
  pinMode (blue_people, OUTPUT);
  pinMode (red_car, OUTPUT);
  pinMode (green_car, OUTPUT);
  pinMode (blue_car, OUTPUT);
}

void loop() {
  unsigned int distance_people1 = sonar1_people.ping_cm();   // получение данных от сонара для людей №1
  unsigned int distance_people2 = sonar2_people.ping_cm();   // получение данных от сонара для людей №2
  unsigned int distance_car1 = sonar1_car.ping_cm();         // получение данных от сонара для машин №1
  unsigned int distance_car2 = sonar2_car.ping_cm();         // получение данных от сонара для машин №2

  // присвоение флагов в зависимости от наличия пешеходов или машин
  bool people = ((distance_people1 < 20 && distance_people1 > 0) || (distance_people2 < 20 && distance_people2 > 0));
  bool car = ((distance_car1 < 20 && distance_car1 > 0) || (distance_car2 < 20 && distance_car2 > 0));

  if (!people) {                                             //если нет пешеходов
    car_green_light ();                                      // включаем машинам зеленый свет
  }
  else if (people && car && timer < t_car) {                 //если есть пешеходы и машины и время для разгрузки машинопотока HE закончилось
    car_green_light ();                                      // включаем машинам зеленый свет
  }
  else if (people && car && timer > t_car) {                 // если есть пешеходы и машины и время для разгрузки машинопотока закончилось
    delay (5000);                                            // ждем для избежания ложного срабатывания пешегодного зеленого сигнала
    unsigned int distance_people1 = sonar1_people.ping_cm(); // получение данных от сонара для людей №1
    unsigned int distance_people2 = sonar2_people.ping_cm(); // получение данных от сонара для людей №2
    unsigned int distance_car1 = sonar1_car.ping_cm();       // получение данных от сонара для машин №1
    unsigned int distance_car2 = sonar2_car.ping_cm();       // получение данных от сонара для машин №2

    // присвоение флагов в зависимости от наличия пешеходов или машин
    bool people = ((distance_people1 < 20 && distance_people1 > 0) || (distance_people2 < 20 && distance_people2 > 0));
    bool car = ((distance_car1 < 20 && distance_car1 > 0) || (distance_car2 < 20 && distance_car2 > 0));

    if (people) {                                            // если пешеход не передумал и не ушел
      blink_yellow (5);                                      // моргаем желтым всем 5 раз
      people_green_light(10);                                // включаем пешеходу зеленый свет и сбрасываем таймер
    }
  }
  else if (people && !car && timer < t_car) {                // если есть пешеходы и нет машин, а время для разгрузки машинопотока не закончилось
    delay (5000);                                            //ждем для избежания срабатывания пешегодного зеленого сигнала
    unsigned int distance_people1 = sonar1_people.ping_cm(); // получение данных от сонара для людей №1
    unsigned int distance_people2 = sonar2_people.ping_cm(); // получение данных от сонара для людей №2
    unsigned int distance_car1 = sonar1_car.ping_cm();       // получение данных от сонара для машин №1
    unsigned int distance_car2 = sonar2_car.ping_cm();       // получение данных от сонара для машин №2

    // присвоение флагов в зависимости от наличия пешеходов или машин
    bool people = ((distance_people1 < 20 && distance_people1 > 0) || (distance_people2 < 20 && distance_people2 > 0));
    bool car = ((distance_car1 < 20 && distance_car1 > 0) || (distance_car2 < 20 && distance_car2 > 0));

    if (people && !car) {                                    // если пешеход не передумал и не ушел, а так же не появились машины
      blink_yellow (5);                                      // моргаем желтым всем 5 раз
      people_green_light(10);                                // включаем зеленый пешеходам, обнуляем таймер
    }
  }
}
