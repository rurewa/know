// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Сигнализация на инфракрасном датике движения ПИР.
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
const int buton1 = 2; // дачик движения 
const int buzzer = 11; // пищалка

void setup() {
  pinMode(buton1, INPUT); // указываем состаяние 
  pinMode(buzzer, OUTPUT);

}

void loop() {
   if (digitalRead(buton1) == true) // когда датчик замечает движение
   {
      digitalWrite(buzzer, HIGH); // пищим
   }

   if (digitalRead(buton1) == false) // если нету движения 
   {
      digitalWrite(buzzer, LOW); // перестаем пищать
   }
}
