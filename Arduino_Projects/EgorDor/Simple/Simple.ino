// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Egor D. China car. Простой код для управления моторами
// V 1.7
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>

const int ENA = 9;
const int IN1 = 5;
const int IN2 = 6;
const int IN3 = 7;
const int IN4 = 8;
const int ENB = 3;
/*
// Настройка скорости моторов при поворотах
// Из-за разницы в скорости моторов приходится это компенсировать с помощью ШИМ
const int SPEED_LEFT_TURN = 110; // Скорость левого мотора (50)
const int SPEED_RIGHT_TURN = 110; // Скорость правого мотор
// Настройка скорости моторов при движении вперёд
const int SPEED_RIGHT_MOVE = 170; // Скорость левого мотора (10)
const int SPEED_LEFT_MOVE = 90; // Скорость правого мотора
// Настройка скорости моторов при движении назад
const int SPEED_LEFT_BACK_MOVE = 105; // Скорость правого мотор
const int SPEED_RIGHT_BACK_NOVE = 155; // Скорость правого мотора
// Расстояние поля до кегли
const int distanceToPinsGo = 1150;
const int distanceToPinsBack = 1200;
*/

void setup() {
  Serial.begin(9600); // Монитор порта: для диагностики и тестирования
  pinMode(ENA, OUTPUT); // Скорость (ШИМ) 1-го мотора
  pinMode(IN1, OUTPUT); // Направление 1-го мотора
  pinMode(IN2, OUTPUT);
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT); // Направление 2-го мотора
  pinMode(ENB, OUTPUT); // Скорость (ШИМ) 2-го мотора
}

void loop() {
  // Левое колесо
  analogWrite(ENA, 255); // 0-255
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  // Правое колесо
  analogWrite(ENB, 255); // 0-255
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
