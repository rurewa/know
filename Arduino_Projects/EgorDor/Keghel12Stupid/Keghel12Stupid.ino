// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Egor D. China car. KegelRing 12V
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //

// Пин динамика
const int VOICE = 11;
// Пины драйвера мотора
const int ENA = 9;
const int IN1 = 5;
const int IN2 = 6;
const int IN3 = 7;
const int IN4 = 8;
const int ENB = 3;

// Настройка скорости моторов при движении вперёд
const int SPEED_LEFT_MOVE = 55; // Скорость левого мотора (10)
const int SPEED_RIGHT_MOVE = 80; // Скорость правого мотора
// Настройка скорости моторов при движении назад
const int SPEED_LEFT_BACK_MOVE = 65; // Скорость левого мотор
const int SPEED_RIGHT_BACK_MOVE = 65; // Скорость правого мотора

const int TURN_TIME = 330;

void setup() {
  Serial.begin(9600); // Монитор порта: для диагностики и тестирования
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(VOICE, OUTPUT); // Настройка пина для зумера
  delay(250); // Задержка выполнения всего алгоритма для стабилизации.
}

void loop() {
  int count = 0;
  while (count <= 8) { // Цикл выполнится 8 раз
    ++count;
    switch (count)
    {
      case 1:
        sendMessage(count);
        turnLeft(85, 85, 220); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1250); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1350); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 2:
        sendMessage(count);
        turnLeft(85, 85, 370); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1350); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1350); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 3:
        sendMessage(count);
        turnLeft(85, 85, 380); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1300); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1250); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 4:
        sendMessage(count);
        turnLeft(85, 85, 400); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1400); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1350); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 5:
        sendMessage(count);
        turnLeft(85, 85, 400); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1400); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1350); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 6:
        sendMessage(count);
        turnLeft(85, 85, 410); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1350); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1250); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 7:
        sendMessage(count);
        turnLeft(85, 85, 400); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1350); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1300); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 8:
        sendMessage(count);
        turnLeft(85, 85, 400); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1350); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1300); // Возвращаюсь назад
        moveStop(1000);
        break;
      case 9:
        sendMessage(count);
        turnLeft(85, 85, 410); // Left, Right
        moveStop(1000);
        go(SPEED_LEFT_MOVE, SPEED_RIGHT_MOVE, 1350); // Выбиваю кеглю
        moveStop(1000);
        backMove(SPEED_LEFT_BACK_MOVE, SPEED_RIGHT_BACK_MOVE, 1300); // Возвращаюсь назад
        moveStop(1000);
        break;
      default:
        Serial.println(count);
        Serial.println("Stop!");
        break;
    }
  }
  exit(0); // Завершение программы
}

void go(int speed_left_move, int speed_right_move, int times) {
  analogWrite(ENA, speed_left_move);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENB, speed_right_move);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  delay(times);
}

void backMove(int speed_left_move, int speed_right_move, int times) {
  analogWrite(ENA, speed_left_move);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENB, speed_right_move);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  delay(times);
}

void moveStop(int times) {
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
  delay(times);
}

void turnLeft(int speed_left_turn, int speed_right_turn, int times) {
  analogWrite(ENA, speed_left_turn);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENB, speed_right_turn);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  delay(times);
}

void sendMessage(int count) {
  Serial.println(count);
  Serial.println("Left");
  Serial.println("Forward and Back");
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
