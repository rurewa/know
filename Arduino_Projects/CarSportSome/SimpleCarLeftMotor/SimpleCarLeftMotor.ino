// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Простой код для управления моторами
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>

const int ENA = 3; // Пин скорости
const int IN1 = 5; // Пин направления
const int IN2 = 6;
const int IN3 = 7;
const int IN4 = 8;
const int ENB = 9;

void setup() {
  Serial.begin(9600); // Монитор порта: для диагностики и тестирования
  pinMode(ENA, OUTPUT); // Скорость (ШИМ) 1-го мотора (0-255)
  pinMode(IN1, OUTPUT); // Направление 1-го мотора
  pinMode(IN2, OUTPUT);
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT); // Направление 2-го мотора
  pinMode(ENB, OUTPUT); // Скорость (ШИМ) 2-го мотора (0-255)
}

void loop() {  
  analogWrite(ENB, 100); // Левое колесо. 0-255
  digitalWrite(IN3, HIGH); // Вперёд
  digitalWrite(IN4, LOW);
  delay(2000); // 2 сек.
  // Останов
  analogWrite(ENB, 0);
  delay(2000); // 2 сек.
  analogWrite(ENB, 100); // 0-255
  digitalWrite(IN3, LOW); // Назад
  digitalWrite(IN4, HIGH);
  delay(2000); // 2 сек.
  // Останов
  analogWrite(ENB, 0);
  delay(2000); // 2 сек.
  
  
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
