// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Простой код для теста инфракрасного приёмника
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include "IRremote.h"
IRrecv irrecv(A0);
decode_results results;

// Хранит время последнего нажатия кнопки.
unsigned long _time;

// Опишем коды кнопок макроподстановками:
#define FORWARD 0xFF629D
#define LEFT  0xFF22DD
#define RIGHT 0xFFC23D
#define STOP  0xFF02FD
#define BACK  0xFFA857

const byte SPEED_A = 255;
const byte SPEED_B = 255;

const int ENA = 3; // Пин скорости правого мотора
const int IN1 = 5; // Пин направления правого мотора
const int IN2 = 6;
const int IN3 = 7; // Пин направления левого мотора
const int IN4 = 8; // Пин скорости левого мотора
const int ENB = 9;

void setup() {
  Serial.begin(9600); // Монитор порта: для диагностики и тестирования
  analogWrite(ENA, SPEED_A); // Правое колесо. 0-255
  analogWrite(ENB, SPEED_B); // Левое колесо. 0-255
  pinMode(ENA, OUTPUT); // Скорость (ШИМ) правого мотора (0-255)
  pinMode(IN1, OUTPUT); // Направление 1-го мотора
  pinMode(IN2, OUTPUT);
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT); // Направление левого мотора
  pinMode(ENB, OUTPUT); // Скорость (ШИМ) 2-го мотора (0-255)
  irrecv.enableIRIn();
  _time = millis();
}

void loop() {
  //ir_test(); // Для тестов
  if (irrecv.decode(&results))
  {
    _time = millis();
    switch (results.value) {
      // Вперед
      case FORWARD:
        digitalWrite(IN1, LOW); // Вперёд
        digitalWrite(IN2, HIGH);
        digitalWrite(IN3, LOW); // Вперёд
        digitalWrite(IN4, HIGH);
        break;
      // Назад
      case BACK:
        digitalWrite(IN1, HIGH); // Назад
        digitalWrite(IN2, LOW);
        digitalWrite(IN3, HIGH); // Назад
        digitalWrite(IN4, LOW);
        break;
      // Влево
      case LEFT:
        digitalWrite(IN1, LOW); // Вперёд
        digitalWrite(IN2, HIGH);
        digitalWrite(IN3, LOW); // Назад
        digitalWrite(IN4, LOW);
        break;
      // Вправо
      case RIGHT:
        digitalWrite(IN1, LOW); // Назад
        digitalWrite(IN2, LOW);
        digitalWrite(IN3, LOW); // Вперёд
        digitalWrite(IN4, HIGH);
        break;
    }
    irrecv.resume();
  }
  //Если никакая клавиша не нажата более 0.3сек., то остановка.
  if ((millis() - _time) > 150) {
    digitalWrite(IN1, HIGH); // Назад
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH); // Вперёд
    digitalWrite(IN4, HIGH);
  }
}

void ir_test() {
  if (irrecv.decode(&results)) {
    // Послать полученные данные на ПК в 16-ом представлении.
    Serial.println(results.value, HEX);
    //Готов к приему.
    irrecv.resume();
  }
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
