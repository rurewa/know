// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Простой код для управления моторами
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>

const int ENA = 3; // Пин скорости правого мотора
const int IN1 = 5; // Пин направления правого мотора
const int IN2 = 6;
const int IN3 = 7; // Пин направления левого мотора
const int IN4 = 8; // Пин скорости левого мотора
const int ENB = 9; 

void setup() {
  Serial.begin(9600); // Монитор порта: для диагностики и тестирования
  pinMode(ENA, OUTPUT); // Скорость (ШИМ) правого мотора (0-255)
  pinMode(IN1, OUTPUT); // Направление 1-го мотора
  pinMode(IN2, OUTPUT);
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT); // Направление левого мотора
  pinMode(ENB, OUTPUT); // Скорость (ШИМ) 2-го мотора (0-255)
}

void loop() {
  // Движение вправо
  analogWrite(ENA, 100); // Правое колесо. 0-255
  digitalWrite(IN1, HIGH); // Назад
  digitalWrite(IN2, LOW);
  analogWrite(ENB, 100); // Левое колесо. 0-255
  digitalWrite(IN3, LOW); // Вперёд
  digitalWrite(IN4, HIGH);
  delay(2000); // 2 сек.
  // Останов
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
  delay(2000); // 2 сек.
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
