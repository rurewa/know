#include <SPI.h>                                          // Подключаем библиотеку для работы с шиной SPI
#include <nRF24L01.h>                                     // Подключаем файл настроек из библиотеки RF24
#include <RF24.h>                                         // Подключаем библиотеку для работы с nRF24L01+
#include <DHT.h>
#include <Wire.h>

const int ADXL345 = 0x53;

DHT dht(7, DHT11);
                            // Создаём объект radio для работы с библиотекой RF24, указывая номера выводов nRF24L01+ (CE, CSN)

RF24 radio(9, 10);
float data[5] = {};                                   // Создаём массив для приёма данных
void setup() {
  Serial.begin(9600);
  dht.begin();
  radio.begin();                                        // Инициируем работу nRF24L01+
  radio.setChannel(100);                                  // Указываем канал передачи данных (от 0 до 127), 5 - значит передача данных осуществляется на частоте 2,405 ГГц (на одном канале может быть только 1 приёмник и до 6 передатчиков)
  radio.setDataRate(RF24_1MBPS);                        // Указываем скорость передачи данных (RF24_250KBPS, RF24_1MBPS, RF24_2MBPS), RF24_1MBPS - 1Мбит/сек
  radio.setPALevel(RF24_PA_MAX);                       // Указываем мощность передатчика (RF24_PA_MIN=-18dBm, RF24_PA_LOW=-12dBm, RF24_PA_HIGH=-6dBm, RF24_PA_MAX=0dBm)
  radio.openWritingPipe (0x1234567890LL);               // Открываем трубу с идентификатором 0x1234567890 для передачи данных (на одном канале может быть открыто до 6 разных труб, которые должны отличаться только последним байтом идентификатора)

  Wire.begin(); // Инициализация библиотеки Wire 
  // Установите ADXL345 в режим измерения
  Wire.beginTransmission(ADXL345); // Начать общение с устройством 
  Wire.write(0x2D); // работа с регистром  POWER_CTL  - 0x2D
  // Включить измерение
  Wire.write(8); // (8dec -> 0000 1000 двоичный) Бит D3 High для разрешения измерения
  Wire.endTransmission();
  delay(10);
}

void loop() {
  delay(1000);
    Wire.beginTransmission(ADXL345);
  Wire.write(0x32); // Начать с регистра 0x32 (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(ADXL345, 6, true);

  // посчитаем и отобразим значения X, Y, Z:
  data [2] = float( Wire.read()| Wire.read() << 8)/256;
  data[3] = float(Wire.read()| Wire.read() << 8)/256;
  data[4]= float(Wire.read()| Wire.read() << 8)/256;
  
  data[1] = dht.readHumidity();
  data[0] = dht.readTemperature();
  radio.write(&data, sizeof(data));                     // отправляем данные из массива data указывая сколько байт массива мы хотим отправить. Отправить данные можно с проверкой их доставки: if( radio.write(&data, sizeof(data)) ){данные приняты приёмником;}else{данные не приняты приёмником;}
  for (int i =0; i < 5; i++){
    Serial.print(data[i]);
    Serial.print("\t");
  }
  Serial.println("");
}
