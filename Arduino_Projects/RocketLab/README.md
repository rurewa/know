# Rocket Arduino
---
## Главная
ЦДТТ "Юность"

г.Сергиев Посад

Проект "Rocket lab" или "Спутник"

---

Автор проекта Тимофей Миронюк Никитич

---

Руководитель Александр Александрович Изотов

---

Цель: создание полезной нагрузки для рокеты

---
Задачи:

1.Создание Макета

2.Разроботка прошивки

3.Разработка платы

4.Изнотовление платы

5.Разработка  3d модели holder'а

6.Изготовление holder'а

7.Сборка полезной нагрузки и её испытания

---

В будущем этот проект можно будет использовать метеорологам для сбора данных для прогноза погоды.

---
## Программирование attiny 85 (1)
---
#### Программирование attiny 85 в Linux (1.a)
1.Проверьте если на ПК на котором вы работаете **usb3.0**
- Если есть продолжайте 
- Если нет купите новый ПК с **AMD Ryzen Threadripper PRO и PNY Quadro RTX 8000** :)
---

2.Установка плат
- Заходим в *ARDUINO IDE -->file-->preferences* или по русски *файл-->настройки*
- И в поле доплнительные сылки для менеджера плат ставим сылку **http://digistump.com/package_digistump_index.json**
- Нажимаем **OK**
- Переходим *Инструменты -->Плата --> Менеджер плат*
- Ищем Digistump AVR Boards и устонавливаем
- И перезапускаем Arduino
---

3.Роемся во внутреностях Linux или делаем так чтобы linux видел плату
- переходим в каталог */etc/udev/rules.d/*
-Создаём файл 49-micronucleus.rules
-Открываем файл с помощью команды 
```
sudo micro 49-micronucleus.rules
```
 и вставляем туда этот код
```
# UDEV Rules for Micronucleus boards including the Digispark.
# This file must be placed at:
#
# /etc/udev/rules.d/49-micronucleus.rules    (preferred location)
#   or
# /lib/udev/rules.d/49-micronucleus.rules    (req'd on some broken systems)
#
# After this file is copied, physically unplug and reconnect the board.
#
SUBSYSTEMS=="usb", ATTRS{idVendor}=="16d0", ATTRS{idProduct}=="0753", MODE:="0666"
KERNEL=="ttyACM*", ATTRS{idVendor}=="16d0", ATTRS{idProduct}=="0753", MODE:="0666", ENV{ID_MM_DEVICE_IGNORE}="1"
#
# If you share your linux system with other users, or just don't like the
# idea of write permission for everybody, you can replace MODE:="0666" with
# OWNER:="yourusername" to create the device owned by you, or with
# GROUP:="somegroupname" and mange access using standard unix groups.
```

- Вводим команду:
```
sudo adduser $USER dialout
````

- Вводим команду:
```
sudo udevadm control --reload-rules
```
---
4.Прошиваем сам контролер
- Открываем Arduino
-Выбираем *инструменты --> Плата --> Digistump AVR Boards-->attiny85 default*
-Открываем *файл --> примеры --> Digispark_Examples -->start*
-Нажимаем загрузить не подключая attiny
-Когда вывелось 
```
Running Digispark Uploader...
Plug in device now... (will timeout in 60 seconds)
```
Подключаем attiny через ***USB 3.0***
и ждём. Конец.

---
#### Программирование attiny 85 в Windows (1.b)
1. Снесите винду 
2. Установите linux
3. и переходите к пункту 1.a 
:)

---
## Nano rf

1. File->preferences->settings tab 
вводим:
```
	https://raw.githubusercontent.com/nulllaborg/arduino_nulllab/master/package_nulllab_boards_index.json
```
2. Tools->board->board_manager
находим nallab AVR board

3.выбираем nullab nano
