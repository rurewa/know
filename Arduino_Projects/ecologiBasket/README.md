# Контроль и учёт раздельного сбора отходов
----
## Описание проекта ##
----
  <h5>
    С помощью модели «Контроль и учёт раздельного сбора отходов EcologiBasket», осуществляется экопросветительская деятельность на добровольной основе, способствующее формированию экологически ответственного поведения у подрастающего поколения за счет наглядной подачи, НО это не главная задача проекта! На нем школьники практикут разработку автономного самовывоза при заполнении контейнеров. Работат это будет следующим образом: датчики,установленные ранее в баке при заполнении будут посылать сообщения в спец. службу, тем самым облегчив работу сотрудникам. У них будет отображатся: заполненость (чтоб грамотно составить маршрут обьезда), место, отсек с разновидностью мусора (для подбора машины)
  </h5>

## Фото проекта ##
----
<img
    src="https://gitflic.ru/project/taranova08/ecologi-basket/blob/raw?file=photo%2Fphoto_2023-10-26_16-50-41.jpg&commit=06d426d08b11f6537939e0337dd8124abeba7a91"
    width = 50%
    align="left">
<img
    src="https://gitflic.ru/project/taranova08/ecologi-basket/blob/raw?file=photo%2Fphoto_2023-12-03_16-55-46.jpg&commit=346ca02a6372adaa4a242b6351a2d601ec197c61"
    width = 50% align="right">

---

- [Видео проекта](ecologi_baske_video_small.mp4)
- [Подробное описание проекта (PDF)](/Arduino_Projects/ecologiBasket/doc/docum.pdf)
- [Презентация проекта (PDF)](/Arduino_Projects/ecologiBasket/doc/presentation.pdf)

## Схема подключения ##
----
![Изображение](photo/FritzEco.png)

## Подключение пинов Arduino: ##
----
    Дисплей:
    • const int trig = 5;
    • const int echo = 4;
    • NewPing sonar(trig, echo, 40);
    • unsigned sm = sonar.ping_cm();
    Кнопки:
    • const int BUTTON_RED = 8;
    • const int BUTTON_GREEN = 11;
    Светодиоды:
    • const int RED = 7;
    • const int GREEN = 6;
    Серво:
    • myservo.attach(9);/home/user/ecologi-basket/photo/photo_2023-12-03_16-55-46.jpg
    • myservo2.attach(10); <h4>
    Переменные для подсчета мусора:
    • int count;
    • int count2;

## Код ##
----

 ```c++
 #include <Arduino.h>
#include <NewPing.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>       // Библиотека для работы с I2C экраном
LiquidCrystal_I2C lcd(0x27, 16, 2);  // Устанавливаем дисплей

const int trig = 5;
const int echo = 4;
NewPing sonar(trig, echo, 40);
unsigned sm = sonar.ping_cm();

unsigned long timing;

Servo myservo;
Servo myservo2;

const int BUTTON_RED = 8;
const int BUTTON_BLUE = 11;
const int RED = 7;
const int GREEN = 6;

int count;
int count2;

void setup() {
  pinMode(RED, OUTPUT);    //red
  pinMode(GREEN, OUTPUT);  //green
  pinMode(trig, OUTPUT);

  lcd.init();        // Инициализация LCD
  lcd.begin(16, 2);  // Задаем размерность экрана

  myservo.attach(9);
  myservo2.attach(10);

}

void loop() {
  bool buttonState = digitalRead(BUTTON_RED);
  bool buttonState2 = digitalRead(BUTTON_BLUE);
  sm = sonar.ping_cm();
  if (sm > 0 && sm <= 10) {
    sm = sonar.ping_cm();
    delay(50);
    if (buttonState == false && millis() > 3000) {
      count++;
      digitalWrite(RED, HIGH);
      lcd.backlight();  // Включаем подсветку дисплея
      lcd.clear();      // Очищаем экран перед получением нового значения
      myservo2.write(photo/photo_2023-12-03_16-55-46.jpgphoto/photo_2023-12-03_16-55-46.jpg, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("OPEN");    // Тест на 1-й строке экрана
      delay(2000);
      myservo2.write(20);
      lcd.setCursor(4, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("thank you");
      delay(1500);
    }
    else if (buttonState2 == true && millis() > 3000) {
      count2++;
      digitalWrite(GREEN, HIGH);
      lcd.backlight();  // Включаем подсветку дисплея
      lcd.clear();      // Очищаем экран перед получением нового значения
      myservo.write(180);
      lcd.setCursor(6, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("OPEN");    // Тест на 1-й строке экрана
      delay(2000);
      myservo.write(20);
      lcd.setCursor(4, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("thank you");
      delay(1500);
    }
    else {
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    myservo.write(20);
    myservo2.write(20);
    lcd.backlight();  // Включаем подсветку дисплея

    lcd.setCursor(0, 0);           // курсор на 4-й символ 1-й строки
    lcd.print("Throw the trash");  // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("paper");           // Тест на 1-й строке экрана
    lcd.setCursor(5, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count);              // Значения на 2-й строке экрана

    lcd.setCursor(8, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("plastic");
    lcd.setCursor(15, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count2);              // Значения на 2-й строке экрана
    myservo.write(20);
    myservo2.write(20);
    }
  }
  else {
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    myservo.write(20);
    myservo2.write(20);
    lcd.noBacklight();

    lcd.setCursor(0, 0);           // курсор на 4-й символ 1-й строки
    lcd.print("Throw the trash");  // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("paper");           // Тест на 1-й строке экрана
    lcd.setCursor(5, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count);              // Значения на 2-й строке экрана

    lcd.setCursor(8, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("plastic");
    lcd.setCursor(15, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count2);              // Значения на 2-й строке экрана
    myservo.write(20);
    myservo2.write(20);
  }
}
```
<center>
<h1>
  СПАСИБО ЗА ВНИМАНИЕ!)
</h1>
</center>
