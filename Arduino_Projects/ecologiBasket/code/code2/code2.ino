#include <Arduino.h>
#include <NewPing.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>       // Библиотека для работы с I2C экраном
LiquidCrystal_I2C lcd(0x27, 16, 2);  // Устанавливаем дисплей

const int trig = 5;
const int echo = 4;
NewPing sonar(trig, echo, 40);
unsigned sm = sonar.ping_cm();

unsigned long timing;

Servo myservo;
Servo myservo2;

const int BUTTON_RED = 8;
const int BUTTON_BLUE = 11;
const int RED = 7;
const int GREEN = 6;

int count;
int count2;

void setup() {
  pinMode(RED, OUTPUT);    //red
  pinMode(GREEN, OUTPUT);  //green
  pinMode(trig, OUTPUT);

  lcd.init();        // Инициализация LCD
  lcd.begin(16, 2);  // Задаем размерность экрана

  myservo.attach(9);
  myservo2.attach(10);

}

void loop() {
  bool buttonState = digitalRead(BUTTON_RED);
  bool buttonState2 = digitalRead(BUTTON_BLUE);
  sm = sonar.ping_cm();
  if (sm > 0 && sm <= 10) {
    sm = sonar.ping_cm();
    delay(50);
    if (buttonState == false && millis() > 3000) {
      count++;
      digitalWrite(RED, HIGH);
      lcd.backlight();  // Включаем подсветку дисплея
      lcd.clear();      // Очищаем экран перед получением нового значения
      myservo2.write(180);
      lcd.setCursor(6, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("OPEN");    // Тест на 1-й строке экрана
      delay(2000);
      myservo2.write(20);
      lcd.setCursor(4, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("thank you");
      delay(1500);
    }
    else if (buttonState2 == true && millis() > 3000) {
      count2++;
      digitalWrite(GREEN, HIGH);
      lcd.backlight();  // Включаем подсветку дисплея
      lcd.clear();      // Очищаем экран перед получением нового значения
      myservo.write(180);
      lcd.setCursor(6, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("OPEN");    // Тест на 1-й строке экрана
      delay(2000);
      myservo.write(20);
      lcd.setCursor(4, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("thank you");
      delay(1500);
    }
    else {
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    myservo.write(20);
    myservo2.write(20);
    lcd.backlight();  // Включаем подсветку дисплея

    lcd.setCursor(0, 0);           // курсор на 4-й символ 1-й строки
    lcd.print("Throw the trash");  // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("paper");           // Тест на 1-й строке экрана
    lcd.setCursor(5, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count);              // Значения на 2-й строке экрана

    lcd.setCursor(8, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("plastic");      
    lcd.setCursor(15, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count2);              // Значения на 2-й строке экрана
    myservo.write(20);
    myservo2.write(20);
    }
  } 
  else {
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    myservo.write(20);
    myservo2.write(20);
    lcd.noBacklight();

    lcd.setCursor(0, 0);           // курсор на 4-й символ 1-й строки
    lcd.print("Throw the trash");  // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("paper");           // Тест на 1-й строке экрана
    lcd.setCursor(5, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count);              // Значения на 2-й строке экрана

    lcd.setCursor(8, 1);           // курсор на 4-й символ 1-й строки
    lcd.print("plastic");      
    lcd.setCursor(15, 1);           // курсор на 7-й символ 2-й строки
    lcd.print(count2);              // Значения на 2-й строке экрана
    myservo.write(20);
    myservo2.write(20);
  }
}
