#include <Arduino.h>
#include <NewPing.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>       // Библиотека для работы с I2C экраном
LiquidCrystal_I2C lcd(0x27, 16, 2);  // Устанавливаем дисплей

const int trig = 5;
const int echo = 4;
NewPing sonar(trig, echo, 40);
unsigned sm = sonar.ping_cm();

Servo myservo;
Servo myservo2;

const int BUTTON = 8;
const int RED = 7;
const int GREEN = 6;

int count;

void setup() {
  pinMode(RED, OUTPUT);    //red
  pinMode(GREEN, OUTPUT);  //green
  pinMode(trig, OUTPUT);

  lcd.init();  // Инициализация LCD
  //lcd.backlight();   // Включаем подсветку дисплея
  lcd.begin(16, 2);  // Задаем размерность экрана

  myservo.attach(9);
  myservo2.attach(10);
}

void loop() {
  sm = sonar.ping_cm();
  if (sm > 0 && sm <= 10) {
    //delay(100);
    sm = sonar.ping_cm();
    bool buttonState = digitalRead(BUTTON);
    if (buttonState == true) {
      digitalWrite(RED, LOW);
      digitalWrite(GREEN, LOW);
      //lcd.clear(); // Очищаем экран перед получением нового значения
      //lcd.init();
      lcd.backlight();               // Включаем подсветку дисплея
      lcd.setCursor(0, 0);           // курсор на 4-й символ 1-й строки
      lcd.print("Throw the trash");  // Тест на 1-й строке экрана
      lcd.setCursor(0, 1);           // курсор на 4-й символ 1-й строки
      lcd.print("opened");           // Тест на 1-й строке экрана
      lcd.setCursor(7, 1);           // курсор на 7-й символ 2-й строки
      lcd.print(count);              // Значения на 2-й строке экрана
      myservo.write(20);
      myservo2.write(10);
    } else {
      count++;
      digitalWrite(RED, HIGH);
      digitalWrite(GREEN, HIGH);
      //lcd.init();        // Инициализация LCD
      lcd.backlight();      // Включаем подсветку дисплея
      lcd.clear();          // Очищаем экран перед получением нового значения
      lcd.setCursor(6, 0);  // курсор на 4-й символ 1-й строки
      lcd.print("OPEN");    // Тест на 1-й строке экрана
      myservo.write(180);
      myservo2.write(180);
      delay(1000);
    }
  } else {
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    myservo.write(20);
    myservo2.write(10);
    delay(500);
    lcd.noBacklight();
  }
}
