#include <Arduino.h>
#include <LiquidCrystal_I2C.h> // Библиотека для работы с I2C экраном
LiquidCrystal_I2C lcd(0x27,16,2);  // Устанавливаем дисплей

int val;
int valPWM;

void setup() {
  lcd.init(); // Инициализация LCD
  lcd.backlight();// Включаем подсветку дисплея
  lcd.begin(16, 2);  // Задаем размерность экрана
  
}

void loop() {
  lcd.clear(); // Очищаем экран перед получением нового значения
  lcd.setCursor(0, 0); // курсор на 4-й символ 1-й строки
  lcd.print("Analog:"); // Тест на 1-й строке экрана
    lcd.setCursor(9, 0); // курсор на 7-й символ 2-й строки
    lcd.print(val); // Значения на 2-й строке экрана
  lcd.setCursor(0, 1); // курсор на 4-й символ 1-й строки
  lcd.print("PWM:"); // Тест на 1-й строке экрана
  lcd.setCursor(9, 1); // курсор на 7-й символ 2-й строки
  lcd.print(valPWM); // Значения на 2-й строке экрана
  delay(100);
}
