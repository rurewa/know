#include <Servo.h>

Servo myservo;
Servo myservo2;
const int BUTTON = 8;

void setup() {
  myservo.attach(9);
  myservo2.attach(10);
}

void loop() {
  bool buttonState = digitalRead(BUTTON);
  if (buttonState == true) {
    myservo.write(20);
    myservo2.write(10);

  }
  else{
    myservo.write(180);
    myservo2.write(180);
    delay(1000);
  }
}
