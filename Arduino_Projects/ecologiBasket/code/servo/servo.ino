#include <Servo.h>
#include <Arduino.h>
#include <NewPing.h>

Servo servo1;
Servo servo2;
Servo servo3;

const int trig = 9;
const int echo = 10;

NewPing sonar(trig, echo, 40);
unsigned sm = sonar.ping_cm();

void setup() {
  servo1.attach(5);
  servo2.attach(6);
  servo3.attach(7);
  pinMode(trig, OUTPUT);

}

void loop() {
  sm = sonar.ping_cm();
  if (sm > 0 && sm <= 5) {
    delay(2000);
    sm = sonar.ping_cm();
    if (sm > 0 && sm <= 5) {
      servo1.write(0);
      delay(1500);
      servo1.write(180);
      delay(1500);
      servo1.write(0);
 }
    else {
      servo1.write(0);
      servo1.write(0); 
      servo1.write(0); 
      delay(1500);
    }
  } 
//././././././././././././././././././././././././././././././././
  sm = sonar.ping_cm();
  if (sm > 5 && sm <= 10) {
    delay(2000);
    sm = sonar.ping_cm();
    if (sm > 5 && sm <= 10) {
      servo2.write(0);
      delay(1500);
      servo2.write(180);
      delay(1500);
      servo2.write(0);
 }
    else {
      servo1.write(0);
      servo2.write(0); 
      servo3.write(0); 
      delay(1500);
    }
  } 
//././././././././././././././././././././././././././././././././
 sm = sonar.ping_cm();
  if (sm > 10 && sm <= 15) {
    delay(2000);
    sm = sonar.ping_cm();
    if (sm > 10 && sm <= 15) {
      servo3.write(0);
      delay(1500);
      servo3.write(180);
      delay(1500);
      servo3.write(0);
 }
    else {
      servo1.write(0);
      servo2.write(0); 
      servo3.write(0); 
      delay(1500);
    }
  }
 }
