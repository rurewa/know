# Arduino Radar

## Arduino Radar - это проект позволяющий обнаруживать обЪекты благодаря ультразвуковым волнам.

Проект собран на базе Arduino Nano и сонара HY-SRF05. Он определять растояние до предмета даже в темноте и выводит все графические данные на компьютер.

Он может помочь в сканировании местности темной точью или автоматическом определении обЪектов, спектр его задачь может свободно расширяться.

![Radar](/Arduino_Projects/sonarTurel/content/img/view.jpg)

### Навигация по проекту

[Код Arduino](/Arduino_Projects/sonarTurel/content/radar/radar_ardu/)

[Код для Processing](/Arduino_Projects/sonarTurel/content/radar/radar_processing/radar_proc/)

### Полезные ресурсы

[Arduino Radar Project](https://howtomechatronics.com/projects/arduino-radar-project/)

[Arduino Radar](https://projecthub.arduino.cc/noneedforit/arduino-radar-ce9086)

[arduino_radar](https://github.com/faweiz/arduino_radar/tree/master)

