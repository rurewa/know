#include <Servo.h>
enum { PIN_PHOTO_SENSOR = A0, NUMBER_TRIGG = 4, SENS = 900 };
// SENS - Максимальная чувствительность фоторезистора
Servo servo1;
Servo servo2;
Servo servo3;

void setup() {
  servo3.attach(9);
  servo2.attach(10);
  servo1.attach(11);
  Serial.begin(9600);
  servo1.write(5);
  servo2.write(5);
  servo3.write(5);
}

void loop() {
  static bool lastPhotoResistor = false; // Предыдущий статус фоторезиатора
  static bool currentPhotoResistor = false; // Текущий статус фоторезиатора
  static int counterTrigg = 0; // Счётчик срабатываний
  currentPhotoResistor = sensit(SENS); // Считываем состояние фоторизестора
  if (currentPhotoResistor != lastPhotoResistor) {
    if (currentPhotoResistor == true) {
      ++counterTrigg;
      trigg(counterTrigg); // Переключаем сервы по очереди
      if (counterTrigg >= NUMBER_TRIGG) { // Предел срабатываний
        counterTrigg = 0; // Сбрасываем счётчик
      }
    }
    else {
      trigg(0); // Между срабатываниями
    }
    delay(10); // Для стабилизации срабатываний
  }
  lastPhotoResistor = currentPhotoResistor; // Фиксируем состояние фоторезистора
}

bool sensit(int sens) { // Чувствительность фоторезистора
  int sensitivity = analogRead(PIN_PHOTO_SENSOR);
  return ((sensitivity > sens) ? true : false); // Возвращаем true, если интенсивность света достаточна
}

void trigg(int score) {
  if (score == 1) { // 1-е срабатывание
    Serial.print("Срабатывание 1\t");
    Serial.println("Servo 1 и 2 в 100\t");
    servo1.write(100);
    servo2.write(100);
  }
  // 2-е срабатывание
  else if (score == 2) {
    Serial.print("Срабатывание 2\t");
    Serial.println("Servo 1 и 2 в 100\t");
    servo1.write(100);
    servo2.write(100);
  }
  // 4-е срабатывание
  else if (score == 3) {
    Serial.print("Срабатывание 3\t");
    Serial.println("Servo 1 и 2 в 100\t");
    servo1.write(100);
    servo2.write(100);
  }
  else if (score == 4) {
    Serial.print("Срабатывание 4\t");
    Serial.println("Servo 3 в 90\t");
    servo3.write(90);
  }
  else { // score = 0 || > 5
    Serial.println("Все Servo в 0");
    servo1.write(5);
    servo2.write(5);
    servo3.write(5);
  }
}
