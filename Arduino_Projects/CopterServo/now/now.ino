#include <Servo.h>
enum { PIN_PHOTO_SENSOR = A0, NUMBER_TRIGG = 8, SENSITIVITY = 900 };
// SENSITIVITY - Максимальная чувствительность фоторезистора
bool lastPhotoResistor = false; // Предыдущий статус фоторезиатора
bool currentPhotoResistor = false; // Текущий статус фоторезиатора
Servo servo1;
Servo servo2;
Servo servo3;

void setup() {
  servo3.attach(9);
  servo2.attach(10);
  servo1.attach(11);
  Serial.begin(9600);
  servo1.write(0);
  servo2.write(0);
  servo3.write(0);
}

void loop() {
  static int counterTrigg = 0; // Счётчик срабатываний
  currentPhotoResistor = statPhotoResistor(lastPhotoResistor); // Чтение состояние фоторезиатора
  if (lastPhotoResistor == false && currentPhotoResistor == true) {
    ++counterTrigg; // Количество срабатываний
  }
  lastPhotoResistor = currentPhotoResistor; // Обновление состояния фоторезитора
  if (counterTrigg >= NUMBER_TRIGG) { // Ограничение срабатываний
    counterTrigg = 0; // Сбрасываем счётчик
  }
  trigg(counterTrigg); // Переключаем сервы по очереди
}

bool sensitivity(int sens) { // Чувствительность фоторезистора
  int sensitivity = analogRead(PIN_PHOTO_SENSOR);
  return ((sensitivity > sens) ? true : false); // Возвращаем true, если интенсивность света достаточна
}

bool statPhotoResistor(bool last) { // Защита от ложных срабатываний фоторезистора
  int current = sensitivity(SENSITIVITY);
  if (last != current) {
    delay(5); // Немного ждём
    current = sensitivity(SENSITIVITY);
  }
  return current; // Возвращаем состояние фоторезистора
}

void trigg(int score) {
  if (score == 1) { // 1-е срабатывание
    Serial.print("Срабатывание 1\t");
    Serial.println("Servo 1 и 2 в 100\t");
    servo1.write(100);
    servo2.write(100);
  }
  // 2-е срабатывание для возврата всех серво в 0 град.
  else if (score == 2) {
    Serial.print("Срабатывание 2\t");
    Serial.println("Все Servo в 0");
    servo1.write(0);
    servo2.write(0);
    servo3.write(0);
  }
  else if (score == 3) { // 3-е срабатывание
    Serial.print("Срабатывание 3\t");
    Serial.println("Servo 1 и 2 в 100\t");
    servo1.write(100);
    servo2.write(100);
  }
  // 4-е срабатывание для возврата всех серво в 0 град.
  else if (score == 4) {
    Serial.print("Срабатывание 4\t");
    Serial.println("Все Servo в 0");
    servo1.write(0);
    servo2.write(0);
    servo3.write(0);
  }
  else if (score == 5) { // 5-е срабатывание
    Serial.print("Срабатывание 5\t");
    Serial.println("Servo 1 и 2 в 100\t");
    servo1.write(100);
    servo2.write(100);
  }
  // 6-е срабатывание для возврата всех серво в 0 град.
  else if (score == 6) {
    Serial.print("Срабатывание 6\t");
    Serial.println("Все Servo в 0");
    servo1.write(0);
    servo2.write(0);
    servo3.write(0);
  }
  else if (score == 7) { // 7-е срабатывание
    Serial.print("Срабатывание 7\t");
    Serial.println("Servo 3 в 90\t");
    servo3.write(90);
  }
  else { // 8-е срабатывание (score = 0 || > 7)
    Serial.println("Все Servo в 0");
    servo1.write(0);
    servo2.write(0);
    servo3.write(0);
  }
}
