
// include the library code:
#include <LCD_HD44780.h>
int nw = 0;
int sc = 0;
int mn = 0;
int hr = 0;
int dy = 0;
// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, rw = 10, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LCD_HD44780 lcd(rs, rw, en, d4, d5, d6, d7);

void setup() {
  analogWrite(A0, 127);
  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);
  // Print a message to the LCD.
  lcd.print("hello, world!");
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  nw = millis() / 1000;
  sc = nw % 60;
  mn = (nw / 60) % 60;
  hr = (nw / 3600)%24;
  dy = (nw/(3600*24));

  if (hr < 10) lcd.print(0);
  lcd.print(hr);
  lcd.print(":");
  if (mn < 10) lcd.print(0);
  lcd.print(mn);
  lcd.print(":");
  if (sc < 10) lcd.print(0);
  lcd.print(sc);
}
