  
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <NewPing.h>

const unsigned int ECHO_PIN = 17;
const unsigned int TRIG_PIN =  18 ;

NewPing sonar(TRIG_PIN, ECHO_PIN, 100); 

const int ENA = 3;
const int IN1 = 8;
const int IN2 = 7;
const int IN3 = 5;
const int IN4 = 6;
const int ENB = 9;

const int SPEED_LEFT = 100;
const int SPEED_RIGHT = 100;

void turnGo(int speed_left, int speed_right, int times);
void turnBack(int speed_left, int speed_right, int times);
void turnStop(int times);
void turnLeft(int speed_left, int speed_right, int times);
void turnRight(int speed_left, int speed_right, int times);

void turnGo(int speed_left, int speed_right, int times) {
  analogWrite(ENA, speed_left);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENB, speed_right);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  delay(times);
}

void turnBack(int speed_left, int speed_right, int times) {
  analogWrite(ENA, speed_left);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENB, speed_right);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  delay(times);
}

void turnStop(int times) {
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
  delay(times);
}

void turnLeft(int speed_left, int speed_right, int times) {
  analogWrite(ENA, speed_left);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENB, speed_right);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  delay(times);
}

void turnRight(int speed_left, int speed_right, int times) {
  analogWrite(ENA, speed_left) ;
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENB, speed_right);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  delay(times);
}

void setup() {
  Serial.begin(9600);
  pinMode(4, OUTPUT); //Buzzer
  //Setup Channel A
  pinMode(12, OUTPUT); //Motor A1
  //Setup Channel B
  pinMode(13, OUTPUT);  //Motor B1
  pinMode(TRIG_PIN, OUTPUT);
}
void loop() {
  int distance = sonar.ping_cm();
  Serial.println(distance);

  if((distance > 1) && (distance < 45)) {
    turnBack(100, 100, 450);
  }
  else {
    turnRight(100, 100, 50);
  delay(100);
} 
}
