// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Автоналивалка
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>
const int sw = 3; // Концевик

void setup() {
  Serial.begin(9600);
  pinMode(3, INPUT_PULLUP); // Концевик
  pinMode(5, OUTPUT); // Насос
}

void loop() {
  Serial.println(digitalRead(sw));
  bool statSw = digitalRead(sw);
  if (statSw == 0) {
    digitalWrite(5, HIGH);
  }
  else {
    digitalWrite(5, LOW);
  }
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
