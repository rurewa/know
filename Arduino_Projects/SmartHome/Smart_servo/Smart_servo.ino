// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Управление сервоприводом при помощи ИК пульта.
// V 1.2
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include "IRremote.h"
#include <Servo.h>
IRrecv irrecv(2);
decode_results results;
Servo my_servo;

static int ugol = 0;
#define max_ugol 180

void setup() {
  Serial.begin(9600);
  irrecv.enableIRIn();
  my_servo.attach(5); // пин сервопривода
}

void loop() {
  if (irrecv.decode(&results)) { // если данные пришли
  Serial.println(results.value); // отправляем полученные данные на порт
  // управляем углом сервопривода
  if (results.value == 16) {
    ugol++;
  }
  if (results.value == 2064) {
    ugol--;
  }
   irrecv.resume(); // принимаем следующий сигнал на ИК приемнике
  }
  if (ugol == max_ugol) my_servo.write(180);
  my_servo.write(ugol);
}
