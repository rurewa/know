/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
  Smart_Home_Code
  V2.1
  
  How use?
  1 - led1, 2 - led2, 3 - led3, 4 - fan, 5 - pir control.
  0 - off; 1 - on;
  +/- servo control ugol
  =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
*/

// enum тип данных ардуино похожий на массив const
// here save ik signals
enum {
  right = 16761405,// right
  left = 16720605, // left
  led1 = 16738455, // 1 led
  led2 = 16750695, // 2 led
  led3 = 16756815, // 3 led
  srv_max = 16736925, // low
  srv_min = 16754775, // high
  pir = 16718055, // pir_control
  fan = 16724175, // fan (4pin)
  SS_PIN = 10,    // rfid pin
  RST_PIN = 9     // rfid pin
};

//======== БИБИЛИОТЕКИ =========//
#include <SPI.h>
#include <MFRC522.h>  // for rfid 
#include <DHT.h>      // for dht 11
#include <Servo.h>    // for servo
#include <IRremote.h> // for ik
#include <LiquidCrystal_I2C.h>
MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key; // rfid
LiquidCrystal_I2C lcd(0x27, 16, 2);
decode_results results;  // ik
DHT dht(A0, DHT11); // pin dht
IRrecv irrecv(2);   // pin ik sensor
Servo servo;

//======== НАСТРОЙКИ/ПЕРЕМЕННЫЕ ========//
byte nuidPICC[4]; // for rfid librery
static byte pin_arr[] = {7, 8, A1, 6}; // pins led and fan (6 is fan pin)
static byte flag_arr[8];
static byte butt_arr[9];
uint32_t tim;
bool flag;

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
  pinMode(2, INPUT); // pin ik
  pinMode(4, INPUT); // first pir
  pinMode(5, INPUT); // second pir
  // set pins leds and fan
  pinMode(pin_arr[0, 1, 2, 3], OUTPUT);
  servo.attach(3); // pin servo
  servo.write(0);  // set servo
  irrecv.enableIRIn();
  SPI.begin();     // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522
  dht.begin();     // begin DHT11

  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
}

byte i = 0; // служебная перемення
bool data_flag;

void loop() {
  rfidScan();
  Serial.println(results.value); // отправляем полученные данные на порт
  if (rfid.uid.uidByte[0] == 91 &&
      rfid.uid.uidByte[1] == 40 &&
      rfid.uid.uidByte[2] == 112 &&
      rfid.uid.uidByte[3] == 139) {

// ================================= SKREEN FUNCTIONS ================================= //
    if (data_flag == false) {
      infoScreen();
    } else {
      dhtSkreen();
    }
// ======================================= SWITCH ==================================== //
    if (irrecv.decode(&results)) {   // если данные пришли выполняем команды
      if (millis() - tim > 80) {     // обработчик нажатий c ik
        switch (results.value) {
          case led1:
            butt_arr[0] = 1; // led 1
            i = 0;
            break;
          case led2:
            butt_arr[1] = 1; // led 2
            i = 1;
            break;
          case led3:
            butt_arr[2] = 1; // led 3
            i = 2;
            break;
          case fan:
            butt_arr[3] = 1; // fan
            i = 3;
            break;
          case pir:
            butt_arr[4] = 1; // pir
            i = 4;
            break;
          case right:
            lcd.clear();
            data_flag = true;
            break;
          case left:
            lcd.clear();
            data_flag = false;
            break;
        }
        tim = millis();
      }

// ============================= SERVO ============================ //
      // control servo
      if (results.value == srv_max) {
        servo.write(180); // open
        butt_arr[5] = 1;
        i = 5;
      }
      if (results.value == srv_min) {
        servo.write(0);  // close
        butt_arr[6] = 1;
        i = 6;
      }
      irrecv.resume(); // принимаем следующий сигнал на ik приемнике
    }
// ==================================== ALGORITMS =============================== //
    // Имитация отпускания
    if (millis() - tim > 300) {
      tim = millis();
      butt_arr[i] = 0;
    }
    // Алгоритм фиксированой кнопки
    if (butt_arr[i] == 1 && flag == 0) {
      flag_arr[i] = !flag_arr[i];
      flag = 1;
      lcd.clear();
    }
    if (butt_arr[i] == 0 && flag == 1) {
      flag = 0;
    }
    digitalWrite(pin_arr[i], flag_arr[i]); // вкл. и выкл. всей перефирии
  }
}

// функция для pir дтчиков
void pir_control() {
  bool flag_break = 1;
  bool flag_from = 1;
  // on all leds
  if (digitalRead(4) == 1 && flag_from == 1) {
    flag_break = 0;
    flag_arr[0, 1, 2] = 1;
    digitalWrite(7, HIGH);
    digitalWrite(8, HIGH);
    digitalWrite(9, HIGH);
  }
  // off all leds
  if (digitalRead(5) == 1 && flag_break == 1) {
    flag_from = 0;
    flag_arr[0, 1, 2] = 0;
    digitalWrite(7, LOW);
    digitalWrite(8, LOW);
    digitalWrite(9, LOW);
  }
}
// ====================================== INFO_SCREEN ================================ //
const char* lcd_work = "";

void infoScreen() {
  byte r = 0;
  if (servo.read() < 100) lcd_work = "L1 L2 L3 fan  ||"; // if garage is close
  if (servo.read() > 170) lcd_work = "L1 L2 L3 fan  /|"; // if garage is open
  // print on lcd pir-control function
  if (flag_arr[4] == 1) {
    lcd_work = "Pir control - on";
    pir_control();
  }
  lcd.setCursor(0, 0);
  lcd.print(lcd_work);
  // print status leds 1 - on, 0 - off
  while (r < 4) {
    if (flag_arr[r] == 1) { // сканируем первые 4 переменные
      lcd.setCursor(r * 3, 1);
      lcd.print("1"); // led on
    }
    if (flag_arr[r] == 0) {
      lcd.setCursor(r * 3, 1);
      lcd.print("0"); // led off
    }
    r++;
  }
}

void dhtSkreen() {
  // считываем температуру (t) и влажность (h)
  int h = dht.readHumidity();
  int t = dht.readTemperature();

  // выводим температуру (t) и влажность (h) на монитор порта
  Serial.print("Humidity: ");
  Serial.println(h);
  Serial.print("Temperature: ");
  Serial.println(t);

  lcd.setCursor(0, 0);
  lcd.print("Humid:"); // влажность
  lcd.setCursor(7, 0);
  lcd.print(h); // выводим влажность
  //lcd.write(255);
  lcd.setCursor(0, 3);
  lcd.print("Tempe:"); // температура
  lcd.setCursor(7, 3);
  lcd.print(t); // выводим температуру
}

// ========================================== RFID FUNCTION ================================= //
void rfidScan() {
  // Reset the loop if no new card present on the sensor/reader.
  if ( ! rfid.PICC_IsNewCardPresent())
    return;

  // Verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return;

  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }
  // Check new card
  if (rfid.uid.uidByte[0] != nuidPICC[0] ||
      rfid.uid.uidByte[1] != nuidPICC[1] ||
      rfid.uid.uidByte[2] != nuidPICC[2] ||
      rfid.uid.uidByte[3] != nuidPICC[3] ) {
    Serial.println(F("A new card has been detected."));

    // Store NUID into nuidPICC array
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
    // Serial print a NUID new card
    Serial.print(F("In dec: "));
    printDec(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();
  }
  else Serial.println(F("Card read previously."));

  // Halt PICC
  rfid.PICC_HaltA();
  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
}

//Helper routine to dump a byte array as dec values to Serial.
void printDec(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}
