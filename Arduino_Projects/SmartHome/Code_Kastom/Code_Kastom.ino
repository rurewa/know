// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
// Smart_Home_Code
// V1.2
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //

#include <IRremote.h> // подключаем библиотеку для IR приемника
IRrecv irrecv(2);     // указываем пин, к которому подключен IR приемник
decode_results results;

uint32_t tim;
static int led_arr[] = {7, 8, 9};
static int butt_arr[4];
static int flag_arr[4];

void setup() {
  Serial.begin(9600);  // подключаем монитор порта
  irrecv.enableIRIn(); // запускаем прием инфракрасного сигнала
  // Пины светодиодов
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
}

void loop() {
  static bool flag;
  static byte i = 0;

  if (irrecv.decode(&results)) {   // если данные пришли выполняем команды
    //Serial.println(results.value); // отправляем полученные данные на порт
    if (millis() - tim > 100) {     // обработчик нажатий c IK
      switch (results.value) {
        case 16:
          butt_arr[0] = 1;
          i = 0;
          break;
        case 2064:
          butt_arr[1] = 1;
          i = 1;
          break;
        case 1040:
          butt_arr[2] = 1;
          i = 2;
          break;
      }
      tim = millis();
    }
    irrecv.resume(); // принимаем следующий сигнал на ИК приемнике
  }
  // Имитация отпускания
  if (millis() - tim > 300) {
    tim = millis();
    butt_arr[i] = 0;
  }
  // Алгоритм фиксированой кнопки
  if (butt_arr[i] == 1 && flag == 0) {
    flag_arr[i] = !flag_arr[i];
    flag = 1;
  }
  if (butt_arr[i] == 0 && flag == 1) {
    flag = 0;
  }
  digitalWrite(led_arr[i], flag_arr[i]);
  Serial.println(flag_arr[i]);
}
