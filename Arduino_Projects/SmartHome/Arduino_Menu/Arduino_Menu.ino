// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
// Arduino Menu
// V 1.0
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
#include <Wire.h>
#include "LiquidCrystal_I2C_Menu_Btns.h"
LiquidCrystal_I2C_Menu_Btns lcd(0x27, 16, 2);

// Объявление ключей для showMenu
enum {mkBack, mkRoot, mkHello, mkInputVal, mkON, mkOFF, ledON1, ledOFF1, ledON2, ledOFF2, ledON3, ledOFF3};

// Пины кнопок
#define pinLeft  6
#define pinRight 4
#define pinEnter 5
// Пины светодиодов
#define led1 12 
#define led2 11
#define led3 10
uint32_t tim; 

// Нстройка меню
// Структура пункта меню: {ParentKey, Key, Caption, [Handler]}
sMenuItem menu[] = {
  {mkBack, mkRoot, "Test menu!"},
  {mkRoot, mkHello, "Hello, user!"},
  {mkRoot, mkInputVal, "Control led"},
    {mkInputVal, mkON, "ON"},
      {mkON, ledON1, "Led on 1"},
      {mkON, ledON2, "Led on 2"},
      {mkON, ledON3, "Led on 3"},
      {mkON, mkBack, "Back"},
    {mkInputVal, mkOFF, "OFF"},
      {mkOFF, ledOFF1, "Led off 1"},
      {mkOFF, ledOFF2, "Led off 2"},
      {mkOFF, ledOFF3, "Led off 3"},
      {mkOFF, mkBack, "Back"},
    {mkInputVal, mkBack, "Back"},
    {mkRoot, mkBack, "Exit menu"},
};

uint8_t menuLen = sizeof(menu) / sizeof(sMenuItem);

void setup() {
  Serial.begin(9600);
  lcd.begin();
//lcd.attachIdleFunc(); // функции работающие вне библиотеки
  lcd.attachButtons(pinLeft, pinRight, pinEnter);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
}

void loop() {
// Показываем меню
  uint8_t selectedMenuItem = lcd.showMenu(menu, menuLen, 1);
//И выполняем действия в соответствии с выбранным пунктом
  if (selectedMenuItem == ledON1)
    digitalWrite(led1, HIGH);
  if (selectedMenuItem == ledOFF1)
    digitalWrite(led1, LOW);
  if (selectedMenuItem == ledON2)
    digitalWrite(led2, HIGH);
  if (selectedMenuItem == ledOFF2)
    digitalWrite(led2, LOW);
  if (selectedMenuItem == ledON3)
    digitalWrite(led3, HIGH);
  if (selectedMenuItem == ledOFF3)
    digitalWrite(led3, LOW);
  while (lcd.getButtonsState() == eNone);
}
