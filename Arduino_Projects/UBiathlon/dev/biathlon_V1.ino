const int LMS = 5;//За скорость левый
const int LMD = 2;//HIGH-вперед, LOW-назад
const int RMS = 6;//За скорость правый
const int RMD = 4;

const int DL = 9;//Крайний левый датчик
const int DC = 8;//Центральный датчик
const int DR = 7;//Крайний правый датчик

void setup() {
  pinMode(LMS, OUTPUT);
  pinMode(LMD, OUTPUT);
  
  pinMode(RMS, OUTPUT);
  pinMode(RMD, OUTPUT);
}
void loop() {
  
  while(digitalRead(DC) == 0){
    digitalWrite(LMD, HIGH);
    analogWrite(LMS, 164);
    digitalWrite(RMD, HIGH);
    analogWrite(RMS, 160);
  }
  digitalWrite(LMD, HIGH);
  analogWrite(LMS, 0);
  digitalWrite(RMD, HIGH);
  analogWrite(RMS, 0);
  
  if(digitalRead(DL) == 0 && digitalRead(DR) == 1 && digitalRead(DC) == 1){
    while(digitalRead(DC) == 1){
      digitalWrite(LMD, HIGH);
      analogWrite(LMS, 100);
      digitalWrite(RMD, HIGH);
      analogWrite(RMS, 195);
    }
  }
  
  if(digitalRead(DL) == 1 && digitalRead(DR) == 0 && digitalRead(DC) == 1){
    while(digitalRead(DC) == 1){
      digitalWrite(LMD, HIGH);
      analogWrite(LMS, 203);
      digitalWrite(RMD, HIGH);
      analogWrite(RMS, 75);
    }
  }
}
