const int SPEED_LEFT_GO = 85;
const int SPEED_RIGHT_GO = 80;

enum {
  LEFT_MOTOR_SPEED = 5,
  LEFT_MOTOR_DIRECTION = 2,
  RIGHT_MOTOR_SPEED = 6,
  RIGHT_MOTOR_DIRECTION = 4,
  LEFT_SENS = 7,
  MIDL_SENS = 8,
  RIGHT_SENS = 9,
};
void setup() {
  Serial.begin(9600);
  pinMode(LEFT_MOTOR_SPEED, OUTPUT);
  pinMode(LEFT_MOTOR_DIRECTION, OUTPUT);
  pinMode(RIGHT_MOTOR_SPEED, OUTPUT);
  pinMode(RIGHT_MOTOR_DIRECTION, OUTPUT);
  delay(500); // Задержка выполнения всего алгоритма для стабилизации.
}
void loop() {
  bool leftSens = !digitalRead(LEFT_SENS), midleSens = !digitalRead(MIDL_SENS), rightSens = !digitalRead(RIGHT_SENS);
  //Serial.print(leftSens);
  //Serial.print(midleSens);
  //Serial.println(rightSens);
  if (leftSens == false && midleSens == true && rightSens == false) { // 010
    Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 400);
    Serial.println("Go");
  }
  else if (leftSens == true && midleSens == false && rightSens == false) { // 100
    Go(25, 85, 1000);
    Serial.println("Left");
  }
  else if (leftSens == false && midleSens == false && rightSens == true) { // 001
    Go(85, 25, 1000);
    Serial.println("Right");
  }
  else {
    Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1);
  }
}
void Go(int SPEED_LEFT_GO, int SPEED_RIGHT_GO, int TIME) {
  digitalWrite(LEFT_MOTOR_DIRECTION, HIGH);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  digitalWrite(RIGHT_MOTOR_DIRECTION, HIGH);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  delayMicroseconds(TIME);
}
void Stop(int TIME) {
  analogWrite(LEFT_MOTOR_SPEED, 0);
  analogWrite(RIGHT_MOTOR_SPEED, 0);
  delay(TIME);
}
void Right(int SPEED_LEFT_GO, int SPEED_RIGHT_GO, int TIME) {
  digitalWrite(RIGHT_MOTOR_DIRECTION, LOW);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  digitalWrite(LEFT_MOTOR_DIRECTION, HIGH);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  delay(TIME);
}
void Left(int SPEED_RIGHT_GO, int SPEED_LEFT_GO, int TIME) {
  digitalWrite(LEFT_MOTOR_DIRECTION, LOW);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  digitalWrite(RIGHT_MOTOR_DIRECTION, HIGH);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  delay(TIME);
}
void Back(int SPEED_LEFT_GO, int SPEED_RIGHT_GO, int TIME) {
  digitalWrite(LEFT_MOTOR_DIRECTION, LOW);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  digitalWrite(RIGHT_MOTOR_DIRECTION, LOW);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  delay(TIME);
}

void MotorTest() {
  Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1000);
  Stop(500);
  Right(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1000);
  Stop(500);
  Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1000);
  Stop(500);
}

void sendMessage(int count) {
  Serial.println(count);
  Serial.println("Left");
  Serial.println("Forward and Back");
}
/*
  void ir() {
  Serial.print(leftSens);
  Serial.print(midleSens);
  Serial.println(rightSens);
  }
*/
