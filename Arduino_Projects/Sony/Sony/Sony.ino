const int SPEED_LEFT_GO = 85;
const int SPEED_RIGHT_GO = 80;
const int SPEED_LEFT_BACK = 85;
const int SPEED_RIGHT_BACK = 80;

enum {
  LEFT_MOTOR_SPEED = 5,
  LEFT_MOTOR_DIRECTION = 2,
  RIGHT_MOTOR_SPEED = 6,
  RIGHT_MOTOR_DIRECTION = 4,
  RIGHT_SENS = 7,
  MIDL_SENS = 8,
  LEFT_SENS = 9,
};
void setup() {
  Serial.begin(9600);
  pinMode(LEFT_MOTOR_SPEED, OUTPUT);
  pinMode(LEFT_MOTOR_DIRECTION, OUTPUT);
  pinMode(RIGHT_MOTOR_SPEED, OUTPUT);
  pinMode(RIGHT_MOTOR_DIRECTION, OUTPUT);
  delay(500); // Задержка выполнения всего алгоритма для стабилизации.
}
void loop() {
  //bool leftSens = !digitalRead(LeftSens), midleSens = !digitalRead(MidlSens), rightSens = !digitalRead(RightSens);
  int count = 0;
  while (count <= 8) { // Цикл выполнится 8 раз
    ++count;
    switch (count)
    {
      case 1:
        sendMessage(count);
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 150);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1550);
        Stop(1000);
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1550);
        Stop(1000);
        break;
      case 2:
        sendMessage(count);
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 220);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1650);
        Stop(1000);
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1700);
        Stop(1000);
        break;
      case 3:
        sendMessage(count);
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 200);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1750);
        Stop(1000);
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1750);
        Stop(1000);
        break;
      case 4:
        sendMessage(count);
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 185);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1900);
        Stop(1000);
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1700);
        Stop(1000);
        break;
      case 5:
        sendMessage(count); 
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 210);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1750);
        Stop(1000); 
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1750);
        Stop(1000);   
        break;
      case 6:
        sendMessage(count);
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 200);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1850);
        Stop(1000);
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1750);
        Stop(1000);
        break;
      case 7:
        sendMessage(count);
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 220);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1850);
        Stop(1000);
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1750);
        Stop(1000);
        break;
      case 8:
        sendMessage(count);
        Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 250);
        Stop(1000);
        Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1850);
        Stop(1000);
        Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1750);
        Stop(1000);
        break;
      default:
        Serial.println(count);
        Serial.println("Stop!");
        break;
    }
  }
  exit(0); // Завершение программы
}
void Go(int SPEED_LEFT_GO, int SPEED_RIGHT_GO, int TIME) {
  digitalWrite(LEFT_MOTOR_DIRECTION, HIGH);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  digitalWrite(RIGHT_MOTOR_DIRECTION, HIGH);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  delay(TIME);
}
void Stop(int TIME) {
  analogWrite(LEFT_MOTOR_SPEED, 0);
  analogWrite(RIGHT_MOTOR_SPEED, 0);
  delay(TIME);
}
void Right(int SPEED_LEFT_GO, int SPEED_RIGHT_GO, int TIME) {
  digitalWrite(RIGHT_MOTOR_DIRECTION, LOW);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  digitalWrite(LEFT_MOTOR_DIRECTION, HIGH);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  delay(TIME);
}
void Left(int SPEED_RIGHT_GO, int SPEED_LEFT_GO, int TIME) {
  digitalWrite(LEFT_MOTOR_DIRECTION, LOW);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  digitalWrite(RIGHT_MOTOR_DIRECTION, HIGH);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  delay(TIME);
}
void Back(int SPEED_LEFT_GO, int SPEED_RIGHT_GO, int TIME) {
  digitalWrite(LEFT_MOTOR_DIRECTION, LOW);
  analogWrite(LEFT_MOTOR_SPEED, SPEED_LEFT_GO);
  digitalWrite(RIGHT_MOTOR_DIRECTION, LOW);
  analogWrite(RIGHT_MOTOR_SPEED, SPEED_RIGHT_GO);
  delay(TIME);
}

void MotorTest() {
  Go(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1000);
  Stop(500);
  Right(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1000);
  Stop(500);
  Left(SPEED_LEFT_GO, SPEED_RIGHT_GO, 1000);
  Stop(500);
  Back(SPEED_LEFT_BACK, SPEED_RIGHT_BACK, 1000);
  Stop(500);
}

void sendMessage(int count) {
  Serial.println(count);
  Serial.println("Left");
  Serial.println("Forward and Back");
}
