#include <SoftwareSerial.h>

SoftwareSerial SoftSerial(2, 3);             // 2 - RX, 3 - TX
unsigned char buff[64];                    // Массив для полученных данных (номер карты)
int count = 0;                               // Счетчик полученных символов

// Функция, очищающая приемный буфер
void clrBuff()
{
  for (int i = 0; i < count; i++)            // Цикл от нуля до кол-ва полученных символов
    buff[i] = NULL;                        // Очищаем каждый элемент массива
}

void setup()
{
  Serial.begin(9600);                        // Инициализация первого Serial'a
  SoftSerial.begin(9600);                    // Инициализация Soft Serial'a
}

void loop()
{
  if (SoftSerial.available())                // Пришли данные из SoftSeral порта
  {
    while (SoftSerial.available())           // В порт пришли данные
    {
      byte ch = SoftSerial.read();
      if ((ch != 2) & (ch != 3)) {            // Если не символ начала или конца посылки (данные - номер карты)
        buff[count++] = ch;                // Запись принятого символа в массив
      }
      if (ch == 3) break;                    // Если конец посылки - выходим из цикла чтения
    }
    Serial.write(buff, count);             // Выводим данные в аппаратный порт (Serial)
    Serial.println();                        // Пустая строка
    delay(500);                              // Задержка полсекунды (подавление дребезга)
    clrBuff();                               // Вызываем функцию очистки массива для данных
    count = 0;                               // Обнуляем счетчик количества полученных символов
  }
  if (Serial.available())                    // Если пришли данные в аппаратный порт
    SoftSerial.write(Serial.read());         // Отправляем их в Soft Serial
}
