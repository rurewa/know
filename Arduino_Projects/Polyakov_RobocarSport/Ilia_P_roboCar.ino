// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Робот Ильи П. 2WD, sport
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <NewPing.h>

const unsigned int ECHO_PIN = 4;
const unsigned int TRIG_PIN = 3;

NewPing sonar(TRIG_PIN, ECHO_PIN, 200); // Максимальное расстояние видимости

const int ENA = 5;
const int in1 = 6;
const int in2 = 7;
const int in3 = 8;
const int in4 = 9;
const int ENB = 10;


void setup() {
  Serial.begin(9600);
  pinMode(ENA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  pinMode(ENB, OUTPUT);
}

void loop() {
   //sensTest(40);
/* 
    analogWrite(ENA, 255);
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
    analogWrite(ENB, 255);
    digitalWrite(in3, LOW);
    digitalWrite(in4, HIGH);
  */
}
void sensTest(int times) {
  bool sLeft = 0; bool sCenter = 0; bool sRight = 0;
  Serial.print("Left: ");
  Serial.print(sLeft = digitalRead(11));
  Serial.print(" ");
  Serial.print("Center: ");
  Serial.print(sCenter = digitalRead(12));
  Serial.print(" ");
  Serial.print("Right: ");
  Serial.println(sRight = digitalRead(13));
  delay(times);
}
